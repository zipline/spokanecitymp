<?PHP

	/*********************************************************************************************************
	//CONFIGURATION 
	**********************************************************************************************************/
	
	//DEBUG MODE (MUST BE TURNED OFF WHEN WEBSITE GOES LIVE)
	$config['installation_errors'] = false;
	$config['debug'] = false;
	$config['debug_developer'] = "info@gozipline.com";
	$config['debug_url'] = "website";
	
	//SET THE DATABASE CONNECTION INFORMATION
	$config['hostname'] = "";
	$config['database'] = "";
	$config['database_username'] = "";
	$config['database_password'] = "";

    // SET THE SMTP OPTIONS FOR THE EMAIL (LEAVE THESE EMPTY IF YOU DO NOT WANT TO USE SMTP)
    $config['email_host']	      = 'smtp.gmail.com';
    $config['smtp_secure']        = 'tls';
    $config['smtp_port']          = 587;
    $config['smtp_auth']          = true;
    $config['smtp_timeout']       = 15;
    $config['email_username']     = 'website@ziplineinteractive.com';
    $config['email_password']	  = 'zipline154';
    $config['smtp_debug']         = false;

?>