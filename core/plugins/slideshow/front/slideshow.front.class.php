<?PHP

	/*****************************************************************************************************************************************
	*
	* Front-end Slideshow Plugin
	*
	* @package    ZLCMS
	* @subpackage projects
	* @author     Ryan Stemkoski <ryan@gozipline.com>, Kraig Lutz <kraig@ziplineinteractive.com>
	* @copyright  2005-2014 Zipline Communications Inc.
	* @version    3.0
	* @link       http://www.ziplineinteractive.com
	*
	*****************************************************************************************************************************************/
	class slideshow {

		/*************************************************************************************************************************************
		*
		* This function gets the slides from the database
		*
		*************************************************************************************************************************************/
		function fetch_slides(){
			global $zlcms;

			$return = false;

			// Get slides from database
			$sql = "SELECT * FROM plugin_slideshow_items WHERE company_id=? ORDER BY display";
			$query = $zlcms->database->query($sql, array($zlcms->company_id()));
			$num_rows = $query->num_rows();

			if($num_rows > 0){
				while($result = $query->fetch_assoc()){
					if(!empty($result['link'])){
						$link = $zlcms->build_link('', $result['link']);
						$result['address'] = $link['address'];
						$result['target'] = $link['target'];
					}

					$result['full_image_path'] = $zlcms->file->show_image("slideshow", $result['image'], true);

					$return[] = $result;
				}
			}

			return $return;

		}

	}

?>