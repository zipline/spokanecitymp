<?PHP
    // Get the class name for this plugin
    $plugin = $this->plugin;
?>
<section class="mainContentWrapper" id="<?PHP echo($plugin); ?>">
    <div class="full column borderBottom3px">
        <div class="mainPageTitle">
            <div class="two-third-med half pr">
                <h1 class="push40"><?PHP echo($plugin); ?></h1>
            </div><!--closing of full-->
            <div class="third-med half">
                <a href="index.php?plugin=<?php echo($plugin); ?>&amp;page=form" class="button addPage"><span class="iconSprite whitePlus"></span>Add Item</a>
            </div><!--closing of forty - full-->
        </div><!--closing of mainPageTitle-->

        <div class="full subNavWrapper">

            <?PHP
            // IF YOU WOULD LIKE TO ADD MULTIPLE TABS (i.e. a manage categories tab) THEN YOU NEED TO ADD THE FOLLOWING SPAN AND UL CODE BLOCK
            /*
                <span class="mobileDeviceBtn">Plugin Options <span class="iconSprite downArrowLg"></span></span>
                <ul class="subNav">
                    <li class="active"><a href="#">Core Pages</a></li>
                    <li><a href="#">Orphan Pages</a></li>
                </ul>
             */
            ?>
            <ul class="helpList right">
                <li><a href="#"><span class="info iconSprite"></span></a></li>
                <li><a href="#"><span class="video iconSprite"></span></a></li>

                <li class="pageNumb">1 of 1</li>
                <li><a href="#"><span class="iconSprite arrowLeft"></span></a></li>
                <li><a href="#"><span class="iconSprite arrowRight"></span></a></li>
            </ul>
        </div><!--closing of full - subNavWrapper-->
    </div><!--closing of full-->
    <div class="full p25 bg3">
        <ul class="pageList">
            <li>
                <?PHP
                // BEGIN THE HEADER ROW
                ?>
                <div class="listItem bgDark">
                    <div class="twenty pr">
                        <div class="pageOrder"></div>
                        <div class="pageName">Image</div>
                    </div><!--closing of forty-->
                    <div class="sixty pr">
                        <div class="pageUrl">Title</div>
                    </div><!--closing of forty-->
                </div><!--closing of listItem-->

                <ul class="sortable">
                    <?PHP
                    // BEGIN LISTING THE ITEMS FOR THIS VIEW
                    $slides = $this->slideshow->list;
                    if(!is_array($slides)){
                        echo($slides);
                    } else {
                        foreach($slides as $slide){
                            ?>
                            <li id="id_<?PHP echo($slide['id']); ?>">
                                <div class="listItem <?PHP echo($slide['css']); ?>">
                                    <div class="twenty-sm full pr-sm">
                                        <div class="pageOrder"><span class="iconSprite moveIcon"></span></div>
                                        <div class="pageImg"><img class="border2" src="<?PHP echo($this->file_path_relative . $plugin . '/t_' . $slide['image']); ?>" alt="<?PHP echo($slide['title']); ?>" title="<?PHP echo($slide['title']); ?>" /> </div>
                                    </div><!--closing of forty-->
                                    <div class="sixty-sm eighty pr-sm">
                                        <div class="pageTitle"><?PHP echo($slide['title']); ?></div>
                                    </div><!--closing of forty-->
                                    <div class="twenty">
                                        <div class="pageEdit">
                                            <a href="<?php $this->base(); ?>index.php?plugin=<?php echo($plugin); ?>&amp;page=form&amp;id=<?php echo($slide['id']); ?>">
                                                <span class="iconSprite editIcon"></span>
                                            </a>
                                        </div>
                                        <div class="pageDelete">
                                            <a href="<?php $this->base(); ?>index.php?plugin=<?php echo($plugin); ?>&page=index&action=<?php echo($plugin); ?>_delete&id=<?php echo($slide['id']); ?>" class="delete_confirm">
                                                <span class="iconSprite deleteIcon"></span>
                                            </a>
                                        </div>
                                    </div><!--closing of twenty-->
                                </div><!--closing of main list item-->
                            </li>
                            <?PHP
                        }
                    }
                    ?>
                </ul>
            </li>
        </ul>
    </div>
</section>

<?PHP //BEGIN INFORMATIONAL LIGHTBOX (INFO ABOUT HOW TO USE THIS FORM/PLUGIN GOES HERE) ?>
<section class="lightboxWrapper" id="infoLightbox">
    <div class="full borderBottom3px">
        <h2>Instructions:</h2>
        <div class="closeLightbox">x</div>
    </div>
    <div class="full" id="instructionsHolder">
        <p>This page lists all the slides for your home page slideshow.</p>
        <p><strong>Links</strong><br>If you wish to link to content located at another website, or if you wish to have your link open in a new page, you will need to use the full url.  </p>
        <p><strong>Images</strong><br>It is considered best practice to re-size your image to match the size listed before uploading. The system will automatically crop images that do not match the listed size, and in some cases this may cause the image to appear different than intended.</p>
    </div><!--closing of full-->
</section>

<?PHP //BEGIN VIDEO LIGHTBOX (VIDEO TUTORIAL FOR THIS FORM/PLUGIN GOES HERE) ?>
<section class="lightboxWrapper" id="tutorialLightbox">
    <div class="full borderBottom3px">
        <h2>Video Tutorial</h2>
        <div class="closeLightbox">x</div>
    </div>
    <div class="full" id="videoHolder"></div>
</section>