//**********************************************************************************************************************
/*
* DOCUMENT: /core/plugins/slideshow/back/slideshow.back.js
* DEVELOPED BY: Ryan Stemkoski, Kraig Lutz
* COMPANY: Zipline Interactive
* EMAIL: ryan@gozipline.com, kraig@ziplineinteractive.com
* PHONE: 509-321-2849
* DATE: 4/06/2014
* DESCRIPTION: This document has all of the javascript functions required for the slideshow plugin.
*/
//***********************************************************************************************************************

//***********************************************************************************************************************
//ON DOCUMENT READY FUNCTIONS
//***********************************************************************************************************************
$(function() {

    // Check that we are on the slideshow page
	if($('#slideshow').length > 0){
        //***********************************************************************************************************************
        //CREATE MOVEABLE ELEMENT
        //***********************************************************************************************************************
        plugin = "slideshow";
        plugin_move_url = "index.php?plugin=" + plugin + "&page=index";
        plugin_move_action = plugin + "_rank";
        $('#slideshow .sortable .moveIcon').bind("mousedown",plugin_sort);
    }
	
});