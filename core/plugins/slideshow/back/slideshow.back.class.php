<?PHP

/*****************************************************************************************************************************************
 *
 * Back-end Slideshow Plugin
 *
 * @package    ZLCMS
 * @subpackage projects
 * @author     Ryan Stemkoski <ryan@gozipline.com>, Kraig Lutz <kraig@ziplineinteractive.com>
 * @copyright  2005-2014 Zipline Communications Inc.
 * @version    3.0
 * @link       http://www.ziplineinteractive.com
 *
 *****************************************************************************************************************************************/
class slideshow {

    var $title= '';
    var $image = array();
    var $list = false;
    var $item = false;
    var $items_table = false;

    /*************************************************************************************************************************************
     * This function is used to establish the base options for this plugin, primarily setting the title and image size variables for use throughout the plugin
     *************************************************************************************************************************************/
    function plugin_construct(){
        global $zlcms;

        $this->title = "slideshow";
        $this->items_table = 'plugin_' . $this->title . '_items';
        $this->image["main"] = array("width" => 1200, "height" => 500);

        if(@$_GET['plugin'] == $this->title){
            if(@$_GET['page'] == 'index'){
                $this->list = $zlcms->tools->fetch_list($this->items_table,'display ASC');
            }else if(@$_GET['page'] == 'form'){
                $this->item = $zlcms->tools->fetch_item($this->items_table,@$_GET['id']);
                if(!empty($this->item['image'])){
                    $this->item['image_path'] = $zlcms->file_path_relative . $this->title . '/t_' . $this->item['image'];
                }
            }
        }
    }

    /*************************************************************************************************************************************
     *
     * This function is used to process get or post requests within this plugin.
     * @param $action string Is a string indicating which action we are trying to process.
     *
     *************************************************************************************************************************************/
    function process_request($action){
        global $zlcms;
        if($zlcms->is_logged()){
            if($action == $this->title . "_submit"){
                $this->handle_submit();
            } else if($action == $this->title . "_delete"){
                $this->handle_delete(@$_GET['id']);
            } else if($action == $this->title . "_rank"){
                $zlcms->tools->update_rank($_POST['items'],$this->items_table);
                die;
            }
        }
    }

    /*************************************************************************************************************************************
     *
     * This function processes the plugin add/edit form (saves the slide to the database)
     *
     *************************************************************************************************************************************/
    private function handle_submit(){
        global $zlcms;
        $id = @$_GET['id'];
        $_POST['company_id'] = $zlcms->company_id();

        $zlcms->form->set_rule(@$_POST['title'],"empty","You must provide a valid title to continue.");
        $zlcms->form->set_rule(@$_POST['link'],"empty","You must provide a valid link to continue.");
        $zlcms->form->set_rule(@$_POST['link_text'],"empty","You must provide valid link text to continue.");

        if(empty($id)){
            $zlcms->form->set_rule(@$_FILES['image']['name'],"empty","You must provide a valid image to add a slide.");
        }

        if($zlcms->form->run_rules()){
            if(empty($id)){
                $_POST['display'] = $zlcms->tools->calculate_display($this->items_table);
            }

            $fields = $zlcms->database->convert_array_to_fields($_POST,array("id","action","submit","save"));

            if(!empty($id)){
                $sql_files = "SELECT image FROM {$this->items_table} WHERE id=?";
                $query_files = $zlcms->database->query($sql_files,array($id));
                $result_files = $query_files->fetch_assoc();
            }

            if(!empty($_FILES['image']['name'])){
                if($zlcms->file->check_type(strtolower($_FILES['image']['name']),array("jpg","gif","png","jpeg"))){
                    $image = $zlcms->file->upload($this->title,@$_FILES['image'],@$result_files['image']);
                    $zlcms->file->delete($this->title,"t_" . @$result_files['image']);

                    $image_file = $zlcms->file_path_relative . "/" . $this->title . "/" . strtolower($image);
                    $image_file2 = $zlcms->file_path_relative . "/" . $this->title . "/t_" . strtolower($image);

                    $zlcms->image->crop($image_file, $this->image['main']['width'], $this->image['main']['height'], $image_file);

                    // this scales the image to the same ratio, but so that it's only 155 pixels wide to fit well in the backend thumbnail column
                    $zlcms->image->crop($image_file, 155, round($this->image['main']['height'] * 155 / $this->image['main']['width']), $image_file2);

                    if(!empty($image)){
                        $fields[] = "image";
                        $_POST['image'] = $image;
                    }
                } else {
                    $message = "The image you uploaded was not the proper file type.";
                    header("Location: " . $zlcms->base(true) . "index.php?plugin={$this->title}&page=index&error=" . $message);
                    exit;
                }
            }

            // IF ID UPDATE OTHERWISE STORE IT
            if(!empty($id)){
                $zlcms->log_event("updated the {$this->title} item {$_POST['title']}");
                $query = $zlcms->database->update_auto_helper($fields,$_POST,$this->items_table,"WHERE id=?",$arguments = array($id));
            } else {
                $zlcms->log_event("added the {$this->title} item {$_POST['title']}");
                $query = $zlcms->database->insert_auto_helper($fields,$_POST,$this->items_table);
            }

            //IF QUERY SHOW SUCCESS OTHERWISE SHOW ERROR
            if($query){
                $message = "Your {$this->title} item has been saved.";
                header("Location: " . $zlcms->base(true) . "index.php?plugin={$zlcms->plugin}&page=index&success=" . $message);
            } else {
                $zlcms->log_error("There was a problem updating the database. Please try again");
            }
        } else {
            $zlcms->log_error($zlcms->form->error);
        }
    }

    /*************************************************************************************************************************************
     *
     * This function deletes a slide and any associated files from the server.
     * $id The id of the item we are deleting
     *
     *************************************************************************************************************************************/
    private function handle_delete($id){
        global $zlcms;

        $sql_files = "SELECT title,image FROM {$this->items_table} WHERE id=? AND company_id=?";
        $query_files = $zlcms->database->query($sql_files,array($id,$zlcms->company_id()));
        $number_files = $query_files->num_rows();

        if($number_files > 0){
            $result_files = $query_files->fetch_assoc();

            $sql_delete = "DELETE FROM {$this->items_table} WHERE id=? AND company_id=?";
            $query_delete = $zlcms->database->query($sql_delete,array($id,$zlcms->company_id()));

            if($query_delete){
                $zlcms->file->delete($this->title,$result_files['image']);
                $zlcms->file->delete($this->title,"t_" . $result_files['image']);
                $zlcms->tools->reset_rank($this->items_table);

                $zlcms->log_event("deleted the {$this->title} item {$result_files['title']}");
                $zlcms->log_success("You have deleted the {$this->title} item: {$result_files['title']}");
            } else {
                $zlcms->log_error("Your {$this->title} item could not be deleted");
            }
        }
    }
}
?>