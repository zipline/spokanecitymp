<?PHP
    global $zlcms;
    $plugin = $this->plugin;
    $page_title = isset($_GET['id']) ? 'Edit Item' : 'Add Item';
    $slideshow = $this->slideshow->item;
?>

<section class="mainContentWrapper">
    <form id="<?PHP echo($plugin); ?>_item" action="<?PHP $this->get_form_action(); ?>" method="post" enctype="multipart/form-data">
        <div class="full column borderBottom3px">
            <div class="mainPageTitle">
                <div class="two-third-med half pr">
                    <h1 class="push40"><?PHP echo($page_title); ?></h1>
                </div><!--closing of full-->
                <div class="third-med half">
                    <a href="/zlcms/index.php?plugin=<?PHP echo($plugin); ?>&page=index" class="button button2 backPage">Back</a>
                    <input type="submit" class="button addPage" value="SAVE CHANGES">
                </div><!--closing of forty - full-->
            </div><!--closing of mainPageTitle-->

            <div class="full subNavWrapper">
                <?PHP
                // IF YOU WOULD LIKE TO ADD MULTIPLE TABS (i.e. a manage categories tab) THEN YOU NEED TO ADD THE FOLLOWING SPAN AND UL CODE BLOCK
                /*
                    <span class="mobileDeviceBtn">Plugin Options <span class="iconSprite downArrowLg"></span></span>
                    <ul class="subNav">
                        <li class="active"><a href="#">Core Pages</a></li>
                        <li><a href="#">Orphan Pages</a></li>
                    </ul>
                 */
                ?>
                <ul class="helpList right">
                    <li><a href="#"><span class="info iconSprite"></span></a></li>
                    <li><a href="#"><span class="video iconSprite"></span></a></li>
                </ul>
            </div><!--closing of full - subNavWrapper-->
        </div>
        <div class="full p25 bg3">
            <div class="full">
                <label class="full" for="title">Title *</label>
                <input class="full" type="text" id="title" value="<?PHP $this->form->toggle_value(@$slideshow['title'],@$_POST['title']); ?>" name="title" />
            </div><!--closing of full-->

            <div class="full">
                <div class="half-med pr-med full">
                    <label class="full" for="link_text">LINK TEXT *</label>
                    <input class="full" type="text" id="link_text" value="<?PHP $this->form->toggle_value(@$slideshow['link_text'],@$_POST['link_text']); ?>" name="link_text" />
                </div>
                <div class="half-med full">
                    <label class="full" for="link">LINK *</label>
                    <input class="full" type="text" id="link" value="<?PHP $this->form->toggle_value(@$slideshow['link'],@$_POST['link']); ?>" name="link" />
                </div>
            </div><!--closing of full-->

            <div class="sixty-med full pr-med pushd30">
                <div class="full">
                    <label class="full" for="file_upload">Image * (<?PHP echo($this->$plugin->image['main']['width'] . ' x ' . $this->$plugin->image['main']['height']); ?>)</label>
                </div><!--closing of full-->
                <div class="three-quarter-lg full pr-lg">
                    <div class="fileUploadWrapper">
                        <label class="fileName">No image selected</label>
                        <div class="fileInputWrapper">
                            <input type="file" name="image" class="full upload_button" />
                        </div><!--closing of fileInputWrapper-->
                    </div><!--closing of fileUploadWrapper-->
                </div><!--closing of full-->
                <div class="quarter-lg full">
                    <?PHP
                    if(!empty($slideshow['image_path'])){
                        echo('<img class="imgUploadEx border1" src="' . $slideshow['image_path'] . '" title="Slideshow Image" alt="Slideshow Image" /> ');
                    }
                    ?>
                </div><!--closing of quarter-->
            </div><!--closing of full-->

            <div class="full">
                <input type="hidden" name="action" value="<?PHP echo($plugin); ?>_submit" />
                <input type="submit" value="Save Changes" class="button" />
            </div><!--closing of full-->
        </div><!--closing of p25 full bg3-->
    </form>
</section>

<?PHP //BEGIN INFORMATIONAL LIGHTBOX (INFO ABOUT HOW TO USE THIS FORM/PLUGIN GOES HERE) ?>
<section class="lightboxWrapper" id="infoLightbox">
    <div class="full borderBottom3px">
        <h2>Instructions:</h2>
        <div class="closeLightbox">x</div>
    </div>
    <div class="full" id="instructionsHolder">
        <p>Use this form to add/edit this slide.</p>
        <p><strong>Links</strong><br>If you wish to link to content located at another website, or if you wish to have your link open in a new page, you will need to use the full url.  </p>
        <p><strong>Images</strong><br>It is considered best practice to re-size your image to match the size listed before uploading. The system will automatically crop images that do not match the listed size, and in some cases this may cause the image to appear different than intended.</p>
    </div><!--closing of full-->
</section>

<?PHP //BEGIN VIDEO LIGHTBOX (VIDEO TUTORIAL FOR THIS FORM/PLUGIN GOES HERE) ?>
<section class="lightboxWrapper" id="tutorialLightbox">
    <div class="full borderBottom3px">
        <h2>Video Tutorial</h2>
        <div class="closeLightbox">x</div>
    </div>
    <div class="full" id="videoHolder"></div><!--closing of full-->
</section>