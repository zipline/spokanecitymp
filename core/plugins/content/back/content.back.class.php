<?PHP

	//*****************************************************************************************************************************************
	/**
	*
	* Back-end Content Plugin
	* 
	* @package    ZLCMS
	* @subpackage content
	* @author     Ryan Stemkoski <ryan@ziplineinteractive.com>
	* @copyright  2005-2014 Zipline Communications Inc.
	* @version    3.0
	* @link       http://www.ziplineinteractive.com
	*/
	//*****************************************************************************************************************************************
	class content {
		
		/**
		* @var string Default pagetype to display.
		*/
		var $pagetype = "core";

		/**
		* @var array An array that contains configuration data specific to the content plugin.
		*/
		var $config = array();
		
		/**
		* @var array An array that contains the users display preferences
		*/
		var $preferences = array();

		/**
		* @var array An array that contains all of the information available on the page that is currently being edited..
		*/
		var $page = array();

		/**
		* @var string A string that is populated with information about the row color.
		*/
		var $row_color = "";
		

		//*************************************************************************************************************************************
		/**
   		* process_request function. This method is used to process get or post requests within this plugin.
   		* 
   		* @access public
		* @param string $action A mixed string that functions as a keyword to trigger a method.
   		* @return void
   		*/
   		//*************************************************************************************************************************************
   		public function process_request($action) {
   			
   			global $zlcms;		
   			
   			//CONFIRM THE USER IS LOGGED IN BEFRORE COMPLETING ANY TASKS	
   			if($zlcms->is_logged()) {
				
				//HANDLE AN AJAX REQUEST TO RANK CONTENT 
				if($action == "content_rank") {
					$this->content_rank();
					die;
					
				//HANDLE AN AJAX REQUEST TO SWITCH PAGETYPE
				} else if ($action == "content_switch_pagetype") {
					$this->content_switch_pagetype();
					die;
				
				//HANDLE A REQUEST TO UPDATE PREFERENCES
				} else if ($action == "content_update_preferences") {
					$this->content_update_preferences();
					die;
					
				//HANDLE AN AJAX REQUEST TO SET A PERMALINK
				} else if ($action == "content_permalink") {
					$this->content_permalink(@$_POST['text'],@$_POST['id']);
					die;
				
				//HANLDE A SAVE CONTENT REQUEST
				} else if ($action == "content_save") {
					$this->content_save();
					
				//HANDLE A DElETE CONTNET REQUEST
				} else if ($action == "content_delete") {
					$this->content_delete(@$_GET['id']);
				}	
			}
		}
		
		//*************************************************************************************************************************************
		/**
		* plugin_construct function. Set pagetype on the construct of the content plugin.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function plugin_construct() {
		
			$this->set_pagetype();
		
		}

		
		//*************************************************************************************************************************************
		/**
		* content_save function.  This method is used to save content on submit of content form.
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function content_save() {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//DETERMINE SAVE REDIRECT BASED ON WHICH BUTTON WAS PUSHED
			if(!empty($_POST['save_continue'])) {
				unset($_POST['save_continue']);
				
			} else {
				$redirect = $zlcms->base(true) . "index.php?plugin=content&page=index";
				unset($_POST['save']);
			}
			
			//UNSET ACTION
			unset($_POST['action']);
			
			//ADJUST PARENT ID
			$_POST['parent_id'] = $this->calculate_parent_id(@$_POST['pagetype'],@$_POST['parent_id']);
			$_POST['tier'] = $this->calculate_tier(@$_POST['parent_id']);
			$_POST['permalink'] = $this->content_permalink((empty($_POST['permalink'])) ? @$_POST['title'] : @$_POST['permalink'],@$_POST['id'],true);
			$_POST['company_id'] = $zlcms->company_id();
			

			//RESET MOBILE CONTENT TO EMPTY IF ONLY A LINE BREAK
			if(!empty($_POST['mobile_content'])) {
				if(strlen($_POST['mobile_content']) == 8) {
					if(substr($_POST['mobile_content'], 0, 6) == "<br />") {
						$_POST['mobile_content'] = "";
					}
				}
			}
			
			//RESET CONTENT TO EMPTY IF ONLY A LINE BREAK
			if(strlen($_POST['content']) == 8) {
				if(substr($_POST['content'], 0, 6) == "<br />") {
					$_POST['content'] = "";
				}
			}

						
			//NOW DETERMINE IF WE ARE ADDING OR EDITING
			if(!empty($_POST['id'])) {
			
				//DO QUICK CHECK TO MAKE SURE USER HAS PERMS ON CURRENT ITEM
				$sql = "SELECT * FROM plugin_content_pages WHERE id=? AND level<=?";
				$query = $zlcms->database->query($sql,array($_POST['id'],$zlcms->level()));
				$number = $query->num_rows();
				$result = $query->fetch_assoc();
				
				//IF ONE THEN USER HAS PERMS TO EDIT THIS PAGE
				if($number >= 1) {
					
					//CALCULATE ORPHAN DISPLAY IF MOVING TO ORPHAN
					if(@$_POST['pagetype'] == "orphan" && $result['pagetype'] == "core") {
						$_POST['tier'] = 1;
						$_POST['display'] = $this->calculate_display("orphan",0);
					} else {
						if($result['parent_id'] != $_POST['parent_id']) {
							$_POST['display'] = $this->calculate_display("core",$_POST['parent_id']);
						}
					}
					
					//SET LIST OF FIELDS
					$fields = array_keys($_POST);
					
					$update_query = $zlcms->database->update_auto_helper($fields,$_POST,"plugin_content_pages","where id=?",array($_POST['id']));
				
					//HANDLE RESPONSE
					if($update_query) {
						$message = "Your page has been updated.";
						$zlcms->log_success($message);
						$zlcms->log_event("updated the page {$_POST['title']} for");
												
						//IF PAGETYPE HAS CHANGED TO ORPHAN FROM CORE THEN CHILDREN NEED TO MOVE TO ORPHAN TOO
						if(@$_POST['pagetype'] == "orphan" && $result['pagetype'] == "core") {
	
							//MOVE ALL CHILD PAGES TO ORPHAN PAGES
							$this->move_children_to_orphan($_POST['id']);
						}
						
						if(!empty($redirect)) {
							header("Location: " . $redirect . "&success=" . $message);
						}
					} else {
						$zlcms->log_error("There was a problem saving your content. Please try again.");
					}
				
				} else {
					$zlcms->log_error("You do not have permission to edit this page.");
				}
			
			} else {
			
				//DETERMINE CALCULATED VALUES
				$_POST['display'] = $this->calculate_display($_POST['pagetype'],$_POST['parent_id']);
				
				//DO INSERT
				$fields = array_keys($_POST);
				$insert_query = $zlcms->database->insert_auto_helper($fields,$_POST,"plugin_content_pages");
				$insert_number = $insert_query->num_rows();
			
				//IF QUERY
				if($insert_number >= 1) {
					$message = "Your page has been saved.";
					$zlcms->log_success($message);
					$zlcms->log_event("added the page {$_POST['title']} for");
					
					if(!empty($redirect)) {
						header("Location: " . $redirect . "&success=" . $message);
					} else {
						$insert_id = $insert_query->insert_id();
						header("Location: " . $zlcms->base(true) . "index.php?plugin=content&page=form&id={$insert_id}");
					}
				} else {
					$zlcms->log_error("There was a problem saving your content. Please try again.");
				}
			
			} 
		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* content_delete function. This method is used to delete a page and reposition all of it's child elements as orphan pages.
		* 
		* @access private
		* @param integer $id The database ID of the item that is to be deleted.
		* @return void
		*/
		//*************************************************************************************************************************************
		private function content_delete($id) {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//CHECK TO SEE IF USER HAS PERMISSIONS TO DELETE THIS PAGE
			$sql = "SELECT id,title FROM plugin_content_pages WHERE id=? AND level<=?";
			$query = $zlcms->database->query($sql,array($id,$zlcms->level()));
			$number = $query->num_rows();
			
			//IF USER HAS PERMISSIONS DO DELETE
			if($number == 1) {
				
				//DO DELETE
				$sql_delete = "DELETE FROM plugin_content_pages WHERE id=?";
				$query_delete = $zlcms->database->query($sql_delete,array($id));
				$number_delete = $query_delete->num_rows();
				
				//IF ITEM WAS DELETED
				if($number_delete == 1) {
					$result = $query->fetch_assoc();
					$zlcms->log_event("deleted the page {$result['title']} for");
					$zlcms->log_success("The {$result['title']} has been deleted.");
					
					
					//MOVE ALL CHILD PAGES TO ORPHAN PAGES
					$this->move_children_to_orphan($id);
					
				} else {
					$zlcms->log_error("An error occurred while deleting your page.  Please try again.");
				}
				
			} else {
				$zlcms->log_error("Sorry, you do not have permission to remove this page.");
			}
		
		}
		
		//*************************************************************************************************************************************
		/**
		* move_children_to_orphan function. This functions moves all of the children pages of a given item to orphan status. It is called internally
		* when a page is deleted or edited in a way that causes its children to no longer be an option.
		* 
		* @access private
		* @param integer $parent_id The database ID of the item that is being deleted or moved. All items with that ID or a child ID as a parent ID get moved.
		* @return void
		*/
		//*************************************************************************************************************************************
		private function move_children_to_orphan($parent_id) {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
	
			//SELECT ITEMS TO MOVE
			$sql = "SELECT id FROM plugin_content_pages WHERE parent_id=?";
			$query = $zlcms->database->query($sql,array($parent_id));
			
			//FOR EACH ITEM DO UPDATE
			while($result = $query->fetch_assoc()) {
		
				//CALCULATE NEW DISPLAY
				$new_display = $this->calculate_display("orphan",0);
				
				//UPDATE ITEM
				$sql_update = "UPDATE plugin_content_pages SET pagetype=?, parent_id=?, display=?, tier=1 WHERE id=?";
				$query_update = $zlcms->database->query($sql_update,array("orphan",0,$new_display,$result['id']));
				
				//NOW MOVE CHILDREN OF THIS ITEM TO ORPHAN STATUS 
				$this->move_children_to_orphan($result['id']);
				
			}
		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* calculate_display function. This method is used to detrmine the display rank of an item being inserted into the database.
		* 
		* @access private
		* @param string $pagetype A Boolean style string.  values are either core or orhpan and vary based on where the page is to be positioned.
		* @param integer $parent A numeric value that represents the parent ID of the item in question.
		* @return void
		*/
		//*************************************************************************************************************************************
		private function calculate_display($pagetype,$parent) {
		
			//GLOBAL ZLCMS
			global $zlcms;
		
			//CALCULATE RANK FOR THIS ITEM
			$sql = "SELECT id FROM plugin_content_pages WHERE pagetype=? AND parent_id=? AND company_id=?";
			$query = $zlcms->database->query($sql,array($pagetype,$parent,$zlcms->company_id()));
			$number = $query->num_rows();
			$output = $number+1; 
			
			//RETURN CALCULATED RANK
			return $output;
		}
		

		//*************************************************************************************************************************************
		/**
		* calculate_parent_id function. This method is used to toggle the parent ID of the current element based on the page type that is being
		* provided.
		* 
		* @access private
		* @param string $pagetype A Boolean style string.  values are either core or orhpan and vary based on where the page is to be positioned.
		* @param integer $parent A numeric value that represents the parent ID of the item in question.
		* @return integer The parent ID of the current page.
		*/
		//*************************************************************************************************************************************
		private function calculate_parent_id($pagetype,$parent) {
		
			if(!empty($parent)) {
				if($pagetype == "orphan") {
					$output = 0;
				} else {
					$output = $parent;
				}
			} else {
				$output = 0;
			}
			
			return $output;
		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* calculate_tier function. Determines the navigation tier for the provided parent ID.
		* 
		* @access private
		* @param mixed $parent The database ID of the parent item for which we need to find the navigation tier.
		* @return integer The navigation tier of the parent item provided.
		*/
		//*************************************************************************************************************************************
		private function calculate_tier($parent) {
			
			//IMPORT GLOBAL
			global $zlcms;
			
			//CHECK TO SEE IF THIS ITEM HAS SELECTABLE ITEMS
			if($parent > 0) {
			
				//CALCULATE RANK FOR THIS ITEM
				$sql = "SELECT tier FROM plugin_content_pages WHERE id=? AND company_id=? LIMIT 1";
				$query = $zlcms->database->query($sql,array($parent,$zlcms->company_id()));
				$result = $query->fetch_assoc();
				$output = $result['tier']+1;
				
			} else {
				$output = 1;
			}
			
			//RETURN CALCULATED RANK
			return $output;
			
		}
		

		//*************************************************************************************************************************************
		/**
		* set_pagetype Toggles the pagetype variable with data stored in the session.
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function set_pagetype() {
		
			if(!empty($_SESSION['zladmin']['content']['pagetype'])) {
				$this->pagetype = $_SESSION['zladmin']['content']['pagetype'];
			} else {
				$this->pagetype = "core";
				$_SESSION['zladmin']['content']['pagetype'] = "core";
			}
		
		}
		
	
		//*************************************************************************************************************************************
		/**
		* load_settings function. This method is used to load the content plugin settings.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function load_settings() {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//IF EMPTY ARRAY
			if($zlcms->is_logged() && $zlcms->company_id() != 0 && $zlcms->plugin = "content") {
						
				//LOAD THE SETTINGS
				$sql = "SELECT * FROM plugin_content_config WHERE company_id=?";
				$query = $zlcms->database->query($sql,array($zlcms->company_id()));
				$number = $query->num_rows();

				if($number >= 1) {
					$result = $query->fetch_assoc();
					$this->config = $result;
				} else {
					die;
				}
			
			}
		
		}
		

		//*************************************************************************************************************************************
		/**
		* load_preferences function. This method is used to load the content plugin settings.
		* 
		* @access public
		* @param bool $output_javascript (default: false) A boolean value indicating if we should output the Javascript the function generates or not.
		* @var array $preferences Sets a value called preferences that is used through out the content page to indicate the user preferences.
		* @return void
		*/
		//*************************************************************************************************************************************
		public function load_preferences($output_javascript = false) {
		
			//LOAD GLOBAL ZLCMS
			global $zlcms;
		
			//DO QUERY FOR PREFERENCES
			$sql = "SELECT * FROM plugin_content_preferences WHERE user_id=? AND company_id=?";
			$query = $zlcms->database->query($sql, array($zlcms->user_id(),$zlcms->company_id()));
			$number = $query->num_rows();
			
			if($number >= 1) {
				$this->preferences = $query->fetch_assoc();
			} 
			
			//IF OUTPUT JAVASCRIPT IS TRUE
			if($output_javascript == true) {
						
				//TEMP ARRAY USED TO COMPILE VALUES SET TO TRUE
				$temp = array();
				
				//BUILD LIST OF TRUE ELEMENTS
				$output = "<script type=\"text/javascript\"> var panel = new Array(); panel.push(";
					
					//BUILD LIST OF TRUE ELEMENTS
					foreach($this->preferences as $key=>$value) {
						if($value == "true") {
							$temp[] = $key;
						}
					}
					
					//NOW ADD THOSE TO THE ARRAY
					$total  = count($temp);
					$n = 1;
					
					//BUILD THE FORMATTED ARRAY FOR JAVASCRIPT
					foreach($temp as $key) {
						if($n < $total) {
							$output .= "'{$key}',";
						} else {
							$output .= "'{$key}'";
						}
						$n++;
					}
					
				$output .= "); </script>";
				
				echo($output);
		
			}
		}
		

		//*************************************************************************************************************************************
		/**
		* form_tabs function. This method uses the preferences value to determine which buttons to show.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function form_tabs() {
		
			//GLOBAL ZLCMS
			global $zlcms;
		
		
			//SET LIST OF POSSIBLE BUTTONS FOR THIS USER
			$possible_buttons = array("metatags"=>"Meta Tags","url"=>"URL","permissions"=>"Permissions","notes"=>"Notes");
			
			//ADD PROPERTIES FOR SUPER ADMIN
			if($zlcms->level() >= 1000) {
				$possible_buttons['properties'] = "Properties";
			}
			
			//CLEAR OOUTPUT FOR USE
			$output = '<ul class="subNav">';
			
			//LOOP BUTTONS IN REQUEST AND BUILD HTML
			foreach($possible_buttons as $key=>$value) {
				
				//DETERMINE IF THIS STARTS ON OR OFF
				if(!empty($this->preferences)) {
					if($this->preferences[$key] == "true") {
						$active = " active";
					} else {
						$active = "";
					} 
				} else {
					$active = "";
				}
				
				//ADD IT TO THE ARRAY IF IT IS IN THE PARENT ARRAY (HIDE PREFERENCES FROM NON SUPER ADMIN USERS, PERIOD);
				$output .= '<li><a href="#" class="content_form_tab '.$active.'" data-type="'.$key.'">'.$value.'</a></li>';
				
				
			}
			
			$output .= '</ul>';
			
			//IF WE CREATED OUTPUT SHOW IT
			if(!empty($output)) {
				echo($output);
			}
		}
		
		//*************************************************************************************************************************************
		/**
		* content_update_preferences function. This method is called from the content form using AJAX and is used to store which paenls the user wants open.
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function content_update_preferences() {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//GET CURRENT ITEM
			$item = $zlcms->database->escape_string($_POST['item']);
			
			//CHECK TO SEE WHAT CURRENT PREFRENCES IS
			$sql = "SELECT * FROM plugin_content_preferences WHERE user_id=? AND company_id=?";
			$query = $zlcms->database->query($sql, array($zlcms->user_id(),$zlcms->company_id()));
			$number = $query->num_rows();
			
			//IF USER HAS EXISTING PREFERENCE UPDATE IT OTHERWISE MOVE ON
			if($number >= 1) {
			
				//GET CURRENT ID
				$result = $query->fetch_assoc();
				
				//DETERMINE NEW PREFERENCE
				if($result[$item] == "true") {
					$store = "false";
				} else {
					$store = "true";
				}
				
				//UPDATE PREFERENCES
				$sql_update = "UPDATE plugin_content_preferences SET {$item}=? WHERE id=?";
				$query_update = $zlcms->database->query($sql_update,array($store,$result['id']));
				
			} else {
			
				//CREATE PREFERENCES
				$sql_insert = "INSERT INTO plugin_content_preferences (user_id,company_id,{$item}) Values (?,?,?)";
				$query_insert= $zlcms->database->query($sql_insert, array($zlcms->user_id(),$zlcms->company_id(),"true"));
			
			}
		
		}


		//*************************************************************************************************************************************
		/**
		* show_list function. This method generates a complete HTML output showing all of the available pages.  The list is filtered based on user permissions.
		* 
		* @access public
		* @param string $pagetype A boolean style string containing core or orphan that indicates which type of pages to load.
		* @return void
		*/
		//*************************************************************************************************************************************
		public function show_list($pagetype) {
			
			//LOAD THE CONTENT PLUGIN SETTINGS
			$this->load_settings();
			
			//BUILD THE NAVIGATION
			$content = $this->get_navigation_tree($pagetype);
			
			//IF WE HAVE ITEMS SHOW THEM
			if($content) {
				echo($content);
			} else {
				echo('<p>This site does not have any pages of this type.</p>');
			}

		}
		
		
		//*************************************************************************************************************************************
		/**
		* content_rank function. This function allows the user to rank pages based using drag and drop on the plugin index page.
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function content_rank() {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//MAKE SURE THE USER IS LOGGED IN
			if($zlcms->is_logged()) {
			
				$parent_id = $_POST['parent'];
				$values = $_POST['items'];
				$values = str_replace("id[]=","",$values);
				$items = explode("&",$values);

				$n = 1;
				foreach($items as $item) {
					$sql_update_rank = "UPDATE plugin_content_pages SET display=? WHERE id=?";
					$query_update_rank = $zlcms->database->query($sql_update_rank,array($n,$item));
					echo($zlcms->database->error);
					$n++;
				}
				
				//SELECT THE PARENT ITEM FOR THE MESSAGE
				if($parent_id == 0) {
					if($this->pagetype == "core") {
						$title = "the main navigation";
					} else {
						$title = "the orhan pages";
					}
				} else {
					$sql = "SELECT title FROM plugin_content_pages WHERE id=?";
					$query = $zlcms->database->query($sql,array($parent_id));
					$result = $query->fetch_assoc();
					$title = $result['title'];
				}
				
				//LOG SUCCESS
				$zlcms->log_event("updated the rank of {$title}");
				
				//END THE SCRIPT AFTER UPDATE
				die;
			
			} else {
				echo("not logged");
				die;
			}
		
		}
		
		//*************************************************************************************************************************************	
		/**
		* content_permalink function. This function will build a permalink from the text input.  It also includes an optional parameter that 
		* allows it to be output or returned
		* 
		* @access private
		* @param string $text A string that will be turned into a valid URL
		* @param int $id (default: 0) The database ID of the item we are editing (If editing)
		* @param bool $return (default: false) A boolean value indiciating if we should return or output the value directly
		* @return string A valid permalink in string format. 
		*/
		//*************************************************************************************************************************************
		private function content_permalink($text, $id=0, $return = false) {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//CLEAR OUTPUT FOR USE
			$output = "";
			
			//SETS THE LOCALE
			setlocale(LC_ALL, 'en_US.UTF8');
			
			//CREATES REQUIRED VARIABLES
			$replace = array();
			$delimiter = "-";
			$str = "";
			
			if(!empty($replace)) {
				$text = str_replace((array)$replace, ' ', $text);
			}

			//CLEAN URL
			$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $text);
			$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
			$clean = strtolower(trim($clean, '-'));
			$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);	
			
			//REMOVE TRAILING DASH
			$last = substr($clean, -1);
			if($last == "-") {
				$clean = substr($clean,0,-1);
			} 	
	
			//CHECK TO SEE IF THIS URL EXISTS
			$sql = "SELECT * FROM plugin_content_pages WHERE permalink=? AND company_id=? AND id != ?";
			$query = $zlcms->database->query($sql, array($clean,$zlcms->company_id(),$id));
			$number = $query->num_rows();
			
			//IF WE SOMEHOW GOT HERE WITH NO TEXT SO NO TITLE OR TEXT ADD A PAGE NUMBER
			if(empty($text)) {
				$sql_max = "SELECT MAX(id) as id FROM plugin_content_pages";
				$query_max  = $zlcms->database->query($sql_max,array());
				$result_max = $query_max->fetch_assoc();
				$new_id =  $result_max['id'] + 1;
				$clean = $clean . $new_id;
			
			//THIS HANDLES ALL NORMAL INSTANCES
			} else {
			
				if($number >= 1) {
					if(!empty($id)) {
						$clean = $clean . $id;
					} else {
						$sql_max = "SELECT MAX(id) as id FROM plugin_content_pages";
						$query_max  = $zlcms->database->query($sql_max,array());
						$result_max = $query_max->fetch_assoc();
						$new_id =  $result_max['id'] + 1;
						$clean = $clean . $new_id;
					}
				}
			
			}
	
			//RETURN OR OUTPUT
			if($return == "true") {
				return $clean;
			} else {
				echo($clean);
			}
			
		}
		

		//*************************************************************************************************************************************	
		/**
		* get_pagetype function. This function checks the pagetype session value then outputs the value as a javascript variable for the
		* tabbed system on the index view of this plugin.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function get_pagetype() {
		
			//REET PAGETYPE TO THE STORED TYPE IF THERE IS ONE OTHERWISE USE THE DEFULAT DEFINED AT THE TOP OF THE DOCUMENT
			if(!empty($_SESSION['zladmin']['content']['pagetype'])) {
				$this->pagetype = $_SESSION['zladmin']['content']['pagetype'];
			}
			
			echo('<script> pagetype = "'.$this->pagetype.'" </script>');
		
		}
		

		//*************************************************************************************************************************************	
		/**
		* content_switch_pagetype function. This method switches the page type from core to orphan and sets a session value using ajax. 
		* Everytime the page loads it will stay in that pagetype until the user logs out or uses the tab to switch back.
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function content_switch_pagetype() {
		
		
			if(!empty($_POST['pagetype'])) {
				$_SESSION['zladmin']['content']['pagetype'] = $_POST['pagetype'];
			} else {
				$_SESSION['zladmin']['content']['pagetype'] = "core";
			}	
		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* get_navigation_tree function. This function builds an array containing the navigation information necessary to build the navigation 
		* tree for the ZLCMS system. Note that this function calls itself again and again to build each teir so if modifications are made
		* they could impact more than a single level.
		* 
		* @access private
		* @param string $pagetype (default: "core") A boolean style value with the options of core or orphan
		* @param int $tier (default: 0) The numerical tier of the navigation
		* @param int $parent (default: 0) The parent ID for which to build the current level of the menu.
		* @return void
		*/
		//*************************************************************************************************************************************
		private function get_navigation_tree($pagetype="core",$tier=0,$parent=0) {
		
			//INCREMENT TIER BASED ON THE TIER WE ARE WORKING ON 
			$tier++;
			
			//IMPORT THE BLOBAL ZLCMS OBJECT
			global $zlcms;
			
			//CLEAR OUTPUT FOR USE
			$output = "";
			
			//GET LIST OF NAVIGATION IN THIS TIER
			$sql = "SELECT id,user_edit,permalink,user_delete,level,tier,title FROM plugin_content_pages WHERE parent_id=? AND company_id=? AND pagetype=? ORDER BY display";
			$query = $zlcms->database->query($sql,array($parent,$zlcms->company_id(),$pagetype));
			$number = $query->num_rows();

			if($number >= 1) {

				//ADD OUR START LIST
				if($tier == 1) {
					$output .= '<ul class="sortable pageList" data-parent="'.$parent.'">';
				} else {
					$output .= '<ul class="sortable'.$tier.'" data-parent="'.$parent.'">';
				}	
	
				while($result = $query->fetch_assoc()) {		
					
					//DETERMINE IF THE NAVIGATION IS RANKABLE
					if($tier == 1) {
						if($this->config['rank_navigation'] == "true" || $zlcms->level() == 1000) {
							$move = '<span class="iconSprite content_move moveIcon"></span>';
						} else {
							$move = '&nbsp;';
						}
					} else {
						$move = '<span class="iconSprite content_move moveIcon"></span>';
					}
					
					//TOGGLE COLOR BASED ON WHERE WE ARE
					if($tier == 1 && $pagetype == "core") {
						$this->color = "bgDark";
					} else {
						$this->color = ($this->color == "bgLight") ? "bgMid" : "bgLight";
					}
					
					//SHOW IF THE ITEM HAS EDIT PROPERTIES SELECTED // NOTE IT IS AWLAYS EDITABLE BY SUPER ADMIN
					if($result['user_edit'] == "true" && $result['level'] <= $zlcms->level() || $zlcms->level() == 1000) {
						$edit = '<a href="' . $zlcms->base(true) . 'index.php?plugin=' . $zlcms->plugin . '&page=form&id=' . $result['id']. '"><span class="iconSprite editIcon"></span></a>';
					} else {
						$edit = "&nbsp;";
					}	
					
					//SHOW IF THE ITEM HAS EDIT PROPERTIES SELECTED // NOTE IT IS AWLAYS EDITABLE BY SUPER ADMIN
					if($result['user_delete'] == "true" && $result['level'] <= $zlcms->level() || $zlcms->level() == 1000) {
						$delete = '<a href="' . $zlcms->base(true) . 'index.php?plugin=' . $zlcms->plugin . '&page=index&action=content_delete&id=' . $result['id']. '"><span class="iconSprite deleteIcon delete_confirm"></span></a>';
					} else {
						$delete = '&nbsp';
					}
			
					
					$output .= '<li id="id_'.$result['id'].'">
									<div class="listItem '.$this->color.'">
										<div class="forty-sm eighty pr-sm">
											<div class="pageOrder">'.$move.'</div>
											<div class="pageName">'.$result['title'].'</div>
										</div>
										<div class="forty-sm eighty pr-sm">
											<div class="pageUrl"><a href="'.$zlcms->config['base_path'].$result['permalink'].'">/'.$result['permalink'].'</a></div>
										</div>
										<div class="twenty">
											<div class="pageEdit">'.$edit.'</div>					
											<div class="pageDelete">'.$delete.'</div>
										</div>
									</div>';
									

									$output .= $this->get_navigation_tree($pagetype,$tier,$result['id']);
									
					$output .= '</li>';
					
					
					
					
				}
				
					
				//END OUR UL
				if($tier == 1) {
					$output .= '</ul>';
				} else {
					$output .= '</ul>';
				}
				
			}
			
			if(!empty($output)) {
				return $output;
			} else {
				return false;
			} 
			
			
		}
		

		//*************************************************************************************************************************************
		/**
		* index_tabs function. This method is used to set the value for the tabs that display on the top of the content view. It will 
		* automatically select the values based on what is input into the system.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function index_tabs() {
		
			//CHECK THE SESSION VALUE FOR A STORED VALUE OTHERWISE USE THE LOCAL VALUE
			if(!empty($_SESSION['zladmin']['content']['pagetype'])) {
				$this->pagetype = $_SESSION['zladmin']['content']['pagetype'];
			} 
			
			//IF ITS A CORE HIGHLIGHT IT IF ITS AN ORPHAN HIGHLIGHT IT
			if($this->pagetype == "core") {
				$core = " active";
				$orphan = " inactive";
			} else {
				$core = " inactive";
				$orphan = " active";			
			}
		
			//DO THE ORK
			$output = '
				<ul class="subNav">
					<li><a href="#" class="content_index_tab '.$core.'" data-type="core">Navigation Pages</a></li>
					<li><a href="#" class="content_index_tab '.$orphan.'" data-type="orphan">Orphan Pages</a></li>
				</ul>';
				
			//SEND IT OUT
			echo($output);
		
		}
		

		//*************************************************************************************************************************************
		/**
		* load_current_page function. This method load all of the data for the current page if there is one set.
		* 
		* @access public
		* @param mixed $id An integer representing the database ID of the current page to be edited.
		* @return boolean A value of true or false indicating if the page was founded and stored in the $page variable.
		*/
		//*************************************************************************************************************************************
		public function load_current_page($id) {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
		
			//CHECK FOR ID
			if(!empty($id)) {
		
				//QUERY FOR THE CURRENT PAGE CONTENT
				$sql = "SELECT * FROM plugin_content_pages WHERE id=? AND company_id=? AND level <= ?";
				$query = $zlcms->database->query($sql, array($id, $zlcms->company_id(), $zlcms->level()));
				$number = $query->num_rows();
				
				//IF RESULTS LETS SET ARRAY
				if($number == 1) {
					$this->page = $query->fetch_assoc();
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		
		//*************************************************************************************************************************************
		/**
		* output_properties_panel function. This method outputs the properties panel for superadmin users.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function output_properties_panel() {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
		
			//OUTPUT PROPERTIES PANEL FOR SUPERADMIN USERS
			if($zlcms->level() >= 1000) {
			
			
				$output = '
				
				<div class="pane pane4" id="properties_panel">
		
					<div class="full quarter-med half-sm pushd25 pr-sm">
						
						<label class="full">Allow User Subnav *</label>
						<div class="selectBg">
							<select name="allow_subnav">
								<option value="true"' . $zlcms->form->is_selected(@$_POST['allow_subnav'],"true",@$this->page['allow_subnav'],"",true) . '>True</option>
								<option value="false"' . $zlcms->form->is_selected(@$_POST['allow_subnav'],"false",@$this->page['allow_subnav'],"",true) . '>False</option>
							</select>
						</div>
					
					</div>	
					
					<div class="full quarter-med half-sm pushd25 pr-med">
						
						<label class="full">User Delete *</label>
						<div class="selectBg">							
							<select name="user_delete">
								<option value="true"' . $zlcms->form->is_selected(@$_POST['user_delete'],"true",@$this->page['user_delete'],"",true) . '>True</option>
								<option value="false"' . $zlcms->form->is_selected(@$_POST['user_delete'],"false",@$this->page['user_delete'],"",true) . '>False</option>
							</select>
						</div>
						
					</div>
	
					<div class="full quarter-med half-sm pushd25 pr-sm">
						
						<label class="full">Allow User Edit *</label>
						<div class="selectBg">
							<select name="user_edit">
								<option value="true"' . $zlcms->form->is_selected(@$_POST['user_edit'],"true",@$this->page['user_edit'],"",true) . '>True</option>
								<option value="false"' . $zlcms->form->is_selected(@$_POST['user_edit'],"false",@$this->page['user_edit'],"",true) . '>False</option>
							</select>
						</div>
						
					</div>
					
					<div class="full quarter-med half-sm pushd25">
						
						<label class="full">Protect From User *</label>
						<div class="selectBg">
							<select name="user_protect">
								<option value="false"' . $zlcms->form->is_selected(@$_POST['user_protect'],"false",@$this->page['user_protect'],"",true) . '>False</option>
								<option value="true"' . $zlcms->form->is_selected(@$_POST['user_protect'],"true",@$this->page['user_protect'],"",true) . '>True</option>
							</select>
						</div>
						
					</div>
					
					<div class="full half-sm pr-sm">
						<label class="full">Script URL</label>
						<input class="full" type="text" value="' . $zlcms->form->toggle_value(@$this->page['script_url'],@$_POST['script_url'],true) . '" name="script_url">				
					</div>
					<div class="full half-sm">
						<label class="full">Image URL</label>
						<input class="full" type="text" value="' . $zlcms->form->toggle_value(@$this->page['image_url'],@$_POST['image_url'],true) . '" name="image_url">
					</div>
				</div>
			';
								
				echo($output);
			}
		}
		
		
		//*************************************************************************************************************************************
		/**
		* choose_permission_level function. This method outputs a dropdown menu containing selectable permission levels based on the users 
		* permission level.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function choose_permission_level() {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
					
			//MAKE SURE THE USER IS AT LEAST AN ADMIN LEVEL USER
			if($zlcms->level() >= 250) {

				//GET AVAILABLE LEVELS
				$sql = "SELECT * FROM zlcms_levels WHERE company_id=? AND level <= ?";
				$query = $zlcms->database->query($sql,array($zlcms->company_id(),$zlcms->level()));
				$number = $query->num_rows();
					
				//IF THERE ARE SELECTABLE LEVELS SHOW THEM
				if($number >= 1) {
								
					$output = '	
					<div class="full quarter-med half-sm pushd25 pr-sm">
						
						<label class="full">Select User Level *</label>
						<div class="selectBg">
							<select name="level">';
								while($result = $query->fetch_assoc()) {
									$output .= '<option value="' . $result['level'] . '"' . $zlcms->form->is_selected(@$_POST['level'],@$result['level'],@$this->page['level'],"",true) . '>' . $result['text'] . '</option>';
								}
					$output .= '</select>	
						</div>
					</div>';
					
								
				}
							
				//SEND OUTPUT TO BROWSER		
				echo($output);
			}
			
		}
		

		//*************************************************************************************************************************************
		/**
		* choose_password_protect function. This method checks to see if the site has password protection enabled. If so then it will show a 
		* button to password protect pages if not then it does not.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function choose_password_protect() {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms; 
			
			//CHECK TO SEE IF USER HAS ACCESS
			if($zlcms->level() >= 1000 || $this->config['password_protect'] == "true") {
			
				$output = '
					<div class="full quarter-med half-sm pushd25 pr-sm">	
						<label class="full">Password Protect</label>				
						<div class="selectBg">
							<select name="password_protect">
								<option value="false"' . $zlcms->form->is_selected(@$_POST['password_protect'],"false",@$this->page['password_protect'],"",true) . '>False</option>
								<option value="true"' . $zlcms->form->is_selected(@$_POST['password_protect'],"true",@$this->page['password_protect'],"",true) . '>True</option>
							</select>
						</div><!--closing of selectBg-->	
					</div><!--closing of quarter-->			
				';
				
				//SEND OUTPUT TO BROWSER
				echo($output);
				
			}
		}
		

		//*************************************************************************************************************************************
		/**
		* choose_visible function. This method checks to see if the site has password protection enabled. If so then it will show a button to 
		* password protect pages if not then it does not.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function choose_visible() {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms; 
			
			//CHECK TO SEE IF USER HAS ACCESS
			if($zlcms->level() >= 1000 || @$this->page['user_protect'] != 'true') {
			
				$output = '
					<div class="full quarter-med half-sm pushd25 pr-med">
					
						<label class="full">Visibility *</label>
						<div class="selectBg">
							<select name="visible">
								<option value="true"'.$zlcms->form->is_selected(@$_POST['visible'],"true",@$this->page['visible'],'',true).'>True</option>
								<option value="false"'.$zlcms->form->is_selected(@$_POST['visible'],"false",@$this->page['visible'],'',true).'>False</option>
							</select>
						</div>
					</div>	
				';
				
				//SEND OUTPUT TO BROWSER
				echo($output);
				
			}
		}



		//*************************************************************************************************************************************
		/**
		* choose_parent_id function. This method constructs an HTML select menu that has this users parent options for this company
		* 
		* @access public
		* @param string $post_parent_id (default: "") An optional variable indicating the post parent ID if the page is being edited so it can be selected.
		* @return void
		*/
		//*************************************************************************************************************************************
		public function choose_parent_id($post_parent_id="") {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//CHECK TO SEE IF WE HAVE AN ID
			if(empty($post_parent_id)) {
				if(!empty($this->page['parent_id'])) {
					$current_parent_id = $this->page['parent_id'];
				} else {
					$current_parent_id = "0";
				}
			} else {
				$current_parent_id = $post_parent_id;
			} 
			
			
			//LOOK UP TO SEE IF THE USER CAN MOVE THE CURRENTLY ACTIVE PAGE
			if(!empty($this->page['id'])) {
				
				$current_id = $this->page['id'];
				
				//DETERMINE PERMISSION
				if($this->page['user_protect'] == "false") {
					$move = true;
				} else {
					$move = false;
				}
				
			} else {
				$current_id = 0;
				$move = true;
			}
			
			//START MENU
			$output = '<select name="parent_id" class="content_half_100 round" id="parent_id"' . $this->is_protected(true) . '>';
			
			//CHECK TO MAKE SURE THE USER HAS PERMISSION TO MOVE THIS PAGE
			if($move == true || $zlcms->level() >= 1000) {
				
				//START SELECT MENU
				$output .= '<option value="none">- Please Select - </option>'; 
				
				//CHECK CONFIGURATION TO SEE IF USER CAN ADD PAGES
				if($this->config['add_main_navigation'] == "true" || $zlcms->level() >= 1000) {
				
					if($current_parent_id == 0) {
						$selected = ' selected="selected"';
					} else {
						$selected = '';
					}
				
					$output .= '<option value="0"' . $selected . '>Main Navigation Item</option>'; 
				} else {
					
					if(!empty($this->page['id']) && $current_parent_id==0) {
					
						if($current_parent_id == 0) {
							$selected = ' selected="selected"';
						} else {
							$selected = '';
						}
					
						$output .= '<option value="0"' . $selected . '>Main Navigation Item</option>';
					}
					
				}
				
				//SELECT THE PARENT ID
				$output .= $this->build_parent_option_menu($current_id,$current_parent_id,0,0,"");
			
			} else {
				$output  = '<input type="hidden" name="parent_id" value="' . $this->page['parent_id'] . '" />';
				$output .= '<select name="parent_id_holder" class="content_half_100 round" id="parent_id"' . $this->is_protected(true) . '>';
				$output .= '<option value="' . $this->page['parent_id'] . '">-Fixed Position -</option>';
			}
			
			//FINISH SELECT MENU
			$output .= '</select>';
			
			//SEND OUTPUT
			echo($output);
			
		}		

		//*************************************************************************************************************************************
		/**
		* build_parent_option_menu function. This method constructs an HTML select menu that has this users parent options for this company.
		* 
		* @access private
		* @param int $current_id The database ID of the currently active page.
		* @param int $current_parent_id The database ID of the currently active page's parent page.
		* @param int $query_parent_id The parent ID for which we are building the navigation
		* @param int $tier The tier of the navigation for which we are building.  This is a numerical value indicating the level of navigation.
		* @param string $output (default: "") A value that can contain HTML output that will be used as a base for the next iteration of the function.
		* @return string A string containing the HTML for hte menu
		*/
		//*************************************************************************************************************************************
		private function build_parent_option_menu($current_id,$current_parent_id,$query_parent_id,$tier,$output="") {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//ADD 1 TO TIER
			$tier++;
			
			//CHECK TO SEE IF WE SHOULD ADD
			if($tier < $this->config['navigation_levels']) {
			
				//GET LIST OF CURRENT ITEMS
				$sql = "SELECT id,parent_id,title FROM plugin_content_pages WHERE id !=? AND company_id=? AND parent_id=? AND pagetype='core' AND allow_subnav='true'";
				$query = $zlcms->database->query($sql,array($current_id,$zlcms->company_id(),$query_parent_id));
				
				//DONT INCLUDE CURRENT ITEM
				while($result = $query->fetch_assoc()) {
					
					//ADD IN DOTS
					$inject = "";
					for($n=1;$n<=$tier;$n++) {
						if($n == 2) {
							$inject .= "&nbsp;&bull;&nbsp;";
						} else if ($n > 2) {
							$inject .= "&bull;&nbsp;";
						}
					}	
					
					//INJECT SELECTED 
					if($result['id'] == $current_parent_id) {
						$selected = ' selected="selected"';
					} else {
						$selected = '';
					}
					
					//SELECT THE PARENT ID
					$output .= '<option value="' . $result['id'] . '"' . $selected . '>' .  $inject . $result['title'] . '</option>';
					
					//BUILD NEXT TIER
					$output = $this->build_parent_option_menu($current_id,$current_parent_id,$result['id'],$tier,$output);
				}

			}
							
			//RETURN
			return $output;
				
		}

		//*************************************************************************************************************************************
		/**
		* is_update function. This is executed on the content form.  It will use the page ID to determine if we are updating or not and will add
		* a hidden filed to the form with the ID in case of update.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function is_update() {
			if(!empty($this->page['id'])) {
				echo('<input type="hidden" name="id" id="id" value="' . $this->page['id'] . '" />');
			}
		}
		

		//*************************************************************************************************************************************
		/**
		* is_protected function. This function used the page value to determine if the page is locked.  If so then the disabled function will 
		* be applied to certian elements.
		* 
		* @access public
		* @param bool $return (default: false)
		* @return void
		*/
		//*************************************************************************************************************************************
		public function is_protected($return = false) {
			global $zlcms;
			if(!empty($this->page['user_protect'])) {
				if($this->page['user_protect'] == "true" && $zlcms->level() < 1000) {
					$output = ' disabled="disabled"';
					if($return == true) {
						return $output;
					} else {
						echo($output);
					}
				}
			}
		}
		
	}
?>