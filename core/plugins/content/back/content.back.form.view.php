<?PHP 

	$this->content->load_preferences(true); 
	$this->content->load_settings();
	$this->content->load_current_page(@$_GET['id']);

?>


<section class="mainContentWrapper" id="content_plugin">

	<div class="full column borderBottom3px">			
	

<?PHP 
/*
	//page title 
*/
?>
		<form name="content_form" method="post" action="<?PHP $this->get_form_action(); ?>">	
			<div class="mainPageTitle">
				<div class="two-third-med half pr">
					<h1 class="push40"><?PHP if(!empty($_GET['id'])) { ?>Edit Page<?PHP } else { ?>Add Page<?PHP } ?></h1>
				</div><!--closing of full-->
				<div class="third-med half">
					<a href="index.php?plugin=content&page=index" class="button button2 backPage">Back</a>

					<input type="submit" class="button addPage" name="save" value="SAVE CHANGES" />
				</div><!--closing of forty - full-->
			</div><!--closing of mainPageTitle-->
	
	<?PHP 
	/*
		//sub navigation 
	*/
	?>
			
			<div class="full subNavWrapper">
				<span class="mobileDeviceBtn">Plugin Options <span class="iconSprite downArrowLg"></span></span>
				<?PHP $this->content->form_tabs(); ?>
				<ul class="helpList right">
					<li><a href="#"><span class="info iconSprite"></span></a></li>
					<li><a href="#"><span class="video iconSprite"></span></a></li>
				</ul>
				
			</div><!--closing of full - subNavWrapper-->
		</div><!--closing of full-->
	
		<div class="full p25 bg3">			
					
				<div class="pane pane1 active">
					<div class="half-sm full pr-sm">
						
						<label class="full">Page Title *</label>
						<input class="full" type="text" value="<?PHP $this->form->toggle_value(@$this->content->page['title'],@$_POST['title']); ?>" name="title" id="title">
						
						<div class="permaWrapper">
							<label class="full">Permalink *</label>
							<?PHP $this->can_edit_permalink(); ?>
							<input class="full" type="text" value="<?PHP $this->form->toggle_value(@$this->content->page['permalink'],@$_POST['permalink']); ?>" name="permalink" id="permalink" readonly="readonly">
						</div>
						
					</div><!--closing of half-->
					
					<div class="half-sm full wellBg pusht20 pushd20">
						<div class="wellFull dashedBorderBottom1px">					
							<div class="half-med pr-med full">
								<input type="radio" name="pagetype" id="navPageType" value="core"<?PHP $this->content->is_protected(); ?><?PHP $this->form->is_checked(@$_POST['pagetype'],"core",@$this->content->page['pagetype'],"core"); ?>>
								<label for="navPageType" class="color2"><span></span>Navigation Page</label>
							</div><!--closing of half-->
							
							<div class="half-med full">
								<input type="radio" name="pagetype" id="orphanPageType" value="orphan"<?PHP $this->content->is_protected(); ?><?PHP $this->form->is_checked(@$_POST['pagetype'],"orphan",@$this->content->page['pagetype'],"core"); ?>>
								<label for="orphanPageType" class="color2"><span></span>Orphan Page</label>
							</div><!--closing of half-->
						</div><!--closing of wellFull-->
						
						<div class="wellFull">
							<label class="full">Choose Parent *</label>
							<div class="selectBg">
							<?PHP
								if(!empty($_POST)) {
									$parent_id = @$_POST['parent_id'];
								} else {
									$parent_id = @$this->content->page['parent_id'];
								}
		
								$this->content->choose_parent_id($parent_id);
							?>
							</div><!--closing of selectBg-->
						</div><!--closing of wellFull-->
												
					</div><!--closing of half-->
				</div><!--closing of pane-->
				
	<?PHP 
	/*
		Wrapper 3 - meta tag - contains:
						- meta title
						- meta description
						- meta keywords
	*/
	?>			
				<div class="pane pane2" id="url_panel">
				
					<div class="full">
						<label class="full">Url</label>
						<input class="full" type="text" name="url" value="<?PHP $this->form->toggle_value(@$this->content->page['url'],@$_POST['url']); ?>">
					</div><!--closing of full-->
					
				</div><!--closing of pane-->	
	
				<div class="pane pane3" id="metatags_panel">
				
					<div class="full">
						<label class="full">Meta Title</label>
						<input class="full" type="text" value="<?PHP $this->form->toggle_value(@$this->content->page['meta_title'],@$_POST['meta_title']); ?>" name="meta_title">
					</div><!--closing of full-->
					
					<div class="full half-sm pr-sm">
						<label class="full">Meta Keywords</label>
						<textarea name="meta_keywords" class="full minHeight140"><?PHP $this->form->toggle_value(@$this->content->page['meta_keywords'],@$_POST['meta_keywords']); ?></textarea>
						
					</div><!--closing of half-->
	
					<div class="full half-sm">
						<label class="full">Meta Description</label>
						<textarea name="meta_description" class="full minHeight140"><?PHP $this->form->toggle_value(@$this->content->page['meta_description'],@$_POST['meta_description']); ?></textarea>
						
					</div><!--closing of half-->
	
					
				</div><!--closing of pane-->				
				
			
				<?PHP $this->content->output_properties_panel(); ?>
				
	<?PHP 
	/*
		Wrapper 5 - permissions - contains:
						- password ?
						- visible
						- permissions
						- template
	*/
	?>			
				<div class="pane pane5" id="permissions_panel">
					
					<?PHP $this->content->choose_password_protect(); ?>
					
					<?PHP $this->content->choose_visible(); ?>
					
					<?PHP $this->content->choose_permission_level(); ?>
				
				</div><!--closing of half-->
				
				<div class="pane pane6" id="notes_panel">
					<div class="full">
						<label class="full">Notes</label>
						<textarea name="notes" class="full minHeight320"><?PHP $this->form->toggle_value(@$this->content->page['notes'],@$_POST['notes']); ?></textarea>
					</div><!--closing of full-->
				</div>
				
			
	
					<?PHP //$this->editor("content",$this->form->toggle_value(@$this->content->page['content'],@$_POST['content'],true),$this->config['editor_css'],$this->config['editor_width'],600,true); ?>
					
					<?PHP $this->editor("content",$this->form->toggle_value(@$this->content->page['content'],@$_POST['content'],true),$this->config['editor_css'],'',600,true); ?>
				
				<div class="full">
					<?PHP $this->content->is_update(); ?>
					<input type="hidden" name="action" value="content_save" />
					<input type="submit" class="button" name="save" value="SAVE CHANGES" />
					<input type="submit" class="button" name="save_continue" value="SAVE CHANGES AND CONTINUE EDITING" />
				</div><!--closing of full-->
				
			</form>
			
		
	</div><!--closing of p25 full bg3-->



</section>
<section class="lightboxWrapper" id="infoLightbox">
	<div class="full borderBottom3px">
		<h2>Instructions:</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full">
		<p>This page allows you to add / edit website content. Each field allows you to specify more specific information about your page.</p>
		<p><strong>Meta Tags: </strong>Meta tags allow you to provide specific information for search engines.</p>
		<p><strong>URL: </strong>The URL field allows you to create navigational pages that point to an external source.</p>
		<p><strong>Permissions: </strong>The permissions options allow you to choose if a page is visible to the public and what level of user can edit the page.</p>
		<p><strong>Notes: </strong>The notes field allows users to store notes and information for other users.</p>
	</div><!--closing of full-->
</section>

<section class="lightboxWrapper" id="tutorialLightbox">
	<div class="full borderBottom3px">
		<h2>Video Tutorial</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full">
		<iframe width="960" height="720" src="//www.youtube.com/embed/kWdTy2Sw_Mc" frameborder="0" allowfullscreen></iframe>
	</div><!--closing of full-->
</section>
