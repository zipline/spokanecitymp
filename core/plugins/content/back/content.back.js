//**********************************************************************************************************************
/**
 * DOCUMENT: /core/plugins/content/back/content.back.js
 * DEVELOPED BY: Ryan Stemkoski
 * COMPANY: Zipline Interactive
 * EMAIL: ryan@gozipline.com
 * PHONE: 509-321-2849
 * DATE: 4/28/2010
 * DESCRIPTION: This document has all of the javascript functions required for the content plugin.
 */
//***********************************************************************************************************************

var is_sortable = true;
var original_permalink = "";

//***********************************************************************************************************************
//ON DOCUMENT READY FUNCTIONS
//***********************************************************************************************************************
$(function() {
    // Make sure this is the content plugin
    if($("#content_plugin").length > 0){

        //***********************************************************************************************************************
        //THIS FUNCTION USES AJAX TO CREATE A PERMALINK
        //***********************************************************************************************************************
        function content_update_permalink(change,type) {

            var current_permalink = original_permalink;
            var updated_permalink = $('#permalink').val();
            var current_title = $('#title').val();
            var current_id = $('#id').val();

            //TOGGLE VALUE TO USE BASED ON POSITION
            if(updated_permalink) {
                title = updated_permalink;
            } else {
                title = current_title
            }

            //IF THERE IS NO PERMALINK OR WE ARE UPDATING IT RUN FUNCTION.  IF THERE IS A PERMALINK SET DON'T TRY TO SET IT
            if(change == true) {
                if(title != "") {
                    $.post("index.php?plugin=content&page=form", { action: "content_permalink", text: title, id: current_id },
                        function(data){
                            if(data != "" && data != "-") {
                                $('#permalink').val(data);

                                if(type=="permalink") {
                                    $('.permalink_edit').show();
                                    $('#permalink').css("background-color","#F2F2F2");
                                    $('#permalink').prop('readonly', true);
                                }
                            }
                        }
                    );
                }
            }
        }

        //***********************************************************************************************************************
        //IF TITLE CHANGES CHECK TO SEE IF PERMALINK NEEDS UPDATING
        //***********************************************************************************************************************
        $('#title').change(function() {
            var current_permalink = $('#permalink').val();
            if(current_permalink == "") {
                content_update_permalink(true,"title");
            }
        });

        $('#permalink').blur(function() {
            content_update_permalink(true,"permalink");
        });

        //***********************************************************************************************************************
        //UPDATE PARENT SELECT FUNCTION DISABLES OR ENABLES DROPDOWN ON CONTENT PLUGIN
        //***********************************************************************************************************************
        function content_update_parent_select(click) {
            var value = $("input[name='pagetype']:checked").val();
            if(value == "orphan") {
                $('#parent_id').attr("disabled","disabled");
            } else {
                if(click == true) {
                    $('#parent_id').removeAttr("disabled");
                }
            }
        }

        //***********************************************************************************************************************
        //IF ORPHAN PAGE IS SELECTED THEN ENABLE
        //***********************************************************************************************************************
        $("input[name='pagetype']").change(function() {
            content_update_parent_select(true);
        });


        //***********************************************************************************************************************
        // WHEN A CHANGE IS MADE TO THE ORDER OF NAVIGATION RESTRIPE THE ITEMS
        //***********************************************************************************************************************
        function restripe() {

            //SET DEFAULT COLOR
            var using = "bgMid";

            //TOGGLE BACKGROUND COLOR ON ALL CHILD LISTS
            $('.pageList > li ul li').each(function() {
                if(using == "bgMid") { using = "bgLight"; } else { using = "bgMid"; }
                $(this).find('div').removeClass("bgMid").removeClass("bgLight").addClass(using);
            });

        }

        //***********************************************************************************************************************
        //THIS FUNCTION CREATES SORTABLE NAVIGATION ELEMENTS
        //***********************************************************************************************************************
        function content_add_sort() {

            //CHECK TO SEE IF THERE IS AN ACTIVE SORTABLE
            if(is_sortable) {

                //GET THE PARENT AND CURRENT ELEMENTS TO THE CLICKED ITEM
                var parent = $(this).parent('div').parent('div').parent('div').parent('li').parent('ul');
                var current = $(this).parent('div').parent('div').parent('div').parent('li');

                //MAKE TRANSPARENT
                current.animate({ opacity: 0.60 }, 100 );

                //PREVENT ANOTHER SORTABLE
                is_sortable = false;

                //CREATE SORTABLE
                parent.sortable({

                    axis: 'y',
                    update: function() {
                        var parent_id = parent.attr("id");
                        var list = parent.sortable('serialize');
                        $.post("index.php?plugin=content&page=index", { action: "content_rank", parent: parent_id, items: list });
                        //parent.sortable('destroy');
                        is_sortable = true;
                        restripe();
                    },
                    stop: function() {
                        current.animate({ opacity: 1.0 }, 100 );
                        parent.sortable('destroy');
                        is_sortable = true;
                    }

                }).disableSelection();

                //IF THERE IS AN EXISTING SORTABLE SHOW ERROR
            } else {
                zlcms_alert("Sorting Error","You currently have an instance of sorting initiated.  You may only sort one item at a time. Please complete the previous sort and try again.")
            }

        }

        //***********************************************************************************************************************
        //CUSTOM ZLCMS ALERT
        //***********************************************************************************************************************
        function set_pagetype() {
            if(typeof(pagetype) != "undefined") {
                if(pagetype == "core") {
                    $('.core').show();
                    $('.orphan').hide();
                } else {
                    $('.core').hide();
                    $('.orphan').show();
                }
            }
        }



        //***********************************************************************************************************************
        //CREATE MOVEABLE ELEMENT
        //***********************************************************************************************************************
        $('.content_move').bind("mousedown",content_add_sort);


        //***********************************************************************************************************************
        //REMEMBER WHAT TAB IS CLICKED BY THE USER ON THE INDEX PAGE CORE/ORPHAN TOGGLE
        //***********************************************************************************************************************
        $('.content_index_tab').click(function(e) {

            e.preventDefault();

            //GET NEW PAGETYPE
            pagetype = $(this).data('type');

            //ADJUST TAB
            $('.content_index_tab').removeClass("active");
            $(this).addClass("active");

            //ADJUST VIEW
            set_pagetype();

            //UPDATE SESSION SO WE REMEMBER
            $.post("index.php?plugin=content&page=index", { action: "content_switch_pagetype", pagetype: pagetype });

        });



        //***********************************************************************************************************************
        //THIS METHOD CLOSES ALL PANELS
        //***********************************************************************************************************************
        function content_close_panels(panel) {
            for(var i in panel){
                $("#" + panel[i] + "_panel").toggle();
            }
        }


        //***********************************************************************************************************************
        //THIS FUNCTION CLOSES FORM PANELS AS NEEDED
        //***********************************************************************************************************************
        $('.content_form_tab').click(function(e) {
            e.preventDefault();
            var current = $(this).data('type');
            $("#" + current + "_panel").slideToggle("normal");
            $(this).toggleClass("active");
            $.post("index.php?plugin=content&page=form", { action: "content_update_preferences", item: current });


        });



        //DO CLOSE PANELS
        if(typeof(panel) != "undefined") {
            content_close_panels(panel);
        }

        //UPDATE PARENT SELECT
        content_update_parent_select(false);

        //SET THE PAGE TYPE ON LOAD OF THE MAIN INDEX
        set_pagetype();
    }

});