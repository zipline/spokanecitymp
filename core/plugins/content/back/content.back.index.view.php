<?PHP $this->content->get_pagetype(); ?>
<section class="mainContentWrapper" id="content_plugin">
	<div class="full column borderBottom3px">			
		<div class="mainPageTitle">
			<div class="two-third-med half pr">
				<h1>Content</h1>
			</div>
			<div class="third-med half">
				<a href="index.php?plugin=content&page=form" class="button addPage"><span class="iconSprite whitePlus"></span> Add Page</a>
			</div>

		</div>
	
		<div class="full subNavWrapper">
			<span class="mobileDeviceBtn">Plugin Options <span class="iconSprite downArrowLg"></span></span>
			<?PHP $this->content->index_tabs(); ?>
			<ul class="helpList right">
				<li><a href="#"><span class="info iconSprite"></span></a></li>
				<li><a href="#"><span class="video iconSprite"></span></a></li>				
			</ul>
	
		</div>
	</div>

	<div class="full p25 bg3 core">		
		<?PHP $this->content->show_list("core"); ?>
	</div>
	
	<div class="full p25 bg3 orphan">		
		<?PHP $this->content->show_list("orphan"); ?>
	</div>

</section>
<section class="lightboxWrapper" id="infoLightbox">
	<div class="full borderBottom3px">
		<h2>Instructions:</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full" id="instructionsHolder">
		<p>The page below lists all of the pages in your website. You can edit a page by clicking the orange pencil icon and you can delete a page by clicking the red x icon.  The icon to the left the titles allows you to click and drag to re-order the navigation items.</p>
		<p><strong>Navigation Pages</strong><br>This area shows all of the pages that are connected to your website navigation. This area typically contains the homepage, about page, and contact page as well as any other product or service pages.</p>
		<p><strong>Orphan Pages</strong><br>This area shows all of the pages that are not connected to your website navigation. This area typically holds promotional pages, thank you pages, and other pages that cannot be access by clicking a navigation item directly.</p>
	</div><!--closing of full-->
</section>

<section class="lightboxWrapper" id="tutorialLightbox">
	<div class="full borderBottom3px">
		<h2>Video Tutorial</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full" id="videoHolder">
		<iframe width="640" height="480" src="//www.youtube.com/embed/4_Xz7cjpyks" frameborder="0" allowfullscreen></iframe>
	</div><!--closing of full-->
</section>
