<?PHP

	//*****************************************************************************************************************************************
	/**
	*
	* Front-end Content Plugin
	* 
	* @package    ZLCMS
	* @subpackage content
	* @author     Ryan Stemkoski <ryan@gozipline.com>
	* @copyright  2005-2010 Zipline Communications Inc.
	* @version    2.0
	* @link       http://www.ziplineinteractive.com
	*/
	//*****************************************************************************************************************************************
	class content {
		
		var $id = "";
		var $page = array();
		var $level = array();
		
		
		//*************************************************************************************************************************************
		/**
		* This functin serves as a __construct() for the content class. It uses the segment determined in the ZLCMS class and then finds
		* the correct page data to display and use within the other content class functions.
		*/
		//*************************************************************************************************************************************
		function plugin_construct() {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//REDIRECT TO SECURE IF NECESSARY
			$this->secure_redirect();
			
			//LOAD THE CORRECT ID
			$this->set_page();
			
			//BUILD AN ARRAY OF THE LEVELS CURRENTLY IN USE
			$this->build_level($this->id);
			
		}
		
		//*************************************************************************************************************************************
		/**
		* This function redirects the user to a secure version of the page if necessary
		*/
		//*************************************************************************************************************************************
		function secure_redirect() {
			global $zlcms;
			
			if(in_array($zlcms->segments['0'],$zlcms->config['secure_pages']) && ($zlcms->config['secure_pages'][0] != '')) {
				if($_SERVER['SERVER_PORT'] != 443) {
					header("HTTP/1.1 301 Moved Permanently");
					header("Location: https://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]);
					exit();
				}
			} else {
				if($_SERVER['SERVER_PORT'] == 443) {
					header("HTTP/1.1 301 Moved Permanently");
					header("Location: http://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]);
					exit();
				}
			}
		}
				
		//*************************************************************************************************************************************
		/**
		* This function loads with every page. It triggers a number of functions that determine the current page, plugin, and configuration
		* This function also initiates a database connection and loads a number of sub classes from the configuration file.
		* @returns $page string Indicates which template to load for the current page
		*/
		//*************************************************************************************************************************************
		function set_page() {
				
			//IMPORT THE GLOBAL ZLCMS OBJECT
			global $zlcms;
				
			//CLEAR OUTPUT FOR USE
			$output = "";	
					
			//CONTENT
			$content = true;
			
					
			//FIRST LETS SET THE ID FOR THE CURRENT PAGE IF IT DOESN'T EXIST CHOOSE THE HOMEPAGE
			if(!empty($zlcms->segments['0'])) {
			
				//QUERY THE DATABASE TO MAKE SURE THE PAGE EXISTS
				$sql = "SELECT * FROM plugin_content_pages WHERE permalink=? AND company_id=? AND visible='true'";
				$query = $zlcms->database->query($sql, array($zlcms->segments['0'],$zlcms->company['id']));
				$number = $query->num_rows();
				
				//IF IT DOES THEN USE ITS ID IF NOT THEN SEND THIS USER TO THE HOMEPAGE
				if($number >= 1) {
					$result = $query->fetch_assoc();
					$this->page = $result;
					$this->id = $result['id'];
				} else {
					// If the error page is different than the default page then set a 404 header
					if($zlcms->config['error_page'] != $zlcms->config['default_page']){
						header("HTTP/1.0 404 Not Found");
					}
					$this->id = $zlcms->config['error_page'];
					$content = false;
				}	
				
			//IF THERE IS NO ID SET THEN JUST JUMP STRIAGHT ON TO THE DEFAULT PAGE
			} else {
				$this->id = $zlcms->config['default_page'];
				$content = false;
			}
			
			//CHECK TO SEE IF ID IS THE SAME AS PASSED IN THE URL IF SO THEN SET THE PAGE VALUES TO THE QUERY WE USED TO CHECK
			if($content == false) {
				
				//QUERY THE DATABASE TO MAKE SURE THE PAGE EXISTS
				$sql = "SELECT * FROM plugin_content_pages WHERE id=? AND company_id=? AND visible='true'";
				$query = $zlcms->database->query($sql, array($this->id,$zlcms->company['id']));
				$number = $query->num_rows();
				
				if($number == 1) {
					$result = $query->fetch_assoc();
					$this->page = $result;
					$content = true;
				}
				
				//IF THIS IS EXECUTED AN ERROR OCCURED
				if($content == false) {
				
					//LOG ERROR
					$zlcms->log_error("Fatal Error: Could not select a page.");
					
					//STOP THE SCRIPT THIS IS A SECURITY RISK
					die;
				
				}
			}
			
		}
	
		//*************************************************************************************************************************************
		/**
		* build_level()
		* This function recursively builds an array of all pages in association with the current page and above it in the 
		* navigational hierarchy
		*
		* @param integer $input A Numerical value representing the page to be queried.
		* @uses array $this->level $ An array set using this function that contains the current page and the id of all pages before it in the navigational hierarchy
		* @uses boolean $complete Notifies the function if it should continue executing recursively.
		* @uses integer $level The current level we're working on. Level will continue counting through each possible level of the site ane executing the function until it ends.
		*/
		//*************************************************************************************************************************************
		function build_level($input,$level = 1) {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//SET COMPLETE TO FALSE
			$complete = false;
			
			//IF THIS IS THE FIRST LEVEL THEN SET THE ARRAY VALUES
			if($level == "1") {
		
				//LETS NOW SELECT THE MAXIMUM NAVIGATION LEVEL IN THE ZLCMS SYTEM 
				$sql = "SELECT max(tier) as tier FROM plugin_content_pages WHERE company_id=?";
				$query = $zlcms->database->query($sql,array($zlcms->company['id']));
				$number = $query->num_rows;
				$result = $query->fetch_assoc();
				$lookups = $result['tier'];
				
				//NOW FOR EACH LEVEL WE WILL BUILD AN ARRAY VALUE USED FOR LEVELS
				for($counter = 1; $counter < $lookups; $counter++) {
					$this->level[$counter] = intval($input);
				}
			}

			//OK LETS QUERY AND FIND THE DIRECT PARENT OF THIS ITEM
			$sql = "SELECT id,tier,parent_id FROM plugin_content_pages WHERE id=? and company_id=?";
			$query = $zlcms->database->query($sql,array($input,$zlcms->company['id']));
			$result = $query->fetch_assoc();
			
			//BUILD THE ID OF THIS LEVEL
			$this->level[$result['tier']] = intval($result['id']);

			//CHECK TO SEE IF THIS LEVEL HAS SUBNAV IF SO THEN RUN THIS BAD BOY AGAIN			
			if($result['parent_id'] != 0) {
				$nextLevel = $level + 1;
				$this->build_level($result['parent_id'],$nextLevel);
			} else {
				$complete = true;
			}
			
			//IF WE ARE COMPLETE THEN OUTPUT THE RESULTS
			if($complete == true) {
				$this->level;
			}
		}

		/*************************************************************************************************************************************
		 *
		 * This function is called by the main CMS controller to build the meta title. It uses stored variables within the system to produce
		 * the title.
		 *
		 * @param $type string This represent the type of meta information we want to return.
		 * @return string A string that represents the title output by this plugin.
		 *
		 *************************************************************************************************************************************/
		function meta($type) {

			global $zlcms;

			//OUTPUT
			$output = "";

			if($type == "title") {
				if(!empty($this->page['meta_title'])) {
					$output = $this->page['meta_title'];
				} else {

					//CHECK TO SEE IF WE HAVE A TITLE SET IF NOT USE DEFAULT
					if(!empty($this->page['title'])) {
						$output = $this->page['title'] . " > " . $zlcms->company['company'];
					}

				}

			} else if($type == "keywords") {
				if(!empty($this->page['meta_keywords'])) {
					$output = $this->page['meta_keywords'];
				}
			} else if($type == "description") {
				if(!empty($this->page['meta_description'])) {
					$output = $this->page['meta_description'];
				}
			}

			return $output;

		}

		//*************************************************************************************************************************************
		/**
		* This function selects and includes the right template for use on the current page. The tempaltes are located
		* in system/teampltes/pages/.  The select template function uses an array with $key=>$value pairs where $key is the 
		* id of the page to use the custom template on and value is the name of hte template located in /system/tempaltes/pages/.
		* The value name would be the text name such as sample in a template called sample.template.php and an array to show this
		* page id 36 would look like this $this->select_template(array(36,"sample"));. If this array is not passed a simple index/interior
		* tempalte style will be used by default.
		*
		* @param $custom array Array of page ids and corresponding template names.
		* @param $customsection array Array of section headings and corresponding template names 
		*/
		//*************************************************************************************************************************************
		function select_template($custom = "",$customsection = "") {
		
			//GLOBAL ZLCMS
			global $zlcms;
		
			//SET REGULAR A TRUE
			$regular = true;
		
			//LOOK FOR A SECTION TEMPLATE. IF ONE EXISTS SET IT
			if(is_array($customsection)) {
				foreach($customsection as $key=>$value) {
					if(in_array($key,$zlcms->content->level)) {
						if(is_file($zlcms->template_path_relative . "views/{$value}.view.php")) {
							$template = $zlcms->template_path_relative . "views/{$value}.view.php";
							$regular = false; //IF WE MAKE IT HERE THIS IS A CUSTOM PAGE SO SHOW IT AND SKIP REGULAR TEMPLATES
						}
					}
				}
			}

			//NOW LOOK FOR A PGE SPECIFIC TEMPLATE. IF ONE EXISTS SET IT (OVERRIDES SECTION TEMPLATE)
			if(is_array($custom)) {
				foreach($custom as $key=>$value) {
					if($this->id == $key) {
						if(is_file($zlcms->template_path_relative . "views/{$value}.view.php")) {
							$template = $zlcms->template_path_relative . "views/{$value}.view.php";
							$regular = false; //IF WE MAKE IT HERE THIS IS A CUSTOM PAGE SO SHOW IT AND SKIP REGULAR TEMPLATES
						}
					}
				}
			}
		
			//IF THIS IS A REGULAR PAGE GO DO YOUR BUSIENSS OTHERWISE INCLUDE THE CUSTOM TEMPLATE WE FOUND BEFORE
			if($regular == true) {
				if($this->id == $zlcms->config['default_page']) {
					include($zlcms->template_path_relative . "views/index.view.php");
				} else {
					include($zlcms->template_path_relative . "views/interior.view.php");
				}
			} else {
				include($template);
			}	
		}

		//*************************************************************************************************************************************
		/**
		* output_content();
		* Outputs the page content and includes the script_url if there is one present. Any specialized functions like plugins
		* or contact forms will display using the script_url. If there aren't any for this page it will only show content.
		*
		* @param $custom array Array of page ids and corresponding tempalte names. 
		*/
		//*************************************************************************************************************************************
		function output_content() {
			
			//IF VISIBLE SHOW 
			if($this->page['visible'] == "true") {
			
				//OUTPUT THE CONTENT TO THE 
				if(!empty($this->page['content'])) {
					echo($this->page['content']);
				}
				
				//INCLUDE THE SCRIPT URL
				if(!empty($this->page['script_url'])) {
					include($this->page['script_url']);
				}
			
			}
		}
		
			
	}
		
?>