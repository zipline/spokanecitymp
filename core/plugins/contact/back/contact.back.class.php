<?PHP
	// ZDO: SET UP ABILITY TO MANAGE ATTACHMENTS

	/*****************************************************************************************************************************************
	 *
	 * Back-end Contact Plugin
	 *
	 * @package    ZLCMS
	 * @subpackage contact
	 * @author     Ryan Stemkoski <ryan@gozipline.com>, Kraig Lutz <kraig@ziplineinteractive.com>
	 * @copyright  2005-2014 Zipline Communications Inc.
	 * @version    3.0
	 * @link       http://www.ziplineinteractive.com
	 *
	 *****************************************************************************************************************************************/
	class contact {

		var $title;

		/*************************************************************************************************************************************
		 * This function is used to establish the base options for this plugin, primarily setting the title and image size variables for use throughout the plugin
		 *************************************************************************************************************************************/
		function plugin_construct(){
			$this->title = "contact";
		}

		/*************************************************************************************************************************************
		 *
		 * This function is used to process get or post requests within this plugin.
		 * @param $action string Is a string indicating which action we are trying to process.
		 *
		 *************************************************************************************************************************************/
		function process_request($action){
			global $zlcms;
			if($zlcms->is_logged()){
				if($action == $this->title . "_settings_submit"){
					$this->handle_settings_submit();
					die;
				}else if($action == $this->title . "_csv_form"){
					$this->generate_csv();
					die;
				}
			}
		}

		/*************************************************************************************************************************************
		*
		* This function is used to get the settings info for each form in the database
		*
		*************************************************************************************************************************************/
		function fetch_form_info(){
			global $zlcms;

			$return = false;

			$sql = "SELECT id,form_name,form_url,email_subject FROM plugin_contact_forms WHERE company_id=?";
			$query = $zlcms->database->query($sql,array($zlcms->company_id()));
			$num = $query->num_rows();

			if($num > 0){
				while($result = $query->fetch_assoc()){
					$return[$result['id']] = $result;

					$sql_emails = "SELECT email_address,email_preference FROM plugin_contact_form_emails WHERE form_id=?";
					$query_emails = $zlcms->database->query($sql_emails,array($result['id']));
					$num_emails = $query_emails->num_rows();

					if($num_emails > 0){
						while($result_emails = $query_emails->fetch_assoc()){
							$return[$result['id']]['emails'][] = $result_emails;
						}
					}
				}
			}

			return $return;
		}

		/**************************************************
		*
		* This function is used to get all form submissions from the database 
		*
		**************************************************/
		function get_submissions(){
			global $zlcms;
			$submissions = 'There are currently no form submissions.';

			$zebra = array('bgMid', 'bgLight');
			$n = 1;

			// Get all the form submissions from the database order by submission date
			$sql = "SELECT a.id,a.submission_date,b.form_name FROM plugin_contact_submissions a JOIN plugin_contact_forms b ON a.form_id=b.id WHERE b.company_id=? ORDER BY a.submission_date DESC";
			$query = $zlcms->database->query($sql,array($zlcms->company_id()));
			$num = $query->num_rows();

			if($num > 0){
				$submissions = array();
				while($result = $query->fetch_assoc()){
					$name = '';
					$fname = '';
					$lname = '';

					// For each submission get the full submission details
					$sql_details = "SELECT field_name, field_value FROM plugin_contact_submission_details WHERE submission_id=?";
					$query_details = $zlcms->database->query($sql_details,array($result['id']));
					$num_details = $query_details->num_rows();

					if($num_details > 0){
						while($result_details = $query_details->fetch_assoc()){
							if($result_details['field_name'] == "contactName"){
								$name = $result_details['field_value'];
							} elseif($result_details['field_name'] == "first_name"){
								$fname = $result_details['field_value'];
							} elseif($result_details['field_name'] == "last_name"){
								$lname = $result_details['field_value'];
							}
						}
					}

					if(empty($name) && !empty($fname) && !empty($lname)){
						$name = $fname . ' ' . $lname;
					}

					$submissions[] = array(
						"id"				=> $result['id'],
						"contact_name" 	=> $name,
						"form"			=> $result['form_name'],
						"submission_date"	=> $result['submission_date'],
						"css"			=> $zebra[$n%2]
					);

					$n++;
				}
			}

			return $submissions;

		}

		/*************************************************************************************************************************************
		 *
		 * This function is used to retrieve details on a particular submission from the database
		 * @param $id integer A value representing the database ID of the current submission we are looking up.
		 * @return string/array The no results found message or an array containing the results.
		 *
		 *************************************************************************************************************************************/
		function retrieve_item_details($id){
			global $zlcms;
			$return = false;

			if(!empty($id)){
				$sql = "SELECT field_name,field_value FROM plugin_contact_submission_details WHERE submission_id=?";
				$query = $zlcms->database->query($sql,array($id));
				$num = $query->num_rows();

				if($num > 0){
					$return = array();
					while($result = $query->fetch_assoc()){
						$return[] = $result;
					}
				}
			}

			return $return;

		}

		/**************************************************
		*    This function is used to get the form names from the database for use in the google chart as pretty labels.
		**************************************************/
		function get_submission_chart_columns(){
			global $zlcms;

			$return = false;

			$sql = "SELECT form_name FROM plugin_contact_forms WHERE company_id=? ORDER BY form_name";
			$query = $zlcms->database->query($sql,array($zlcms->company_id()));
			$num = $query->num_rows();

			if($num > 0){
				$return = array();
				while($result = $query->fetch_assoc()){
					$return[] = '"' . $result['form_name'] . '"';
				}
			}

			return $return;
		}

		/**************************************************
		*    This function is used to get the number of daily submissions for each form over the past 30 days.
		**************************************************/
		function get_submission_chart_data(){
			global $zlcms;

			$data = array();
			$form_ids = array();

			$today = date("Y-m-d");
			$first = date("Y-m-d", strtotime('- 30 days'));

			// Get all form id numbers
			$sql_forms = "SELECT id FROM plugin_contact_forms WHERE company_id=? ORDER BY form_name";
			$query_forms = $zlcms->database->query($sql_forms,array($zlcms->company_id()));
			$num_forms = $query_forms->num_rows();

			if($num_forms > 0){
				while($result_forms = $query_forms->fetch_assoc()){
					$form_ids[] = $result_forms['id'];
					// Gather the counts by date for each form
					$sql = "SELECT COUNT(form_id) as submissions, DATE(submission_date) as date FROM plugin_contact_submissions WHERE DATE(submission_date) >= DATE(?) AND DATE(submission_date) <= DATE(?) AND form_id = ? GROUP BY DATE(submission_date)";
					$query = $zlcms->database->query($sql,array($first, $today, $result_forms['id']));
					$num = $query->num_rows();

					if($num > 0){
						while($result = $query->fetch_assoc()){
							$submissions[$result['date']][$result_forms['id']] = $result['submissions'];
						}
					}
				}
			}

			for($i=30;$i>=0;$i--){
				$date = date("Y-m-d", strtotime('- ' . $i . 'days'));
				$output = "['" . date("n/j", strtotime($date)) . "'";

				$count = 1;
				foreach($form_ids as $id){
					if(!empty($submissions[$date][$id])){
						$num_subs = $submissions[$date][$id];
					} else {
						$num_subs = 0;
					}

					$output .= ', ' . $num_subs;

					if($count == $num_forms){
						$output .= ']';
					}

					$count++;
				}

				$data[] = $output;
			}

			return $data;

		}

		/**************************************************
		*
		* This function is used to save the settings for the submitted form
		*
		**************************************************/
		private function handle_settings_submit(){
			global $zlcms;

			$email_query = false;
			$subject_query = false;

			if(!empty($_POST['email_address'])){
				// Remove any previously set email addresses for this form
				$sql_delete = "DELETE FROM plugin_contact_form_emails WHERE form_id=?";
				$query_delete = $zlcms->database->query($sql_delete,array($_POST['form_id']));

				if($query_delete){
					$num_emails = count($_POST['email_address']);
					for($i=0;$i<$num_emails;$i++){
						$values = array(
							"form_id"			=> $_POST['form_id'],
							"email_address"	=> $_POST['email_address'][$i],
							"email_preference"	=> $_POST['email_preference'][$i]
						);

						$fields = $zlcms->database->convert_array_to_fields($values,array());
						if(!empty($_POST['email_address'][$i])){
							$email_query = $zlcms->database->insert_auto_helper($fields,$values,"plugin_contact_form_emails");
						}
					}
				}
			}

			if(!empty($_POST['email_subject'])){
				$subject_query = $zlcms->database->update_auto_helper(array("email_subject"),$_POST,"plugin_contact_forms","WHERE id=?",$arguments = array($_POST['form_id']));
			}

			if($email_query && $subject_query){
				echo("success");
			} else {
				echo("error");
			}
		}

		//*************************************************************************************************************************************
		/**
		 * This function generates a csv file of all the investors in the system
		 */
		//*************************************************************************************************************************************
		public function generate_csv(){
			global $zlcms;

			$where_clause = '';
			$where_array = array();

			if(!empty($_POST['form_filter'])) {
				$where_clause .= 's.form_id = ?';
				$where_array[] = $_POST['form_filter'];
				if(!empty($_POST['start_date'])) {
					$where_clause .= ' AND s.submission_date >= ?';
					$where_array[] = $_POST['start_date'] . ' 00:00:01';
					if(!empty($_POST['end_date'])) {
						$where_clause .= ' AND s.submission_date <= ?';
						$where_array[] = $_POST['end_date'] . ' 23:59:59';
					}
				}
			}else if(!empty($_POST['start_date'])) {
				$where_clause .= ' s.submission_date >= ?';
				$where_array[] = $_POST['start_date'] . ' 00:00:01';
				if(!empty($_POST['end_date'])) {
					$where_clause .= ' AND s.submission_date <= ?';
					$where_array[] = $_POST['end_date'] . ' 23:59:59';
				}
			}else{
				$where_clause .= ' s.submission_date <= ?';
				if(!empty($_POST['end_date'])) {
					$where_array[] = $_POST['end_date'] . ' 23:59:59';
				}else{
					$where_array[] = date('Y-m-d') . ' 23:59:59';
				}
			}

			$sql = "SELECT d.*, f.form_name, s.id as form_id, s.submission_date FROM plugin_contact_submission_details AS d JOIN plugin_contact_submissions s ON d.submission_id = s.id JOIN plugin_contact_forms f ON s.form_id = f.id WHERE {$where_clause} ORDER BY s.submission_date DESC, d.id";
			$query = $zlcms->database->query($sql,$where_array);
			$num = $query->num_rows();

			$filename = 'contactCSV-' . date("Ymd") . '.csv';
			header('Content-Type: text/csv; charset=utf-8');
			header('Content-Disposition: attachment; filename=' . $filename);
			$output = fopen('php://output', 'w');

			if($num > 0){
				// Clear an previous error message before we get to far
				//header("Location: " . $zlcms->base(true) . "index.php?plugin={$this->title}&page=export");


				// We have results so start building the csv file
				$fields = array(
					"form_id"			=> "ID",
					"form_name"			=> "Form",
					"field_name"		=> "Field",
					"field_value"		=> "Info",
					"submission_date"	=> "Date",
				);

				fputcsv($output, $fields);

				$count = 0;
				while($result = $query->fetch_assoc()){
					//SET INITIAL SUBMISSION ID FOR EASE OF READING THE CSV
					if($count === 0) {
						$row_id = $result['submission_id'];
					}

					if($row_id == $result['submission_id']) {
						// Format the results before adding them to the csv
						$rows[] = array(
							"form_id"			=> $result['form_id'],
							"form_name"			=> $result['form_name'],
							"field_name"		=> $result['field_name'],
							"field_value"		=> $result['field_value'],
							"submission_date"	=> $result['submission_date']
						);
					}else{
						// add a blank row after every completed form
						$rows[] = array(
							"form_id"			=> ' ',
							"form_name"			=> ' ',
							"field_name"		=> ' ',
							"field_value"		=> ' ',
							"submission_date"	=> ' '
						);
						$rows[] = array(
							"form_id"			=> $result['form_id'],
							"form_name"			=> $result['form_name'],
							"field_name"		=> $result['field_name'],
							"field_value"		=> $result['field_value'],
							"submission_date"	=> $result['submission_date']
						);
					}
					$count++;
					$row_id = $result['submission_id'];
				}

				// Add each row to the csv file
				foreach($rows as $row){
					fputcsv($output, $row);
				}

			}else{
				$message = array("The submission filter returned no results. Please try different parameters");
				fputcsv($output, $message);
			}

			// Close it up
			fclose($output);
			die;

		}

	}
?>
