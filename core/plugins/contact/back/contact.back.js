//**********************************************************************************************************************
/*
 * DOCUMENT: /core/plugins/contact/back/contact.back.js
 * DEVELOPED BY: Ryan Stemkoski, Kraig Lutz
 * COMPANY: Zipline Interactive
 * EMAIL: ryan@gozipline.com, kraig@ziplineinteractive.com
 * PHONE: 509-321-2849
 * DATE: 4/06/2014
 * DESCRIPTION: This document has all of the javascript functions required for the contact plugin.
 */
//***********************************************************************************************************************


//***********************************************************************************************************************
//ON DOCUMENT READY FUNCTIONS
//***********************************************************************************************************************
$(function() {

    // Check that we are on the contact index page
    if($('#contact_chart').length > 0){

        // Set a callback to run when the Google Visualization API is loaded.
        google.setOnLoadCallback(draw_contact_chart);

        $(window).resize(function(){
            draw_contact_chart();
        });

    }

    // Check that we are on the contact settings page
    if($('#contact_settings').length > 0){

        // Handle contact settings form submissions
        $('#contact_settings form').submit(function(e){
            var post_array = $(this).serializeArray();

            $.post("index.php?plugin=contact&page=settings", post_array, function(data){
                if(data == "success"){
                    window.location.assign("index.php?plugin=contact&page=settings&success=Your%20settings%20have%20been%20saved.");
                } else {
                    alert("There was a problem saving the settings. Please try again.")
                }
            });

            e.preventDefault();
        });

		// Handle add email to form click event
	    function toggleRemoveButtons(){
		    $('form').each(function(){
			    if(($('.input_block',this).length - 1) === 1){
				    $('.removeEmail',this).toggle(false);
			    }else{
				    $('.removeEmail',this).toggle(true);
			    }
		    });
	    }

	    $('.add_email').click(function(e){
		    var form = $(this).parent("div").parent("form");
		    var last = form.find('.input_block:last');
		    var num = last.data("num") + 1;

		    last.after('<div class="full input_block" data-num="' + num + '">' +
			    '<div class="half-med full pr-med">' +
			    '<label class="full" for="email_address_' + num + '">Email Address</label>' +
			    '<input class="full" type="email" id="email_address_' + num + '" value="" name="email_address[]" required />' +
			    '</div>' +
			    '<div class="half-med full pr-med">' +
			    '<div class="full">' +
			    '<label class="full" for="email_preference_' + num + '">Preferred Recipient Type</label>' +
			    '</div>' +
			    '<div class="half pr">' +
			    '<div class="selectBg">' +
			    '<select name="email_preference[]" id="email_preference_' + num + '">' +
			    '<option value="to">To</option>' +
			    '<option value="cc">CC</option>' +
			    '<option value="bcc">BCC</option>' +
			    '</select>' +
			    '</div>' +
			    '</div>' +
			    '<div class="half">' +
			    '<a href="#" class="removeEmail"><i class="iconSprite deleteIcon"></i></a>' +
			    '</div>' +
			    '</div>' +
			    '</div>');

		    e.preventDefault();
	    });

	    $(document).on('click', '.removeEmail', function(e){
		    e.preventDefault();

		    if(confirm('Are you sure you want to remove this recipient?\r\nAfter removing a recipient, remember to press the save button to finalize the removal.')){
			    $(this).closest('.input_block').remove();
			    toggleRemoveButtons();
		    }
	    });

	    toggleRemoveButtons();
    }

});
