<?PHP
	// Get the class name for this plugin
	$plugin = $this->plugin;

	$page_title = 'Submission Details';

	$details = $this->$plugin->retrieve_item_details(@$_GET['id']);
?>

<section class="mainContentWrapper">	
	<form id="<?PHP echo($plugin); ?>_item" action="<?PHP $this->get_form_action(); ?>" method="post">
		<div class="full column borderBottom3px">
			<div class="mainPageTitle">
				<div class="two-third-med half pr">
					<h1 class="push40"><?PHP echo($page_title); ?></h1>
				</div><!--closing of full-->
				<div class="third-med half">
					<a href="/zlcms/index.php?plugin=<?PHP echo($plugin); ?>&page=index" class="button button2 backPage">Back</a>
				</div><!--closing of forty - full-->
			</div><!--closing of mainPageTitle-->

			<div class="full subNavWrapper">
				<?PHP
				// IF YOU WOULD LIKE TO ADD MULTIPLE TABS (i.e. a manage categories tab) THEN YOU NEED TO ADD THE FOLLOWING SPAN AND UL CODE BLOCK
				/*
					<span class="mobileDeviceBtn">Plugin Options <span class="iconSprite downArrowLg"></span></span>
					<ul class="subNav">
						<li class="active"><a href="#">Core Pages</a></li>
						<li><a href="#">Orphan Pages</a></li>
					</ul>
				 */
				?>
				<ul class="helpList right">
					<li><a href="#"><span class="info iconSprite"></span></a></li>
					<li><a href="#"><span class="video iconSprite"></span></a></li>
				</ul>
			</div><!--closing of full - subNavWrapper-->
		</div>
		<div class="full p25 bg3">
			<?PHP
				if($details){
					$count = 1;
					foreach($details as $item){
						$mod = $count % 2;
						if($mod){
							echo('<div class="full">');
						}

						if(substr($item['field_name'], 0, 5) == "file_"){
							// We are working with a file so we need to display a download link to the file
							$file_info = explode(" | ", $item['field_value']);

							echo('
								<div class="half-med full pr-med">
									<label class="full" for="' . $item['field_name'] . '">Attached File</label>
									<a class="button" href="' . $this->file->show_file('contact',$file_info[1], true) . '" target="_blank" >' . $file_info[0] . '</a>
								</div>
							');
						} else {
							echo('
								<div class="half-med full pr-med">
									<label class="full" for="' . $item['field_name'] . '">' . $item['field_name'] . '</label>
									<input class="full" type="text" disabled="disabled" id="' . $item['field_name'] . '" value="' . $item['field_value'] . '" name="' . $item['field_name'] . '" />
								</div>
							');
						}

						if(!$mod){
							echo('</div>');
						}

						$count++;
					}
				}
			?>
		</div><!--closing of p25 full bg3-->
	</form>
</section>

<?PHP //BEGIN INFORMATIONAL LIGHTBOX (INFO ABOUT HOW TO USE THIS FORM/PLUGIN GOES HERE) ?>
<section class="lightboxWrapper" id="infoLightbox">
	<div class="full borderBottom3px">
		<h2>Instructions:</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full" id="instructionsHolder">
		<p>Use this form to add/edit this slide.</p>
		<p><strong>Links</strong><br>If you wish to link to content located at another website, or if you wish to have your link open in a new page, you will need to use the full url.  </p>
		<p><strong>Images</strong><br>It is considered best practice to re-size your image to match the size listed before uploading. The system will automatically crop images that do not match the listed size, and in some cases this may cause the image to appear different than intended.</p>
	</div><!--closing of full-->
</section>

<?PHP //BEGIN VIDEO LIGHTBOX (VIDEO TUTORIAL FOR THIS FORM/PLUGIN GOES HERE) ?>
<section class="lightboxWrapper" id="tutorialLightbox">
	<div class="full borderBottom3px">
		<h2>Video Tutorial</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full" id="videoHolder"></div><!--closing of full-->
</section>
