<?PHP
	// Get the class name for this plugin
	$plugin = $this->plugin;
	// Grab all forms for export dropdown list
	$forms = $this->$plugin->fetch_form_info();
?>
<section class="mainContentWrapper" id="<?PHP echo($plugin); ?>">
	<div class="full column borderBottom3px">
		<div class="mainPageTitle">
			<div class="two-third-med half pr">
				<h1 class="push40"><?PHP echo($plugin); ?></h1>
			</div><!--closing of full-->
			<div class="third-med half">
				<!--				<a href="index.php?plugin=--><?php //echo($plugin); ?><!--&amp;page=form" class="button addPage"><span class="iconSprite whitePlus"></span>Add Item</a>-->
			</div><!--closing of forty - full-->
		</div><!--closing of mainPageTitle-->

		<div class="full subNavWrapper">

			<?PHP
				// IF YOU WOULD LIKE TO ADD MULTIPLE TABS (i.e. a manage categories tab) THEN YOU NEED TO ADD THE FOLLOWING SPAN AND UL CODE BLOCK
			?>
			<span class="mobileDeviceBtn">Plugin Options <span class="iconSprite downArrowLg"></span></span>
			<ul class="subNav">
				<li><a class="plugin_index_tab inactive" href="index.php?plugin=<?PHP echo($plugin); ?>&amp;page=index">Contacts</a></li>
				<li><a class="plugin_index_tab inactive" href="index.php?plugin=<?PHP echo($plugin); ?>&amp;page=settings">Settings</a></li>
				<li><a class="plugin_index_tab active" href="index.php?plugin=<?PHP echo($plugin); ?>&amp;page=export">Export</a></li>
			</ul>
			<ul class="helpList right">
				<li><a href="#"><span class="info iconSprite"></span></a></li>
				<li><a href="#"><span class="video iconSprite"></span></a></li>
				<li class="pageNumb">1 of 1</li>
				<li><a href="#"><span class="iconSprite arrowLeft"></span></a></li>
				<li><a href="#"><span class="iconSprite arrowRight"></span></a></li>
			</ul>
		</div><!--closing of full - subNavWrapper-->
	</div><!--closing of full-->

	<div class="full p25 bg3 plugin_content">

		<form id="csvForm" class="interiorForm" action="<?php $this->base(); ?>index.php?plugin=contact&page=export" enctype="multipart/form-data" method="post">
			<div class="full">
				<p>Use this form to export data for all contact form submissions. The data will be exported in the form of a csv file - as long as entries exist within the parameters.</p>
				<div class="third-sm pr-sm full">
					<label class="full">Start Date:</label>
					<input type="text" name="start_date" maxlength="15" value="<?PHP echo(@$_POST['start_date']); ?>" class="full choose_date" />
				</div>
				<div class="third-sm pr-sm full">
					<label class="full">End Date:</label>
					<input type="text" name="end_date" maxlength="15" value="<?PHP echo(@$_POST['end_date']); ?>" class="full choose_date" />
				</div>
				<div class="third-sm full">
					<div class="full">
						<label class="full">Form Name:</label>
					</div>
					<div class="full">
						<div class="selectBg">
							<select name="form_filter" id="form_filter">
								<option value="">All Forms</option>
								<?php foreach($forms as $form) {
									echo('<option value="' . $form['id'] . '">' . $form['form_name'] . '</option>');
								} ?>
							</select>
						</div>
					</div>
				</div>
				<div class="full">
					<div class="submitWrapper">
						<input type="hidden" name="action" value="contact_csv_form" />
						<input type="submit" value="Submit" name="submit_button round" class="button">
					</div>
				</div>
			</div><!--closing of full-->
		</form>

	</div>

</section>

<?PHP //BEGIN INFORMATIONAL LIGHTBOX (INFO ABOUT HOW TO USE THIS FORM/PLUGIN GOES HERE) ?>
<section class="lightboxWrapper" id="infoLightbox">
	<div class="full borderBottom3px">
		<h2>Instructions:</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full" id="instructionsHolder"></div><!--closing of full-->
</section>

<?PHP //BEGIN VIDEO LIGHTBOX (VIDEO TUTORIAL FOR THIS FORM/PLUGIN GOES HERE) ?>
<section class="lightboxWrapper" id="tutorialLightbox">
	<div class="full borderBottom3px">
		<h2>Video Tutorial</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full" id="videoHolder"></div>
</section>