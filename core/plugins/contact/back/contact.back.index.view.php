<?PHP
	// Get the class name for this plugin
	$plugin = $this->plugin;

	$submissions = $this->$plugin->get_submissions();
?>
<section class="mainContentWrapper" id="<?PHP echo($plugin); ?>">
	<div class="full column borderBottom3px">
		<div class="mainPageTitle">
			<div class="two-third-med half pr">
				<h1 class="push40"><?PHP echo($plugin); ?></h1>
			</div><!--closing of full-->
			<div class="third-med half">
<!--				<a href="index.php?plugin=--><?php //echo($plugin); ?><!--&amp;page=form" class="button addPage"><span class="iconSprite whitePlus"></span>Add Item</a>-->
			</div><!--closing of forty - full-->
		</div><!--closing of mainPageTitle-->

		<div class="full subNavWrapper">

			<?PHP
				// IF YOU WOULD LIKE TO ADD MULTIPLE TABS (i.e. a manage categories tab) THEN YOU NEED TO ADD THE FOLLOWING SPAN AND UL CODE BLOCK
			?>
			<span class="mobileDeviceBtn">Plugin Options <span class="iconSprite downArrowLg"></span></span>
			<ul class="subNav">
				<li><a class="plugin_index_tab active" href="index.php?plugin=<?PHP echo($plugin); ?>&amp;page=index">Contacts</a></li>
				<li><a class="plugin_index_tab inactive" href="index.php?plugin=<?PHP echo($plugin); ?>&amp;page=settings">Settings</a></li>
				<li><a class="plugin_index_tab inactive" href="index.php?plugin=<?PHP echo($plugin); ?>&amp;page=export">Export</a></li>
			</ul>
			<ul class="helpList right">
				<li><a href="#"><span class="info iconSprite"></span></a></li>
				<li><a href="#"><span class="video iconSprite"></span></a></li>
				<li class="pageNumb">1 of 1</li>
				<li><a href="#"><span class="iconSprite arrowLeft"></span></a></li>
				<li><a href="#"><span class="iconSprite arrowRight"></span></a></li>
			</ul>
		</div><!--closing of full - subNavWrapper-->
	</div><!--closing of full-->

	<div class="full bg1 plugin_content">
		<script>
			function draw_contact_chart(){

				var contact_data = new Array();
				<?PHP
					$rows = $this->$plugin->get_submission_chart_data();
					foreach($rows as $row){
				?>
						contact_data.push(<?PHP echo($row); ?>);
				<?PHP
					}
				?>

				var data = new google.visualization.DataTable();
				data.addColumn('string','Dates');

				<?PHP
					$columns = $this->$plugin->get_submission_chart_columns();
					foreach($columns as $col){
				?>
						data.addColumn('number', <?PHP echo($col); ?>);
				<?PHP
					}
				?>

				data.addRows(contact_data);

				// Set chart options
				var options = {
					chartArea: {
						top:35, width: '100%', height: '86%'
					},
					titlePosition: 'in',
					axisTitlesPosition: 'in',
					hAxis: {
						textPosition: 'out',
						showTextEvery: 4,
						textStyle: {
							auraColor: 'none',
							color: '#666666',
							fontName: 'Lato'
						},
						gridlines: {
							count:7,
							color: "#F2F2F2"
						}
					},
					vAxis: {
						textPosition: 'in',
						textStyle: {
							auraColor: 'none',
							color: '#666666',
							fontName: 'Lato',
							fontSize: 10
						},
						gridlines: {
							count:10,
							color: "#F2F2F2"
						},
						baselineColor: '#E6E6E6'
					},
					legend: {
						position: 'top'
					}
				};

				// Instantiate and draw our chart, passing in some options.
				var chart = new google.visualization.LineChart(document.getElementById('contact_chart'));
				chart.draw(data, options);

			}
		</script>

		<?PHP
			// DRAW THE CHART AND CHART HEADING - EMILY CAN WE MAKE THIS LOOK A LITTLE MORE LIKE A FULL WIDTH VERSION OF THE DASHBOARD CHART
		?>
		<div class="chartHeader">
			<h2>Form Submissions last 30 days</h2>
		</div>
		<div class="full p10">
			<div id="contact_chart"></div>
		</div>

		<div class="full p25 bg3">
			<?PHP
				$rows = $this->$plugin->get_submission_chart_data(true);
			?>

			<ul class="pageList">
				<li>
					<?PHP
						// BEGIN THE HEADER ROW
					?>
					<div class="listItem bgDark">
						<div class="twenty pr">
							<div class="pageOrder"></div>
							<div class="pageName">Name</div>
						</div>
						<div class="forty pr">
							<div class="pageName">Form</div>
						</div>
						<div class="twenty pr">
							<div class="pageName">Date</div>
						</div>
					</div><!--closing of listItem-->

					<ul class="sortable">
						<?PHP
							// BEGIN LISTING THE ITEMS FOR THIS VIEW
							if(!is_array($submissions)){
								echo($submissions);
							} else {
								foreach($submissions as $submission){
									?>
									<li id="id_<?PHP echo($submission['id']); ?>">
										<div class="listItem <?PHP echo($submission['css']); ?>">
											<div class="twenty-sm full pr-sm">
												<div class="pageTitle"><?PHP echo($submission['contact_name']); ?></div>
											</div><!--closing of forty-->
											<div class="forty-sm sixty pr-sm">
												<div class="pageTitle"><?PHP echo($submission['form']); ?></div>
											</div><!--closing of forty-->
											<div class="twenty-sm full pr-sm">
												<div class="pageTitle"><?PHP echo($submission['submission_date']); ?></div>
											</div><!--closing of forty-->
											<div class="twenty">
												<div class="pageEdit">
													<a href="<?php $this->base(); ?>index.php?plugin=<?php echo($plugin); ?>&amp;page=form&amp;id=<?php echo($submission['id']); ?>">
														<span class="iconSprite editIcon"></span>
													</a>
												</div>
											</div><!--closing of twenty-->
										</div><!--closing of main list item-->
									</li>
								<?PHP
								}
							}
						?>
					</ul>
				</li>
			</ul>
		</div>
	</div>

</section>

<?PHP //BEGIN INFORMATIONAL LIGHTBOX (INFO ABOUT HOW TO USE THIS FORM/PLUGIN GOES HERE) ?>
<section class="lightboxWrapper" id="infoLightbox">
	<div class="full borderBottom3px">
		<h2>Instructions:</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full" id="instructionsHolder"></div><!--closing of full-->
</section>

<?PHP //BEGIN VIDEO LIGHTBOX (VIDEO TUTORIAL FOR THIS FORM/PLUGIN GOES HERE) ?>
<section class="lightboxWrapper" id="tutorialLightbox">
	<div class="full borderBottom3px">
		<h2>Video Tutorial</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full" id="videoHolder"></div>
</section>