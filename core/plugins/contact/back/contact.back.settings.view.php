<?PHP
	// Get the class name for this plugin
	$plugin = $this->plugin;
	$forms = $this->$plugin->fetch_form_info();
?>
<section class="mainContentWrapper" id="<?PHP echo($plugin); ?>">
	<div class="full column borderBottom3px">
		<div class="mainPageTitle">
			<div class="two-third-med half pr">
				<h1 class="push40"><?PHP echo($plugin); ?></h1>
			</div><!--closing of full-->
			<div class="third-med half">

			</div><!--closing of forty - full-->
		</div><!--closing of mainPageTitle-->

		<div class="full subNavWrapper">

			<?PHP
				// IF YOU WOULD LIKE TO ADD MULTIPLE TABS (i.e. a manage categories tab) THEN YOU NEED TO ADD THE FOLLOWING SPAN AND UL CODE BLOCK
			?>
			<span class="mobileDeviceBtn">Plugin Options <span class="iconSprite downArrowLg"></span></span>
			<ul class="subNav">
				<li><a class="plugin_index_tab inactive" href="index.php?plugin=<?PHP echo($plugin); ?>&amp;page=index">Contacts</a></li>
				<li><a class="plugin_index_tab active" href="index.php?plugin=<?PHP echo($plugin); ?>&amp;page=settings">Settings</a></li>
				<li><a class="plugin_index_tab inactive" href="index.php?plugin=<?PHP echo($plugin); ?>&amp;page=export">Export</a></li>
			</ul>
			<ul class="helpList right">
				<li><a href="#"><span class="info iconSprite"></span></a></li>
				<li><a href="#"><span class="video iconSprite"></span></a></li>
				<li class="pageNumb">1 of 1</li>
				<li><a href="#"><span class="iconSprite arrowLeft"></span></a></li>
				<li><a href="#"><span class="iconSprite arrowRight"></span></a></li>
			</ul>
		</div><!--closing of full - subNavWrapper-->
	</div><!--closing of full-->

	<div class="full p25 bg3 plugin_content" id="contact_settings">
		<?PHP
			if(!empty($forms)){
				foreach($forms as $form){
					echo('
						<h2>' . $form['form_name'] . ' (' . $form['form_url'] . ')</h2>
						<form action="/zlcms/index.php" method="post">
					');

					echo('
						<div class="full input_block">
							<div class="full">
								<label class="full" for="email_subject_' . $form['id'] . '">Email Subject</label>
								<input class="full" type="text" id="email_subject_' . $form['id'] . '" value="' . $form['email_subject'] . '" name="email_subject" />
							</div>
						</div>
					');

					// Build the email edit blocks based on the number of emails
					$num_emails = !empty($form['emails']) ? count($form['emails']) : 0;
					if($num_emails > 0){
						for($i=1;$i<=$num_emails;$i++){
							$email = @$form['emails'][$i-1]['email_address'];
							$type = @$form['emails'][$i-1]['email_preference'];
							$to = $type == "to" ? 'selected="selected"' : '';
							$cc = $type == "cc" ? 'selected="selected"' : '';
							$bcc = $type == "bcc" ? 'selected="selected"' : '';

							echo('
							    <div class="full input_block" data-num="' . $i . '">
							        <div class="half-med full pr-med">
							            <label class="full" for="email_address_' . $i . '">Email Address</label>
							            <input class="full" type="email" id="email_address_' . $i . '" value="' . $email . '" name="email_address[]" required />
							        </div>
							        <div class="half-med full pr-med">
							            <div class="full">
							                <label class="full" for="email_preference_' . $i . '">Preferred Recipient Type</label>
							            </div>
							            <div class="half">
							                <div class="half pr">
							                    <div class="selectBg">
							                        <select name="email_preference[]" id="email_preference_' . $i . '">
							                            <option value="to" ' . $to . '>To</option>
							                            <option value="cc" ' . $cc . '>CC</option>
							                            <option value="bcc" ' . $bcc . '>BCC</option>
							                        </select>
							                    </div>
							                </div>
							                <div class="half">
							                    <a href="#" class="removeEmail"><i class="iconSprite deleteIcon"></i></a>
							                </div>
							            </div>
							        </div>
							    </div>
							');
						}
					} else {
						echo('
						    <div class="full input_block" data-num="1">
						        <div class="half-med full pr-med">
						            <label class="full" for="email_address_1">Email Address</label>
						            <input class="full" type="email" id="email_address_1" value="" name="email_address[]" required />
						        </div>
						        <div class="half">
						            <div class="half pr">
						                <label class="full" for="email_preference_1">Preferred Recipient Type</label>
						                <select name="email_preference[]" id="email_preference_1">
						                    <option value="to">To</option>
						                    <option value="cc">CC</option>
						                    <option value="bcc">BCC</option>
						                </select>
						            </div>
						            <div class="half">
						                <a href="#" class="removeEmail"><i class="iconSprite deleteIcon"></i></a>
						            </div>
						        </div>
						    </div>
						');
					}

					echo('
							<div class="full">
								<a class="button bg1 add_email">Add Email Address</a>
								<input type="hidden" name="action" value="contact_settings_submit" class="button" />
								<input type="hidden" name="form_id" value="' . $form['id'] . '" class="button" />
								<input type="submit" value="Save Changes" class="button" />
							</div>
						</form>
					');

				}
			}
		?>
	</div>

</section>

<?PHP //BEGIN INFORMATIONAL LIGHTBOX (INFO ABOUT HOW TO USE THIS FORM/PLUGIN GOES HERE) ?>
<section class="lightboxWrapper" id="infoLightbox">
	<div class="full borderBottom3px">
		<h2>Instructions:</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full" id="instructionsHolder"></div><!--closing of full-->
</section>

<?PHP //BEGIN VIDEO LIGHTBOX (VIDEO TUTORIAL FOR THIS FORM/PLUGIN GOES HERE) ?>
<section class="lightboxWrapper" id="tutorialLightbox">
	<div class="full borderBottom3px">
		<h2>Video Tutorial</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full" id="videoHolder"></div>
</section>