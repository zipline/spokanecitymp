//**********************************************************************************************************************
/*
 * DOCUMENT: /core/plugins/contact/front/contact.front.js
 * DEVELOPED BY: Ryan Stemkoski, Kraig Lutz
 * COMPANY: Zipline Interactive
 * EMAIL: ryan@gozipline.com, kraig@ziplineinteractive.com
 * PHONE: 509-321-2849
 * DATE: 4/06/2014
 * DESCRIPTION: This document has all of the javascript functions required for the contact plugin.
 */
//***********************************************************************************************************************

//***********************************************************************************************************************
//ON DOCUMENT READY FUNCTIONS
//***********************************************************************************************************************
$(function() {

    // Check that there is a file upload button on this page
    if($('#file').length > 0){
        $('#file').change(function(){
            var filepath = $(this).val().split("\\");
            var filename = filepath.pop();
            $('#file_label').text(filename);
        });
    }

});