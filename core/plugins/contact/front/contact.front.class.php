<?PHP
	/*****************************************************************************************************************************************
	 *
	 * Front-end Contact Plugin
	 *
	 * @package    ZLCMS
	 * @subpackage contact
	 * @author     Ryan Stemkoski <ryan@gozipline.com>, Kraig Lutz <kraig@ziplineinteractive.com>
	 * @copyright  2005-2014 Zipline Communications Inc.
	 * @version    3.0
	 * @link       http://www.ziplineinteractive.com
	 *
	 *****************************************************************************************************************************************/
	class contact {

		//*************************************************************************************************************************************
		/*
		* SOME TIPS TO HELP PREVENT SPAM BOTS FROM SUBMITTING FORMS.
		* Switch the input names on the name and email input fields so that the field labeled name is named email and the field labeled email is named name.
		* Validate the input with the name of name as you would if it were really the email field (because it is).
		* Add an empty hidden input field with name of comments (if you have a visible input with the name comments you will need to change it) and only allow the form submission if the input is actually empty.
		*/
		//*************************************************************************************************************************************

		//*************************************************************************************************************************************
		/*
		* SOME TIPS ON HOW THIS PLUGIN HANDLES FILE UPLOADS.
		* This plugin will automatically handle files.  First it will save the file to the server in the usual manner (in the files/contact folder with a hashed name).
		* Then it will save the file information to the database with the other field/value pairings.  The value will consist of the original filename as well as the new filename (the hashed version that we save) separated by a space vertical pipe space " | ".
		* Finally when the email is built it will contain the attached file (with the original name used).
		*/
		//*************************************************************************************************************************************

		//*************************************************************************************************************************************
		/*
		* SOME TIPS ON WHAT THE BACK END IS EXPECTING IN A FORM
		* The name field(s) should be title name if there is just one or first_name/last_name if they are broken up.
		* Failing to do this will cause the back end index page to appear broken.  If there is no way around this then you can find the back end name functionality on line 107 of the class file.
		*/
		//*************************************************************************************************************************************

		// SET THE DEBUG OPTIONS FOR THE EMAIL (DEBUG EMAILS IS AN ARRAY OF EMAIL ADDRESSES TO USE DURING DEBUGGING)
		// WARNING: SETTING $debug TO TRUE WILL OVERRIDE THE EMAIL ADDRESSES ADDED IN THE BACK END OF THE CONTACT PLUGIN AND WILL ONLY USE THE $debug_emails
		var $debug = false;
		var $debug_emails = array();

		/*************************************************************************************************************************************
		 *
		 * This function is used to process get or post requests within this plugin.
		 * @param $action string Is a string indicating which action we are trying to process.
		 *
		 *************************************************************************************************************************************/
		function process_request($action){
			if($action == "example_form_submit"){
				$this->validate_example_form();
			}
		}

		/*************************************************************************************************************************************
		 *
		 * This function is used to process the location forms used throughout the site.
		 *
		 *************************************************************************************************************************************/
		private function validate_example_form(){
			global $zlcms;

			// SET FORM RULES
			$zlcms->form->set_rule(@$_POST['email'],"empty","You must provide a first name");
			$zlcms->form->set_rule(@$_POST['last_name'],"empty","You must provide a last name");
			$zlcms->form->set_rule(@$_POST['first_name'],"empty|email","You must provide a valid email address");
			$zlcms->form->set_rule(@$_POST['phone'],"empty","You must provide a valid phone number");
			$zlcms->form->set_rule(@$_POST['message'],"empty","You must provide a valid message");

			if(!empty($_POST['comments'])){
				$zlcms->form->error = "An error occurred while submitting the form. Please try again.";
			}

			// PROCESS FORM
			if($zlcms->form->run_rules()) {
				// THE FORM VALIDATED SO LET'S PUT THE EMAIL/NAME FIELDS BACK AND REMOVE THE EMPTY COMMENTS IN THE POST ARRAY
				$name = $_POST['email'];
				$email = $_POST['first_name'];
				$_POST['first_name'] = $name;
				$_POST['email'] = $email;
				unset($_POST['comments']);

				// UNSET ANY FIELDS THAT ARE NO LONGER NEEDED FROM THE POST ARRAY (IE SAVE, SUBMIT)
				unset($_POST['submit']);

				// CONVERT THE INPUT NAMES INTO PRETTY NAMES FOR USE IN THE EMAIL
				$email_fields = array(
					"first_name" 	=> "First Name",
					"last_name" 	=> "Last Name",
					"email"		=> "Email Address",
					"phone"		=> "Phone Number",
					"message"		=> "Message"
				);

				// SET THE PRETTY FROM NAME AND EMAIL ADDRESS FOR USE IN THE EMAIL, THIS ALLOWS THE ADMIN TO REPLY TO THE EMAIL
				$from = array(
					"name"	=> $_POST['first_name'] . ' ' . $_POST['last_name'],
					"email"	=> $_POST['email']
				);

				$this->save_form_data($from, $email_fields);
			} else {
				$zlcms->log_error($zlcms->form->error);
			}

		}

		/*************************************************************************************************************************************
		*
		* This function is used to save the submissions details to the database.
		* @param $from array An array containing the from email and name for use in the email function.
		 * @param $email_fields array An array of input name/pretty name values for use in the email function, if not supplied then the email will use everything from the post array.
		* @param $form_id_override integer The form ID that we want to force the plugin to use. Useful for creating forms on the fly that are all processed the same regardless of form url.
		*
		*************************************************************************************************************************************/
		private function save_form_data($from, $email_fields = array(), $form_id_override = 0){
			global $zlcms;

			$attachments = array();

			// Get the form id that we are working with
			if($form_id_override == 0){
				$sql_form = "SELECT id FROM plugin_contact_forms WHERE form_action=? AND company_id=?";
				$query_form = $zlcms->database->query($sql_form,array($_SERVER['REQUEST_URI'], $zlcms->company_id()));
				$num_form = $query_form->num_rows();

				if($num_form > 0){
					while($result_form = $query_form->fetch_assoc()){
						$form_id = $result_form['id'];
					}
				}
			} else {
				$form_id = $form_id_override;
			}

			if(!empty($form_id)){
				// Insert a new row into the submissions table
				$submission_values = array(
					"form_id"			=> $form_id,
					"submission_date"	=> date("Y-m-d H:i:s")
				);

				$submission_fields = $zlcms->database->convert_array_to_fields($submission_values,array());
				$submission_query = $zlcms->database->insert_auto_helper($submission_fields,$submission_values,"plugin_contact_submissions");

				// If that worked then use the submissions id to insert however many new rows are needed into the submissions details table
				if($submission_query){
					$submission_id = $submission_query->insert_id();
					$saved = true;
					$details_fields = array(
						"submission_id",
						"field_name",
						"field_value"
					);

					// UPLOAD FILES
					if($_FILES){
						$count = 1;
						foreach($_FILES as $file){
							$filename = $zlcms->file->upload("contact",$file);
							$_POST['file_' . $count] = $file['name'] . ' | ' . $filename;
							$attachments[] = array(
								"saved_name"	=> $filename,
								"actual_name"	=> $file['name']
							);
							$count++;
						}
					}

					foreach($_POST as $field_name => $field_value){
						$details_values = array(
							"submission_id"	=> $submission_id,
							"field_name"		=> $field_name,
							"field_value"		=> $field_value
						);

						$details_query = $zlcms->database->insert_auto_helper($details_fields, $details_values, "plugin_contact_submission_details");
						if(!$details_query){
							$saved = false;
						}
					}

					if($saved){
						// If that worked then we can move on to sending the emails
                        $this->build_email($email_fields, $from, $form_id, $attachments);
					} else {
						$zlcms->form->error = "There was a problem saving your submission details. Please refresh the page and try again.";
						$zlcms->log_error($zlcms->form->error);
					}

				}

			} else {
				$zlcms->form->error = "There was a problem saving your submission details. Please refresh the page and try again.";
				$zlcms->log_error($zlcms->form->error);
			}

		}

        //*************************************************************************************************************************************
        /*
         * This function prepares the email to be sent based on passed values and the post array.
         * @param $email_fields array An array of input name/pretty name values
         * @param $from array An array containing the from email and name
         * @param $form_id integer The id of the form we are working with, used to get the email settings
         * @param $attachments array An array containing any attachments
         */
        //*************************************************************************************************************************************
        private function build_email($email_fields, $from, $form_id, $attachments){

            global $zlcms;

            // SET THE LIBRARY CLASS NAME TO A VARIABLE SO WE CAN CALL FUNCTIONS FROM WITHIN THE LIBRARY CLASS IF WE NEED TO
            $library_class = $zlcms->company['class'];

            // Get the subject and email addresses for use with this email
            $sql_subject = "SELECT email_subject, thank_you_page FROM plugin_contact_forms WHERE id=?";
            $query_subject = $zlcms->database->query($sql_subject,array($form_id));
            $num_subject = $query_subject->num_rows();

            if($num_subject > 0){
                while($result_subject = $query_subject->fetch_assoc()){
                    $subject = $result_subject['email_subject'];
                    $redirect = $result_subject['thank_you_page'];
                }
            }

            $sql = "SELECT email_address, email_preference FROM plugin_contact_form_emails WHERE form_id=?";
            $query = $zlcms->database->query($sql,array($form_id));
            $num = $query->num_rows();

            if($num > 0){
                while($result = $query->fetch_assoc()){
                    $emails[] = array(
                        "email"		=> $result['email_address'],
                        "preference"	=> $result['email_preference']
                    );
                }
            }

            if(!empty($subject) && !empty($emails) && !empty($from)){
                // Build the body of the email
                $body = '';

                //IF SPECIFIC FIELDS ARE SUPPLIED USE THEM OTHERWISE USE EVERYTHING
                if(!empty($email_fields)) {
                    foreach($email_fields as $key => $value) {
                        if(!empty($_POST[$key])) {
                            $body .= '<strong>'.$value.':</strong> '.$_POST[$key].'<br />';
                        }
                    }
                } else {
                    foreach($_POST as $key=>$value) {
                        if(!empty($_POST[$key])) {
                            $body .= '<strong>'.ucwords(str_replace("_"," ",$key)).':</strong> '.$_POST[$key].'<br />';
                        }
                    }
                }

                if(empty($from)){
                    $from = array(
                        "name"  => $zlcms->company['company'] . " Website",
                        "email" => "info@ziplineinteractive.com"
                    );
                }

                $to = array();
                $cc = array();
                $bcc = array();
                // CHOOSE WHO TO EMAIL
                if(!$this->debug){
                    foreach($emails as $email){
                        switch($email['preference']){
                            case "to":
                                $to[] = $email['email'];
                                break;
                            case "cc":
                                $cc[] = $email['email'];
                                break;
                            case "bcc":
                                $bcc[] = $email['email'];
                                break;
                        }
                    }
                } else {
                    foreach($this->debug_emails as $email){
                        $to[] = $email;
                    }
                }

                // SEND EMAIL
                if($zlcms->$library_class->send_email($to, $subject, $body, $cc, $bcc, $replyTo = '', $attachments)) {

                    // REDIRECT USER IF A THANK YOU PAGE IS PRESENT
                    if(!empty($redirect)){
                        header("Location: " . $redirect);
                        exit;
                    }

                } else {
                    // SHOW ERROR
                    $zlcms->form->error = "An error occurred while sending the email. Please try again.";
                    $zlcms->log_error($zlcms->form->error);
                }
            } else {
                $zlcms->form->error = "An error occurred while sending the email. A required field was empty. Please try again.";
                $zlcms->log_error($zlcms->form->error);
            }

        }

	}
?>