<?PHP
	global $zlcms;
?>

<div class="full formWrapper">
	<form id="exampleForm" class="interiorForm" action="<?PHP echo($_SERVER['REQUEST_URI']); ?>" enctype="multipart/form-data" method="post">
		<?PHP
			$library_class = $zlcms->company['class'];
			echo($zlcms->$library_class->message());
		?>
		<div class="col-xs-12 col-sm-6">
			<input type="text" value="" placeholder="First Name" name="email">
			<input type="text" value="" placeholder="Last Name" name="last_name">
		</div><!--closing of half-->
		<div class="col-xs-12 col-sm-6">
			<input type="text" value="" placeholder="Email" name="first_name">
			<input type="text" value="" placeholder="Phone" name="phone">									
		</div><!--closing of half-->
		<div class="col-xs-12">
			<textarea placeholder="Message /Interests" name="message"></textarea>
		</div><!--closing of full-->
		<div class="col-xs-12">
			<input type="hidden" name="comments" value="" />
			<input type="hidden" name="action" value="example_form_submit" />
			<input type="submit" value="Submit" name="submit">
			
			<?PHP //NOTE:  If you wish to use multiple file upload buttons the then id of the file input needs to have a counter appended to it (ie file_1) and you will need to change the label and javascript to match ?>
			<div class="fileUploadWrapper">
				<input type="file" value="" name="resume" id="file">
			</div><!--closing of fileUploadWrapper-->
			<?PHP //NOTE:  The label is to display the uploaded file name ?>
			<label for="file" class="fileUploadName" id="file_label"></label>
			
		</div><!--closing of full-->
	</form>
</div><!--closing of formWrapper -->