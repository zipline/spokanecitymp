<?PHP
	// Get the class name for this plugin
	$plugin = $this->plugin;
	$company_id = $this->company_id();

	$items = $this->homepage->retrieve_item_details(@$_GET['id']);
?>
<section class="mainContentWrapper">
	<form id="<?PHP echo($plugin); ?>" action="<?PHP $this->get_form_action(); ?>" method="post" enctype="multipart/form-data">
		<div class="full column borderBottom3px">
			<div class="mainPageTitle">
				<div class="two-third-med half pr">
					<h1 class="push40"><?PHP echo($this->$plugin->title); ?></h1>
				</div><!--closing of full-->
				<div class="third-sm half">
					<input type="submit" class="button addPage" name="save" value="SAVE CHANGES">
				</div><!--closing of forty - full-->
			</div><!--closing of mainPageTitle-->

			<div class="full subNavWrapper">
				<?PHP
					// IF YOU WOULD LIKE TO ADD MULTIPLE TABS (i.e. a manage categories tab) THEN YOU NEED TO ADD THE FOLLOWING SPAN AND UL CODE BLOCK
					/*
						<span class="visible-sm mobileDeviceBtn">Plugin Options</span>
						<ul class="subNav">
							<li class="active"><a href="#">Core Pages</a></li>
							<li><a href="#">Orphan Pages</a></li>
						</ul>
					 */
				?>
				<ul class="helpList right">
					<li><a href="#"><span class="info iconSprite"></span></a></li>
					<li><a href="#"><span class="video iconSprite"></span></a></li>

					<?PHP
						// IF YOU WOULD LIKE TO ADD PAGINATION THEN YOU NEED TO ADD THE FOLLOWING LI'S
						/*
							<li class="pageNumb">1 of 1</li>
							<li><a href="#"><span class="iconSprite arrowLeft"></span></a></li>
							<li><a href="#"><span class="iconSprite arrowRight"></span></a></li>
						*/
					?>

				</ul>
			</div><!--closing of full - subNavWrapper-->
		</div><!--closing of full column -->

		<?PHP
			if($company_id == 1){
		?>
			<div class="full p25 bg3">
				<div class="full dashedBorderBottom1px pushd30">
					<h2>Horizontal Feature Set</h2>
					<div class="full">
						<div class="half-med full pr-med">
							<label class="full" for="hor_fset_title">Title *</label>
						
							<input class="full" type="text" id="hor_fset_title" value="<?PHP $this->form->toggle_value(@$items['hor_fset_title']['content'],@$_POST['hor_fset_title']); ?>" name="hor_fset_title" />
						</div>
						<div class="half-med full pr-med">
							<label class="full" for="hor_fset_title">Link *</label>

							<input class="full" type="text" id="hor_fset_link" value="<?PHP $this->form->toggle_value(@$items['hor_fset_link']['content'],@$_POST['hor_fset_link']); ?>" name="hor_fset_link" />
						</div>
					</div><!--closing of full-->
					<div class="half-med full pr-med pushd30">
						<div class="full">
							<label class="full" for="file_upload">Image * (<?PHP echo($this->$plugin->image['hor_fset_image']['width'] . ' x ' . $this->$plugin->image['hor_fset_image']['height']); ?>)</label>
						</div><!--closing of full-->
						<div class="three-quarter-lg full pr-lg">
							<div class="fileUploadWrapper">
								<label class="fileName">No image selected</label>
								<div class="fileInputWrapper">
									<input type="file" name="hor_fset_image" class="full upload_button" />
								</div><!--closing of fileInputWrapper-->
							</div><!--closing of fileUploadWrapper-->
						</div><!--closing of full-->
						<div class="quarter-lg full">
							<?PHP
								if(!empty($items['hor_fset_image']['content'])){
									$hor_fset_image = $this->file_path_relative . $plugin . '/t_' . $items['hor_fset_image']['content'];
									echo('<img src="' . $hor_fset_image . '" title="Horizontal Feature Set Image" alt="Horizontal Feature Set Image" /> ');
								}
							?>
						</div>
					</div>
				</div><!--closing of full-->
				<div class="full">
				<?PHP
					for($i = 1; $i <= 4; $i++){
				?>					
						<div class="half-med full pr-med pushd30">
							<h2>Vertical Feature Set <?PHP echo($i); ?></h2>
							<div class="full">
								<label class="full" for="vert_fset_title_<?PHP echo($i); ?>">Title *</label>
								<input class="full" type="text" id="vert_fset_title_<?PHP echo($i); ?>" value="<?PHP $this->form->toggle_value(@$items['vert_fset_title_' . $i]['content'],@$_POST['vert_fset_title_' . $i]); ?>" name="vert_fset_title_<?PHP echo($i); ?>" />
							</div><!--closing of full-->

							<div class="full">
								<label class="full" for="vert_fset_description_<?PHP echo($i); ?>">DESCRIPTION * (120 character limit)</label>
								<input class="full" type="text" id="vert_fset_description_<?PHP echo($i); ?>" value="<?PHP $this->form->toggle_value(@$items['vert_fset_description_' . $i]['content'],@$_POST['vert_fset_description_' . $i]); ?>" name="vert_fset_description_<?PHP echo($i); ?>" maxlength="120" />
							</div><!--closing of full-->

							<div class="half-sm full pr-sm">
								<label class="full" for="vert_fset_link_text_<?PHP echo($i); ?>">LINK TEXT *</label>
								<input class="full" type="text" id="vert_fset_link_text_<?PHP echo($i); ?>" value="<?PHP $this->form->toggle_value(@$items['vert_fset_link_text_' . $i]['content'],@$_POST['vert_fset_link_text_' . $i]); ?>" name="vert_fset_link_text_<?PHP echo($i); ?>" />
							</div><!--closing of full-->

							<div class="half-sm full">
								<label class="full" for="vert_fset_link_<?PHP echo($i); ?>">LINK *</label>
								<input class="full" type="text" id="vert_fset_link_<?PHP echo($i); ?>" value="<?PHP $this->form->toggle_value(@$items['vert_fset_link_' . $i]['content'],@$_POST['vert_fset_link_' . $i]); ?>" name="vert_fset_link_<?PHP echo($i); ?>" />
							</div><!--closing of full-->

							<div class="full">
							
								<div class="full">
									<label class="full" for="file_upload">Image * (<?PHP echo($this->$plugin->image['vert_fset_image_' . $i]['width'] . ' x ' . $this->$plugin->image['vert_fset_image_' . $i]['height']); ?>)</label>
								</div><!--closing of full-->
								<div class="three-quarter-lg full pr-lg">
									<div class="fileUploadWrapper">
										<label class="fileName">No image selected</label>
										<div class="fileInputWrapper">
											<input type="file" name="vert_fset_image_<?PHP echo($i); ?>" class="full upload_button" />
										</div><!--closing of fileInputWrapper-->
									</div><!--closing of fileUploadWrapper-->
								</div><!--closing of full-->
								<div class="quarter-lg full">
									<?PHP
										if(!empty($items['vert_fset_image_' . $i]['content'])){
											$hor_fset_image = $this->file_path_relative . $plugin . '/t_' . $items['vert_fset_image_' . $i]['content'];
											echo('<img src="' . $hor_fset_image . '" title="Vertical Feature Set Image ' . $i . '" alt="Vertical Feature Set Image ' . $i . '" /> ');
										}
									?>
								</div>
							
							</div><!--closing of full-->
						</div>

			<?PHP
						}
				}
			?>

				<div class="full">
					<input type="hidden" name="action" value="<?PHP echo($plugin); ?>_submit" />
					<input type="submit" value="Save Changes" class="button" />
				</div><!--closing of full-->
			</div><!--closing of full-->
		</form>
	</div><!--closing of p25 full bg3-->
</section>

<?PHP //BEGIN INFORMATIONAL LIGHTBOX (INFO ABOUT HOW TO USE THIS FORM/PLUGIN GOES HERE) ?>
<section class="lightboxWrapper" id="infoLightbox">
	<div class="full borderBottom3px">
		<h2>Instructions:</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full" id="instructionsHolder">
		<p>Use this form to manage the content on your home page.</p>
		<p><strong>Links</strong><br>If you wish to link to content located at another website, or if you wish to have your link open in a new page, you will need to use the full url.  </p>
		<p><strong>Images</strong><br>It is considered best practice to re-size your image to match the size listed before uploading. The system will automatically crop images that do not match the listed size, and in some cases this may cause the image to appear different than intended.</p>
	</div><!--closing of full-->
</section>

<?PHP //BEGIN VIDEO LIGHTBOX (VIDEO TUTORIAL FOR THIS FORM/PLUGIN GOES HERE) ?>
<section class="lightboxWrapper" id="tutorialLightbox">
	<div class="full borderBottom3px">
		<h2>Video Tutorial</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full" id="videoHolder"></div><!--closing of full-->
</section>