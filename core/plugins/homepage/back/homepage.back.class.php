<?PHP

	/*****************************************************************************************************************************************
	*
	* Back-end Homepage Plugin
	* 
	* @package    ZLCMS
	* @subpackage homepage
	* @author     Ryan Stemkoski <ryan@gozipline.com>, Kraig Lutz <kraig@ziplineinteractive.com>
	* @copyright  2005-2014 Zipline Communications Inc.
	* @version    3.0
	* @link       http://www.ziplineinteractive.com
	*
	*****************************************************************************************************************************************/
	class homepage {
	
		var $title, $image = array();
		/*************************************************************************************************************************************
		 * This function is used to establish the base options for this plugin, primarily setting the title and image size variables for use throughout the plugin
		 *************************************************************************************************************************************/
		function plugin_construct(){
			global $zlcms;
			$company_id = $zlcms->company_id();

			$this->title = "homepage";

			// Set the image width and height based on the front end design (If multiple images are needed just add them to the image array in the image['image_name'] = array("width" => 'image_width', "height" => 'image_height'))
			if($company_id == 1){
				$this->image["hor_fset_image"] = array("width" => 728, "height" => 90);
				$this->image["vert_fset_image_1"] = array("width" => 228, "height" => 402);
				$this->image["vert_fset_image_2"] = array("width" => 228, "height" => 402);
				$this->image["vert_fset_image_3"] = array("width" => 228, "height" => 402);
				$this->image["vert_fset_image_4"] = array("width" => 228, "height" => 402);
			}
		}

		/*************************************************************************************************************************************
		*
		* This function is used to process get or post requests within this plugin.
		* @param $action string Is a string indicating which action we are trying to process.
		*
		*************************************************************************************************************************************/
   		function process_request($action) {
   			global $zlcms;
   			if($zlcms->is_logged()) {
   			
   				//ON FORM SUBMIT
   				if($action == $this->title . "_submit") {
					$this->handle_submit();
   				}
   			
			}
		}
	
		/*************************************************************************************************************************************
		*
		* This function is used to retrieve the homepage info from the database
		* @return boolean/array False if there are no results found, otherwise an array containing all the details from the database.
		*
		*************************************************************************************************************************************/
		function retrieve_item_details() {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			$return = false;
		
			//GET COMPANY ID
			$id = $zlcms->company_id();
		
			//DO THE QUERY
			$sql = "SELECT * FROM plugin_homepage_items WHERE company_id=?";
			$query = $zlcms->database->query($sql,array($id));
			$number = $query->num_rows();

			//IF RESULT RETURN IT OTHERWISE LOG ERROR
			if($number >= 1) {
				while($result = $query->fetch_assoc()) {
					$return[$result['slug']] = $result;
				}
			} else {
				if(!empty($id)) {
					$zlcms->log_error("You have attempted to access an item that has been removed or no longer exists");
				}
			}
			
			return $return;
		
		}
		
		/*************************************************************************************************************************************
		*
		* This function processes the plugin add/edit form
		*
		*************************************************************************************************************************************/
		private function handle_submit() {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			$success = false;
			$company_id = $zlcms->company_id();

			$zlcms->form->set_rule(@$_POST['hor_fset_title'],"empty","You must provide a valid horizontal feature set title to continue.");

			if($company_id == 1){
				for($i = 1;$i <= 4; $i++){
					$zlcms->form->set_rule(@$_POST['vert_fset_title_' . $i],"empty","You must provide a valid vertical feature set {$i} title to continue.");
					$zlcms->form->set_rule(@$_POST['vert_fset_description_' . $i],"empty","You must provide a valid vertical feature set {$i} title to continue.");
					$zlcms->form->set_rule(@$_POST['vert_fset_link_text_' . $i],"empty","You must provide a valid vertical feature set {$i} title to continue.");
					$zlcms->form->set_rule(@$_POST['vert_fset_link_' . $i],"empty","You must provide a valid vertical feature set {$i} title to continue.");
				}
			}

			//RUN RULES
			if($zlcms->form->run_rules()) {	
			
				$result_files = $this->retrieve_item_details();
				
				$fields = array(
					'company_id',
					'slug',
					'content'
				);

				// Loop through the images, if there are any
				foreach ($_FILES as $key => $value) { 
				
					if(!empty($_FILES[$key]['name'])) {
						if($zlcms->file->check_type(strtolower($_FILES[$key]['name']),array("jpg","gif","png","jpeg"))) {						
							$image = $zlcms->file->upload($this->title,@$_FILES[$key],@$result_files[$key]['content']);
							
							$zlcms->file->delete($this->title,@$result_files[$key]['content']);
							$zlcms->file->delete($this->title,"t_" . @$result_files[$key]['content']);
							
							$image_file = $zlcms->file_path_relative . "/" . $this->title . "/" . strtolower($image);
							$image_file2 = $zlcms->file_path_relative . "/" . $this->title . "/t_" . strtolower($image);

							$zlcms->image->crop($image_file, $this->image[$key]['width'], $this->image[$key]['height'], $image_file);

							// this scales the image to the same ratio, but so that it's only 155 pixels wide to fit well in the backend thumbnail column
							$zlcms->image->crop($image_file, 155, round($this->image[$key]['height'] * 155 / $this->image[$key]['width']), $image_file2);

							$values = array(
								'company_id'	=> $company_id,
								'slug'		=> $key,
								'content'		=> $image
							);
							
							// Checks if this field is already in the database,
							// If so, we update it
							if($this->field_exists($key)) {
								$zlcms->database->update_auto_helper($fields,$values,"plugin_homepage_items","WHERE company_id=? AND slug=?",$arguments = array($company_id, $key));
							} else {
								// If not, we add it
								$zlcms->database->insert_auto_helper($fields,$values,"plugin_homepage_items");
							}
					
						} else {
							$message = "The image you uploaded was not the proper filetype.";
							header("Location: " . $zlcms->base(true) . "index.php?plugin={$zlcms->plugin}&page=index&error=" . $message);		
							exit;			
						}
					}
					
				}
						
				//CREATES FIELD ARRAY FORM FORM VALUES
				$slugs = $zlcms->database->convert_array_to_fields($_POST,array("id","company_id","action","submit","save"));

				// Loops through each entered value
				foreach ($slugs as $slug) { 
				
					$values = array(
						'company_id'	=> $zlcms->company_id(),
						'slug'			=> $slug,
						'content'		=> $_POST[$slug]
					);
				
					// Checks if this field is already in the database,
					// If so, we update it
					if($this->field_exists($slug)) {
						$query = $zlcms->database->update_auto_helper($fields,$values,"plugin_homepage_items","WHERE company_id=? AND slug=?",$arguments = array($company_id, $slug));
					} else {
						// If not, we add it
						$query = $zlcms->database->insert_auto_helper($fields,$values,"plugin_homepage_items");
					}
										
					if($query) {
						$success = true;
					}
					
				}

				//IF QUERY SHOW SUCCESS OTHERWISE SHOW ERROR
				if($success == true) {
					$zlcms->log_event("updated the {$this->title}");
					$message = "Your {$this->title} details have been saved.";
					header("Location: " . $zlcms->base(true) . "index.php?plugin={$zlcms->plugin}&page=index&success=" . $message);	
				} else {
					$zlcms->log_error("There was a problem updating the database. Please try again");
				}
				
			} else {
				$zlcms->log_error($zlcms->form->error);
			}

		}
		
		/*************************************************************************************************************************************
		*
		* This function will determine if a field is already in the database or not
		* @param $slug string The slug for the field
		* @return boolean True if the field is found to exist, false if it is not
		*
		*************************************************************************************************************************************/
		private function field_exists($slug) {
		
			global $zlcms;
			
			$sql = "SELECT id FROM plugin_homepage_items WHERE company_id=? AND slug=?";
			$query = $zlcms->database->query($sql,array($zlcms->company_id(),$slug));
			$num	= $query->num_rows();
			
			if($num >= 1) {
				return true;
			} else {
				return false;
			}
		
		}
					
	}
?>