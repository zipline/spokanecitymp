<?PHP

	/*****************************************************************************************************************************************
	*
	* Front-end Homepage Plugin
	* 
	* @package    ZLCMS
	* @subpackage homepage
	* @author     Ryan Stemkoski <ryan@gozipline.com>, Kraig Lutz <kraig@ziplineinteractive.com>
	* @copyright  2005-2014 Zipline Communications Inc.
	* @version    3.0
	* @link       http://www.ziplineinteractive.com
	*
	*****************************************************************************************************************************************/
	class homepage {

		/*************************************************************************************************************************************
		*
		* Query the homepage table for all data for this company
		*
		*************************************************************************************************************************************/
		function load_data() {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			$page = false;

			//QUERY THE DATAABS
			$sql = "SELECT * FROM plugin_homepage_items WHERE company_id=?";
			$query = $zlcms->database->query($sql,array($zlcms->company_id()));
			
			//IF RESULTS LOAD THEM
			if($query) {
				while($result = $query->fetch_assoc()) {
					$page[$result['slug']] = $result['content'];
				}
			}
			
			return $page;
			
		}
				
	}
?>