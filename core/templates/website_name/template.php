<?PHP
	global $zlcms;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?PHP $zlcms->meta('title'); ?></title>

	<?PHP $zlcms->meta('description'); ?>
	<?PHP $zlcms->meta('keywords'); ?>
	<link href="/?action=css&amp;items%5B%5D=font&amp;items%5B%5D=bootstrap.min&amp;items%5B%5D=editor&amp;items%5B%5D=format&amp;items%5B%5D=formatMediaQuery" rel="stylesheet">
	<!--[if lte IE 8]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?PHP $zlcms->content->select_template(); ?>

	<script src="https://code.jquery.com/jquery-1.11.1.min.js" type="text/javascript"></script>
	<script src="/?action=javascript&amp;items%5B%5D=bootstrap.min&amp;items%5B%5D=javascript"></script>
</body>
</html>