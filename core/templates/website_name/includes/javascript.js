//**********************************************************************************************************************
/**
* COMPANY: Zipline Interactive
* EMAIL: infon@gozipline.com
* PHONE: 509-321-2849
* DESCRIPTION: This document contains programming required for the website templates.  Requires the jQuery library.
*/
//***********************************************************************************************************************


//***********************************************************************************************************************
//ON DOCUMENT READY FUNCTIONS
//***********************************************************************************************************************
$(function() {

/***********
THIS SCRIPT IS FOR WINDOWS PHONES AND OTHER DEVICES THAT MAY NOT RENDER MEDIA QUERIES
	- REFERENCE: http://getbootstrap.com/getting-started/#support-ie10-width
***********/
	if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
	  var msViewportStyle = document.createElement('style')
	  msViewportStyle.appendChild(
	    document.createTextNode(
	      '@-ms-viewport{width:auto!important}'
	    )
	  )
	  document.querySelector('head').appendChild(msViewportStyle)
	}


	$('.carousel').carousel({
		interval: 2000
	})

});

