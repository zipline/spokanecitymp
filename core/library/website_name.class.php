<?PHP

	//*****************************************************************************************************************************************
	/**
	*
	* This class is the default class that holds navigations and other custom functions specific to the site being developed.
	* 
	* @package    ZLCMS
	* @subpackage default
	* @author     Ryan Stemkoski <ryan@ziplineinteractive.com>
	* @copyright  2014 Zipline Communications Inc.
	* @version    3.0.2
	* @link       http://www.ziplineinteractive.com
	*/
	//*****************************************************************************************************************************************
	class website_name {

		//*************************************************************************************************************************************
		/*
		* THE DEFAULT FUNCTIONS LISTED HERE ARE SOME OF THE MORE COMMONLY USED FUNCTIONS.
		* The log_error and show_error functions are used in the contact plugin by default so if you choose to remove or change them then you may need to modify the contact plugin as well.
		*/
		//*************************************************************************************************************************************

		//********************************************************************************************************************
		/*
		 * This function creates an array of pages for use in navigation bars or menu blocks
		 * @param $ids string Comma separated list of page id's to use in the navigation
		 */
		//*********************************************************************************************************************
		function build_navigation($ids)  {
			global $zlcms;

			$return = false;

			$sql = "SELECT id,url,permalink,title FROM plugin_content_pages WHERE parent_id = '0' AND id IN({$ids}) AND company_id=? AND visible='true' ORDER BY display";
			$query = $zlcms->database->query($sql,array($zlcms->company_id()));
			$number = $query->num_rows();

			if($number > 0){
				while($result = $query->fetch_assoc()){
					$result['link'] = $zlcms->build_link($result['permalink'], $result['url']);
					if($this->check_for_sub_nav($result['id'])){
						$result['subnav'] = $this->build_sub_nav($result['id']);
					}

					$return[] = $result;
				}
			}

			return $return;
		}

		/********************************************************************************************************************/
		/*
		* This function does a check to see if the current main level item has sub navigation.
		* @param $page_id integer The id of the page we are checking on
		*/
		/*********************************************************************************************************************/
		function check_for_sub_nav($page_id){
			global $zlcms;

			//QUERY THE DATABASE FOR THE ITEM
			$sql = "SELECT id FROM plugin_content_pages WHERE parent_id=? AND visible='true'";
			$query = $zlcms->database->query($sql,array($page_id));
			$number = $query->num_rows();

			//IF THE ITEM EXISTS
			if($number >= 1) {
				return true;
			} else {
				return false;
			}

		}

		/********************************************************************************************************************/
		/*
		* This function gathers all the sub navigation items recursively starting with the top most nav point
		* @param $page_id integer The id of the page we are working with (when calling this function the first time you should use the id of the top level page for the nav that you are building i.e. $zlcms->content->level[1])
		*/
		/*********************************************************************************************************************/
		function build_sub_nav($page_id){
			global $zlcms;

			// Clear the nav array so that we start fresh every time
			$nav = array();

			// Get the pages that have the current page id listed as their parent id
			$sql = "SELECT id,url,title,permalink FROM plugin_content_pages WHERE company_id=? AND parent_id=? AND visible='true' ORDER BY display";
			$query = $zlcms->database->query($sql,array($zlcms->company_id(), $page_id));
			$num = $query->num_rows();

			if($num > 0){
				while($result = $query->fetch_assoc()){

					// Check if the result page has subnav, if it does then call this function again using this page as the new starting point
					if($this->check_for_sub_nav($result['id'])){
						$nav[$result['id']] = array(
							'title'		=> $result['title'],
							'link'		=> empty($result['url']) ? $zlcms->build_link($result['permalink'], $result['url']) : $zlcms->build_link("", $result['url']),
							'sub_pages'	=> $this->build_sub_nav($result['id'])
						);
					} else {
						$nav[$result['id']] = array(
							'title'		=> $result['title'],
							'link'		=> empty($result['url']) ? $zlcms->build_link($result['permalink'], $result['url']) : $zlcms->build_link("", $result['url']),
						);
					}

					$nav[$result['id']]['is_active'] = $result['id'] == $zlcms->content->id ? true : false;
				}
			}

			// Return the nav array
			return $nav;
		}

        /*************************************************************************************************************************************
         *
         * This function is used to send the various emails.
         *
         * @param mixed $to The address(s) to send the email to.
         * @param string $subject The email subject.
         * @param string $body The email body.
         * @param mixed $cc The address(s) to send the email to as a cc.
         * @param mixed $bcc The address(s) to send the email to as a bcc.
         * @param string $replyTo The address the recipient can reply to.
         * @param array $attachments The file attachments that need to be included in the email.
         *
         * @return boolean If the email sent correctly then true otherwise false.
         *
         *************************************************************************************************************************************/
        function send_email($to, $subject, $body, $cc = '', $bcc = '',$replyTo = '', $attachments = ''){
            global $zlcms;

            $library_class = $zlcms->company['class'];

            $mail = new $zlcms->mail;

            try{
                // USE SMTP IF IT HAS BEEN SETUP
                if(!empty($zlcms->config['email_host']) && !empty($zlcms->config['email_username']) && !empty($zlcms->config['email_password'])){
                    $mail->IsSMTP();

                    $mail->SetLanguage("en", 'core/tools/phpMailer/language/');
                    $mail->SMTPSecure   = $zlcms->config['smtp_secure'];
                    $mail->Host         = $zlcms->config['email_host'];
                    $mail->Port         = $zlcms->config['smtp_port'];
                    $mail->SMTPDebug    = $zlcms->config['smtp_debug'];
                    $mail->Timeout      = $zlcms->config['smtp_timeout'];
                    $mail->SMTPAuth     = $zlcms->config['smtp_auth'];
                    $mail->Username     = $zlcms->config['email_username'];
                    $mail->Password     = $zlcms->config['email_password'];
                }

                $mail->From     = $zlcms->config['email_username'];
                $mail->FromName = 'Do Not Reply';

                if(!empty($replyTo)){
                    $mail->AddReplyTo($replyTo);
                }

                $mail->Subject  = $subject;

                if(is_array($to)){
                    foreach($to as $to_val){
                        $mail->AddAddress($to_val);
                    }
                } else {
                    $mail->AddAddress($to);
                }

                if(!empty($cc)){
                    if(is_array($cc)){
                        foreach($cc as $cc_val){
                            $mail->AddAddress($cc_val);
                        }
                    } else {
                        $mail->AddAddress($cc);
                    }
                }

                if(!empty($bcc)){
                    if(is_array($bcc)){
                        foreach($bcc as $bcc_val){
                            $mail->AddBCC($bcc_val);
                        }
                    } else {
                        $mail->AddBCC($bcc);
                    }
                }

                $mail->MsgHTML($body);

                if(!empty($attachments) && is_array($attachments)){
                    foreach($attachments as $file){
                        $mail->AddAttachment("core/files/{$library_class}/contact/{$file['saved_name']}",$file['actual_name']);
                    }
                }

                $sent = $mail->Send();

            } catch(Exception $e){

                //IF THERE IS AN GENERIC EXCEPTION, SEND EMAIL TO THE ADMIN WITH DETAILS
                $devmail = new PHPMailer(true);

                $devmail->SetFrom($zlcms->config['email_username'], "Do Not Reply");

                //EMAIL THE DEVELOPER IF THERE IS AN ERROR
                $devmail->AddAddress('rgauthier@ziplineinteractive.com');
                $devmail->Subject = 'QuantaSub Generic SMTP Error';
                $devmail->Subject = $subject;
                $msg = "You are receiving this email because there was a problem with the SMTP email. Please review the following to ensure that it has come through the system correctly.<br/>";

                $recipients = is_array($to) ? implode(", ", $to) : trim($to);

                $msg .= "The intended recipient(s): " . $recipients . "<br/>";

                $msg .= "The returned error message: " . $e->getMessage() . "<br/>";
                $msg .= "The intended body of the message:<br/>" . $body;

                $devmail->MsgHTML($msg);
                $sent = $devmail->Send();

            }

            return $sent;

        }

		//********************************************************************************************************************
		/**
		 * This method outputs an error message if one has been thrown.
		 * @uses $zlcms->error Array An array that contains system error messages from the application.
		 * @return string $output The html formatted error message.
		 */
		//*********************************************************************************************************************
		public function message()  {
			global $zlcms;
			$output = '';

			if(!empty($zlcms->error)) {
				$output .= '
					<div class="message">
						<div class="container">
							<div class="row">
								<div class="col-xs-12">
				';

				foreach($zlcms->error as $error) {
					$output .= '<p>' . $error . '</p>';
				}

				$output .= '
								</div>
							</div>
						</div>
					</div>
				';

			}

			return $output;

		}

	}
?>