<?PHP

	//*****************************************************************************************************************************************
	/**
	*
	* The file class is used by both the front-end and back-end of the website to handle file interactions including: uploading, deleting, checking, and displaying.  These helper functions are designed to streamline the process of handling files for all website plugins.  
	* 
	* @package    ZLCMS
	* @subpackage file
	* @author     Ryan Stemkoski <ryan@ziplineinteractive.com>
	* @copyright  2012 Zipline Communications Inc.
	* @version    2.1
	* @link       http://www.ziplineinteractive.com
	*/
	//*****************************************************************************************************************************************
	class file {
	
		
		//*****************************************************************************************************************************************
		/**
		* This method uploads a file to the plugin file directory specified.
		* @param $plugin string is the plugin within which we are working and need to store files
		* @param $new_file array contains the upload information on the new file
		* @param $old_file string is the name of an old file that this one is to replace and needs to be removed.
		* @param $filename string is the name of the new file for storing
		* @return mixed This method returns the filename of the upload file if a successful upload occurs otherwise it returns false.
		*/
		//*****************************************************************************************************************************************
		public function upload($plugin,$new_file,$old_file="",$filename="") {
								
			//GLOBAL ZLCMS
			global $zlcms;
			
			//PLUGIN DIRECTORY
			$directory = $zlcms->file_path_relative . $plugin . "/";
			if(!is_dir($directory)){
				mkdir($directory,0777,true);
				@chmod($directory,0777);
			}
			
			//CHECK TO SEE IF A FILE WAS UPLOADED
			if(is_array($new_file)) {
				
				//GET THE .EXT EXTENSION OF THE UPLOADED FILE
				$extension =  "." . strtolower($this->file_extension($new_file['name']));
				
				//NEW FILENAME
				if(empty($filename)) { $filename = md5(time().uniqid()) . $extension; } 
				
				//MOVE UPLOADED FILE
				if(move_uploaded_file($new_file['tmp_name'],$directory . $filename)) {
					if(@$old_file != $filename) {
						$this->delete($plugin,$old_file);
					}
					return $filename;
				} else {
					$zlcms->log_error("There was a problem uploading your file");
					return false;
				}
				
			} else {
				return false;
			}
			
		}
		
		
		//*****************************************************************************************************************************************
		/**
		* This method deletes a file from the server if it exists. If the file does not exist the function will fail gracefully returning false.
		* @param $plugin string is the plugin within which we are working and need to store files
		* @param $file string is the name of the file 
		* @return boolean True if the file has been deleted and false if the file didn't exist or could not be deleted. 
		*/
		//*****************************************************************************************************************************************
		public function delete($plugin,$file) {
		
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//PLUGIN DIRECTORY
			$directory = $zlcms->file_path_relative . $plugin . "/";
			
			//CHECK FOR FILE
			if($this->check($plugin,$file)) {
				if(unlink($directory . $file)) {
					return true;
				} else {
					$zlcms->log_error("This file could not be deleted");
					return false;
				}
			} else {
				return false;
			}

		
		}

		//*****************************************************************************************************************************************
		/**
		* This method checks to make sure a file exists on the server.  This method is designed to be used before displaying an image or file. 
		* @param $plugin string A string that represents the plugin within which we are working and need to store files
		* @param $file string A string that represents the name of the file.
		* @return boolean A true/false value representing if the file exists on the server or not.
		*/
		//*****************************************************************************************************************************************		
		public function check($plugin,$file) {
		
			//GLOBAL ZLCMS
			global $zlcms;
			
			//PLUGIN DIRECTORY
			$directory = $zlcms->file_path_relative . $plugin . "/";
			
			//CHECK FOR FILE
			if(is_file($directory . $file)) {
				return true;
			} else {
				return false;
			}

		}
		

		//*****************************************************************************************************************************************
		/**
		* This method outputs an image path to be used in displaying an image.
		* @param $plugin string A string containing the plugin name within which we are working and need to display files.
		* @param $file string Is the name of the file we wish to show complete with file extension.
		* @param $return boolean A value representing if the function should return a value or output it directly.
		*/
		//*****************************************************************************************************************************************		
		public function show_image($plugin,$file,$return=false) {
		
			//GLOBAL ZLCMS
			global $zlcms;
			
			//PLUGIN DIRECTORY
			$directory =  $zlcms->file_path_relative . $plugin . "/";

			//CHECK FOR FILE
			if($return == true) {
				return $directory . $file;
			} else {
				echo($directory . $file);
			}

		}
		
		//*****************************************************************************************************************************************
		/**
		* This method outputs a file path to the browser to be used in building a direct link to an uploaded file.
		* @param $plugin string A string containing the plugin name within which we are working and need to display files.
		* @param $file string Is the name of the file we wish to show complete with file extension.
		* @param $return boolean A value representing if the function should return a value or output it directly.
		*/
		//*****************************************************************************************************************************************		
		public function show_file($plugin,$file,$return=false) {
		
			//GLOBAL ZLCMS
			global $zlcms;
			
			//PLUGIN DIRECTORY
			$directory =  $zlcms->file_path_relative . $plugin . "/";

			//CHECK FOR FILE
			if($return == true) {
				return $directory . $file;
			} else {
				echo($directory . $file);
			}

		}

		
		//*****************************************************************************************************************************************
		/**
		* This method checks the extension of a file against various extension types in an array. (Note: This does not check MIME type. If a more secure check needs to be made you will need to extend functionality with a custom function.)
		* @param $filename string A string representing the name of the current file.
		* @param $extensions array An array of possible extensions
		*/
		//*****************************************************************************************************************************************	
		public function check_type($filename,$extensions) {
			$ext = $this->file_extension($filename);
			if(in_array($ext,$extensions)) {
				return true;
			} else {
				return false;
			}
		}
		

		//*****************************************************************************************************************************************
		/**
		* This method returns normal .abc file extension. Note this may not work with some off the wall file names
		* @param $filename string is the name of the file for which we need an extension.
		* @return string The extension of the filename that was passed in.
		*/
		//*****************************************************************************************************************************************
		public function file_extension($filename) {
    		     $file = explode(".", $filename);
			$extension =  end($file);
    		     return $extension;
		}

		
	
	}
	
?>