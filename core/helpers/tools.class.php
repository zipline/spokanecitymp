<?PHP 
	class tools {
	
	/*************************************************************************************************************************************
		*
		* This function used to build a list of items we need
		* $table is the name of the database table you need the data from
		* $order_by is an optional parameter used to return the order of the list 
		**************************************************************************************************************************************/
		function fetch_list($table,$order_by=false){
			global $zlcms;
			
			$order = $order_by ? ' ORDER BY ' . $order_by : '';
			$return = 'No items were found.';
			$zebra = array('bgMid', 'bgLight');
			$n = 1;

			$sql = "SELECT * FROM $table WHERE company_id=? $order";
			$query = $zlcms->database->query($sql,array($zlcms->company_id()));
			$number = $query->num_rows();

			if($number > 0){
				$return = array();
				while($result = $query->fetch_assoc()){
					$result['css'] = $zebra[$n%2];
					$return[] = $result;
					$n++;
				}
			}

			return $return;
		}
		
		/*************************************************************************************************************************************
		*
		* This function used to return a specific item we need from the database
		* $table is the name of the database table you need the data from
		**************************************************************************************************************************************/
		function fetch_item($table,$id=false){
			global $zlcms;
			$return = false;

			if($id){
				$sql = "SELECT * FROM $table WHERE id=?";
				$query = $zlcms->database->query($sql,array($id));
				$number = $query->num_rows();

				if($number > 0){
					$return = $query->fetch_assoc();
				} else {
					if($id){
						$zlcms->log_error("You have attempted to access an event that has been removed or no longer exists");
					}
				}
			}

			return $return;
		}
		
		/*************************************************************************************************************************************
		*
		* This function returns the next display rank available for the item being inserted.
		* @return integer A number representing the highest available number in the database.
		* $table is the name of the database table you need the data from
		*
		*************************************************************************************************************************************/
		function calculate_display($table){
			global $zlcms;

			$sql = "SELECT id FROM $table WHERE company_id=?";
			$query = $zlcms->database->query($sql,array($zlcms->company_id()));
			$number = $query->num_rows();

			$output = $number+1;

			return $output;
		}
		
		/*************************************************************************************************************************************
		*
		* This function updates the display rank of the items
		* @param $items array An ordered array of item ids
		* $table is the name of the database table that needs to be udpated
		*************************************************************************************************************************************/
		function update_rank($items,$table){
			global $zlcms;

			$n = 1;

			$values = @$items;
			$values = str_replace("id[]=","",$values);
			$items = explode("&",$values);

			if(is_array(@$items)){
				foreach($items as $item){
					$sql = "UPDATE $table SET display=? WHERE id=? AND company_id=?";
					$zlcms->database->query($sql,array($n,$item,$zlcms->company_id()));
					$n++;
				}
			}
		}
		
		/*************************************************************************************************************************************
		*
		* Reset the display rank for the remaining items after an item has been delete.
		* $table is the name of the database table that needs to be udpated
		*************************************************************************************************************************************/
		function reset_rank($table){
			global $zlcms;

			$sql = "SELECT id FROM $table WHERE company_id=? ORDER BY display";
			$query = $zlcms->database->query($sql,array($zlcms->company_id()));

			$n = 1;

			while($result = $query->fetch_assoc()){
				$sql2 = "UPDATE $table SET display=? WHERE id=?";
				$zlcms->database->query($sql2,array($n,$result['id']));
				$n++;
			}
		}
		
		//*************************************************************************************************************************************	
		/**
		* content_permalink function. This function will build a permalink from the text input.  It also includes an optional parameter that 
		* allows it to be output or returned
		* 
		* @access private
		* @param string $text A string that will be turned into a valid URL
		* @param int $id (default: 0) The database ID of the item we are editing (If editing)
		* @param bool $return (default: false) A boolean value indiciating if we should return or output the value directly
		* @return string A valid permalink in string format. 
		*/
		//*************************************************************************************************************************************
		function plugin_permalink($text, $id=0, $return = false,$table) {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//CLEAR OUTPUT FOR USE
			$output = "";
			
			//SETS THE LOCALE
			setlocale(LC_ALL, 'en_US.UTF8');
			
			//CREATES REQUIRED VARIABLES
			$replace = array();
			$delimiter = "-";
			$str = "";
			
			if(!empty($replace)) {
				$text = str_replace((array)$replace, ' ', $text);
			}

			//CLEAN URL
			$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $text);
			$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
			$clean = strtolower(trim($clean, '-'));
			$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);	
			
			//REMOVE TRAILING DASH
			$last = substr($clean, -1);
			if($last == "-") {
				$clean = substr($clean,0,-1);
			} 	
	
			//CHECK TO SEE IF THIS URL EXISTS
			$sql = "SELECT * FROM $table WHERE permalink=? AND company_id=? AND id != ?";
			$query = $zlcms->database->query($sql, array($clean,$zlcms->company_id(),$id));
			$number = $query->num_rows();
			
			//IF WE SOMEHOW GOT HERE WITH NO TEXT SO NO TITLE OR TEXT ADD A PAGE NUMBER
			if(empty($text)) {
				$sql_max = "SELECT MAX(id) as id FROM $table";
				$query_max  = $zlcms->database->query($sql_max,array());
				$result_max = $query_max->fetch_assoc();
				$new_id =  $result_max['id'] + 1;
				$clean = $clean . $new_id;
			
			//THIS HANDLES ALL NORMAL INSTANCES
			} else {
			
				if($number >= 1) {
					if(!empty($id)) {
						$clean = $clean . $id;
					} else {
						$sql_max = "SELECT MAX(id) as id FROM $table";
						$query_max  = $zlcms->database->query($sql_max,array());
						$result_max = $query_max->fetch_assoc();
						$new_id =  $result_max['id'] + 1;
						$clean = $clean . $new_id;
					}
				}
			
			}
	
			//RETURN OR OUTPUT
			if($return == "true") {
				return $clean;
			} else {
				echo($clean);
			}
			
		}
	}
?>