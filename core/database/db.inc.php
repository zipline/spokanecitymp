<?PHP

	/*********************************************************************************************************
	//DATABASE CONNECTION
	**********************************************************************************************************/

	//ATTEMPT TO CONNECT TO THE DATABASE
	$db = @new mysqli($this->config['hostname'],$this->config['database_username'],$this->config['database_password'],$this->config['database']);    

	//IF ATTEMPT FALES THEN LOG ERROR INTO ARRAY OF RUNTIME ERRORS
	if(mysqli_connect_errno()) {
	  $this->log_error("Unable to connect to the database in " . __FILE__ . " on line " . __LINE__ . "<br><strong>MySQLi Connection Error:</strong> " . mysqli_connect_error());
	  die;
	} 

?>