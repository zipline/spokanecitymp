﻿/*
 Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 For licensing, see LICENSE.md or http://ckeditor.com/license
 */
CKEDITOR.addTemplates("default",
	{
		imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath("templates") + "templates/images/"),
		templates: [
			{
				title: "One Column",
				image: "template1.gif",
				description: "One column mini-template.",
				html: '<div class="row miniTemplate"><div class="col-xs-12"><p>1 column template</p></div></div><p>&nbsp;</p>'
			}, {
				title: "Two Column 25-75",
				image: "template1.gif",
				description: "Two column mini-template where the left column is 25% wide and the right column is 75% wide.",
				html: '<div class="row miniTemplate"><div class="col-xs-12 col-sm-3"><p>left column</p></div><div class="col-xs-12 col-sm-9"><p>right column</p></div></div><p>&nbsp;</p>'
			}, {
				title: "Two Column 75-25",
				image: "template3.gif",
				description: "Two column mini-template where the left column is 75% wide and the right column is 25% wide.",
				html: '<div class="row miniTemplate"><div class="col-xs-12 col-sm-9"><p>left column</p></div><div class="col-xs-12 col-sm-3"><p>right column</p></div></div><p>&nbsp;</p>'
			}, {
				title: "Two Column 33-66",
				image: "template3.gif",
				description: "Two column mini-template where the left column is 33% wide and the right column is 66% wide.",
				html: '<div class="row miniTemplate"><div class="col-xs-12 col-sm-4"><p>left column</p></div><div class="col-xs-12 col-sm-8"><p>right column</p></div></div><p>&nbsp;</p>'
			}, {
				title: "Two Column 66-33",
				image: "template3.gif",
				description: "Two column mini-template where the left column is 66% wide and the right column is 33% wide.",
				html: '<div class="row miniTemplate"><div class="col-xs-12 col-sm-8"><p>left column</p></div><div class="col-xs-12 col-sm-4"><p>right column</p></div></div><p>&nbsp;</p>'
			}, {
				title: "Two Column 50-50",
				image: "template3.gif",
				description: "Two column mini-template where the left column is 50% wide and the right column is 50% wide.",
				html: '<div class="row miniTemplate"><div class="col-xs-12 col-sm-6"><p>left column</p></div><div class="col-xs-12 col-sm-6"><p>right column</p></div></div><p>&nbsp;</p>'
			}, {
				title: "Three Column 33-33-33",
				image: "template3.gif",
				description: "Three column mini-template where the left column is 33% wide, the center column is 33% wide, and the right column is 33% wide.",
				html: '<div class="row miniTemplate"><div class="col-xs-12 col-sm-4"><p>left column</p></div><div class="col-xs-12 col-sm-4"><p>center column</p></div><div class="col-xs-12 col-sm-4"><p>right column</p></div></div><p>&nbsp;</p>'
			}, {
				title: "Three Column 25-50-25",
				image: "template3.gif",
				description: "Three column mini-template where the left column is 25% wide, the center column is 50% wide, and the right column is 25% wide.",
				html: '<div class="row miniTemplate"><div class="col-xs-12 col-sm-3"><p>left column</p></div><div class="col-xs-12 col-sm-6"><p>center column</p></div><div class="col-xs-12 col-sm-3"><p>right column</p></div></div><p>&nbsp;</p>'
			}, {
				title: "Four Column 25-25-25-25",
				image: "template3.gif",
				description: "Four column mini-template where the all the columns are 25% wide.",
				html: '<div class="row miniTemplate"><div class="col-xs-12 col-sm-6 col-md-3"><p>left column</p></div><div class="col-xs-12 col-sm-6 col-md-3"><p>left center column</p></div><div class="col-xs-12 col-sm-6 col-md-3"><p>right center column</p></div><div class="col-xs-12 col-sm-6 col-md-3"><p>right column</p></div></div><p>&nbsp;</p>'
			}
		]
	}
);