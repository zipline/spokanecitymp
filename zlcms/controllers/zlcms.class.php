<?PHP

	//*****************************************************************************************************************************************
	/**
	*
	* ZLCMS :: Zipline Content Management System
	* 
	* PHP versions 5.3.3
	*
	* Copyright (c) 2014 Zipline Communications Inc.
	* 
	* LICENSE:
	* 
	* This is a licensed product created by Zipline Communications Inc. exclusively for our clients.
	* Zipline Communications Inc. maintains complete ownership of all source code, concepts, and methodologies.  
	* This product is licensed on a per domain basis and may not be used for other perposes without
	* direct approval from Zipline Communications Inc.  For information regarding this 
	* system or to purchase a license please call: 1-866-440-3158.
	* 
	* @package    ZLCMS_ADMIN
	* @author     Ryan Stemkoski <ryan@ziplineinteractive.com>
	* @copyright  2005-2014 Zipline Communications Inc.
	* @version    3.0.1
	* @link       http://www.ziplineinteractive.com
	*/
	//*****************************************************************************************************************************************
	class ZLCMS {
	
		/**
		* @var string Provides the relative path to the core folder.  The core and ZLCMS can be hosted independent if need be so this would be changed to accomdate that.
		*/
		var $core = "../";
		
		/**
		* @var string This variable contains the name of the cms folder. If for some reason the CMS needs to be rebranded from ZLCMS to another name such as admin this variable will assist with that.
		*/
		var $cms = "zlcms/";
		
		/**
		* @var string This variable is contains the default page for the site.  By default the login page should be loaded if a user is logged in then they will see other content.
		*/
		var $page = "login";
		
		/**
		* @var string This variable on load with the base path for the installation.
		*/
		var $base = "";
		
		/**
		* @var array This variable is filled with details about the company that is currently being managed in the ZLCMS.
		*/
		var $company = array();

		/**
		* @var string This variable is filled with the name of the plugin that is currently being managed.
		*/
		var $plugin = "";
		
		/**
		* @var array This variable is filled with an array of configuration data about the website and company being managed.
		*/
		var $config = array();
		
		/**
		* @var object This variable is filled with the database connetion object.  This is abstracted through the database helper class so you shoudl not access this variable directly.
		*/
		var $db = "";
		
		/**
		* @var mixed This variable is filled with error / messages for display.
		*/
		var $error = "";

		/**
		* @var mixed This variable is filled with success / messages for display.
		*/
		var $success = "";
		
		/**
		* @var string This variable is filled with the template path on page load.
		*/
		var $template_path = "";

		/**
		* @var string This variable is filled with the file path on page load.
		*/
		var $file_path = "";
		
		/**
		* @var array This variable is filled with the available subclasses on page load.
		*/
		var $classes = array();

		
		//*****************************************************************************************************************************************
		/**
		 * __construct function. Runs automatically on class load.  Creates the base configuration variables and triggers additional methods designed
		 * to connect to the database, determine what view should display, determine if we are running in debug mode, and then load all of the other
		 * classes required by the application. 
		 * 
		 * @access public
		 * @param mixed $page (default: "") An optional mixed string that tells us which page should be loaded. This is passed from index.php where it is obtained from a querystring variable.
		 * @param string $plugin (default: "") An optional mixed string that indicates the plugin that is currently active and should be loaded. This is passed from index.php where it is obtained from a querystring variable.
		 * @return void
		 */
		 //*************************************************************************************************************************************
		function __construct($page="",$plugin="") {
			
			//LETS LOAD OUR CONFIGURATION
			require($this->core . "core/configuration/configuration.inc.php");
			
			//SET THE CONFIG VAR
			$this->set_config($config);
			
			//ATTEMPT TO MAKE A DATABASE CONNECTION
			$this->load_database();
			
			//CHECK PAGE
			$this->determine_view($page,$plugin); 
			
			//DETERMINE IF WE'RE RUNNING IN DEBUG MODE OR NOT. IF SO THEN EMAILS ETC GO TO DEVELOPER
			$this->debug_mode();
						
			//THIS FUNCTION AUTO LOADS CLASSES USED WITHIN THE APPLICATION
			$this->auto_load_classes();
			
			
		}
		
		
		//*************************************************************************************************************************************
		/**
		* set_config function. Ports the ZLMCS configuration values into the ZLCMS class for easy use within functions.
		* 
		* @access private
		* @param array $config An array that contains the configuration data for the site. This information comes form the core/configuration/configuration.inc.php file.
		* @return void
		*/
		//*************************************************************************************************************************************
		private function set_config($config) {
		
			//SET INITIAL CONFIG
			$this->config = $config;
			
		}
		
		
		//*************************************************************************************************************************************
		/**
		* load_database function. This function creates the database connection object $db and the also loads the database abstraction class
		* database.class.php from the database folder. All database queries in the ZLCMS system run through the database abstraction class making
		* it easy to port functionality to another database system should that be required.  There are also a number of helper fucntions to 
		* streamline plugin development in the datbase helper class.  If you will be developing plugins or extending functionality it is advised to review
		* this file carefully.
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function load_database() {	
		
			//CHECK TO SEE IF WE NEED TO CONNECT
			if(!empty($this->config['hostname']) && !empty($this->config['database']) && !empty($this->config['database_username']) && !empty($this->config['database_password'])) {
				include($this->core . "core/database/db.inc.php");
				include($this->core . "core/database/database.class.php");
				$this->db = $db;
				$this->database = new database;
			} else {
				$this->log_error("There are no valid database credentials supplied in the configuration file.");
				die;
			}
		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* determine_view function. his method checks to see if the current page is valid.  If it is a valid page then the function will validate
		* the user's authentication and permissions to make sure they have the proper user level to access this page.
		* 
		* 
		* @access private
		* @param mixed $page A mixed string value that represents the name of the currently displayed / active view.
		* @param mixed $plugin A mixed string value that represents the name of the currently displayed / active plugin.
		* @return void
		*/
		//*************************************************************************************************************************************
		private function determine_view($page,$plugin) {
			if(!empty($page) && empty($plugin)) {
			
				$public = array("login");
				if(!in_array($page,$public)) {
					
					if($this->is_logged()) {
						$this->page = $page;
					} else {
						header("Location: index.php");
						exit;
					}
				} else {
					$this->page = $page;
				}
			} else if(!empty($plugin)) {
			
				if($this->is_logged()) {
					$this->plugin = $plugin;
					if(!empty($page)) {
						$this->page = $page;
					} else {
						header("Location: index.php");
						exit;
					}
				} else {
					$this->plugin = "";
					if($this->is_logged()) {
						$this->page = "dashboard";
					} else {
						$this->page = "login";
					}	
				}
			} else {
				if($this->is_logged()) {
					$this->page = "dashboard";
					$this->plugin = "";
				}
			}
		}
		

		//*************************************************************************************************************************************
		/**
		* debug_mode function. Sets boolean value of debug to true or false based on information contained within the configuration file. 
		* Running in debug mode will provide developers with special information or messages when debugging the application. This toggle can
		* also be used in plugin development for developer testing. For security purposes it is important to disable debug mode in live sites.
		* This setting can be found in the core/configuration/configuration.inc.php file.
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function debug_mode() {
			
			//IF DEBUG ISN'T SET THEN SET IT TO FALUSE 
			if($this->config['debug'] == true) {
				$this->debug = true;
			} else {
				$this->debug = false;
			}
			
			//SET THE DEVELOPER EMAIL FROM CONFIG
			$this->developer = $this->config['debug_developer'];
			
		}
		
		
		//*************************************************************************************************************************************
		/**
		* auto_load_classes function. This function automatically loads of helper and library classes that are set in the autoload section of 
		* the configuration file /configuration/configuration.inc.php.  It creates a subclass with the name matching the value in the 
		* configuration file.
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function auto_load_classes() {
		
			//TYPES OF CLASSES WE CAN ATTEMPT TO AUTOLOAD
			$types = array("helpers","library","plugins");
		
			foreach($types as $type) {
				$dir = $this->core . "core/" . $type . "/";
				if(is_dir($dir)) {
					if($dh = opendir($dir)) {
						while(($file = readdir($dh)) !== false) {
						
							if($type == "plugins") {
								if(is_file($dir . $file . "/back/" . $file . ".back.class.php")) {
									$class = $file;
									include($dir . $file . "/back/" . $file . ".back.class.php");
									$this->$class = new $class;
									$this->classes[$type][] = $class;
								}						
							} else {
								if(preg_match("/.class.php/",$file)){
									$class = str_replace(".class.php","",$file);
									$class = str_replace("-","",$class);
									include($dir . $file);
									$this->$class = new $class;
									$this->classes[$type][] = $class;
								} 
							}
						}
						closedir($dh);
					}
				} 
			}

		}
		
		
		//*************************************************************************************************************************************
		/**
		* initiate function. This function runs and creates all of the configuration values and sets base values for use within the system.
		* These are set by calling subsequent functions that handle specific loading tasks.  
		* 
		* @access public
		* @param mixed $url This is the URL of the site loaded from the HTTP server variables. 
		* @param mixed $uri This is the URI of the site loaded from the HTTP server variables.
		* @return void
		*/
		//*************************************************************************************************************************************
		public function initiate($url,$uri) {
			
			//DO ZLCMS CONSTRUCT
			$this->do_construct();	
			
			//IF THIS USER IS ON THE LOGIN PAGE THEN CHECK TO SEE IF SECURE LOGIN IS REQURED
			if($this->page == "login") {
			
				//THIS ATTEMPTS TO LOAD THE COMPANY BASED OFF THE URL
				$this->load_default_company($url);
				
				//THIS ATTEMPTS TO LOAD THE COMPANY CONFIG BASED OFF THE URL
				$this->load_default_company_config();
				
			}
			
			//LOAD UP AND CREATE THE BASE VALUES
			$this->set_base_values();
	
		}


		//*************************************************************************************************************************************	
		/**
		* do_construct function.This function serves as a constructor for the ZLCMS. It loads the company and config information and stores
		* it in the configuration variable.  Depending on what and where in the process the user is there may be additional configuration
		* information added to the config variable after this point.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		function do_construct() {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;

			//LOAD STANDARD CONFIG FROM DATABASE
			$sql = "SELECT * FROM zlcms_config g, zlcms_company c WHERE c.id=g.company_id AND company_id=? LIMIT 1";
			$query = $zlcms->database->query($sql,array($zlcms->company_id()));
			$number = $query->num_rows();
			
			if($number >= 1) {
				$result = $query->fetch_assoc();
				foreach($result as $key=>$value) {
					$this->config[$key] = $value;
				}
			}
		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* load_default_company function. This method loads the company information from the database.  Generally it uses the domain to determine 
		* which company to load but if the website is in debug mode the the debug domain from the configuration will be used. Note that it is 
		* very important to turn off debugging when the site goes live.
		* 
		* @access protected
		* @param mixed $url A mixed string value that takes the current URL of the website that the ZLCMS is running at. This information is pulled from the server variables.
		* @return void
		*/
		//*************************************************************************************************************************************
		protected function load_default_company($url) {
			
			//LOAD CORE
			global $zlcms;	
			
			//DETERMINE WHICH SITE WE ARE WORKING WITH
			if($this->config['debug'] == true) {
				$this->domain = str_replace("www.","",$this->config['debug_url']);
			} else {
				$this->domain = str_replace("www.","",$url);
			}
			
			//CREATE AN ARRAY OF ELEMENTS
			$class = explode(".",$this->domain);
			
			//BUILD OUR CLASS VARIABLE FROM THE PROPER STRING
			$this->class = str_replace("-", "", $class[0]);

			//QUERY FOR THE COMPANY
			$sql = "SELECT * FROM zlcms_company WHERE class=?";
			$query = $zlcms->database->query($sql,array($this->class));
			$number = $query->num_rows();
			
			//IF THERE IS A RECORD USE IT
			if($number == 1) {
				$this->company = $query->fetch_assoc();			
			} else {
				$this->log_error("Could not select a company from the database for this domain");
			}

		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* load_default_company_config function. This method does a database query to locate the company configuration information in the database. 
		* This method also creates arrays of helpers, library items, plugins, and also secure pages and stores them to the global configuration array.
		* 
		* @access protected
		* @return void
		*/
		//*************************************************************************************************************************************
		protected function load_default_company_config() {

			//LOAD UP CONFIGURATION
			$sql = "SELECT * FROM zlcms_config WHERE company_id=?";
			$query = $this->database->query($sql,array($this->company_id()));
			$number = $query->num_rows();
			
			//IF WE CAN LOAD
			if($number >= 1) {
			
				//NOW LETS USE THEM
				$result = $query->fetch_assoc();
				foreach($result as $key=>$value) {
					$this->config[$key] = $value;
				}

			//LOG ERROR IF WE CAN'T LOAD A COMPANY
			} else {
				$this->log_error("There was a problem loading the company config");
				die;
			}	
		}
		
		
		//*************************************************************************************************************************************
		/**
		* set_base_values function. This method sets all of the base paths for use in the application.  These paths include the 
		* template path, file path, and plugin path both in relative and absolute forms.  These variables are used by plugins and helpers
		* to understand where files and information are located.  If you are noticing invalid values being set check the zlcms_config table 
		* in your database installation.  This information comes from that table. This plugin also determines if the ZLCMS should be running
		* in secure mode or not. If it is then it will use the https connection for the basepath and if it is not then it will use the standard.
		* 
		* @access public
		* @return void
		*/
 		//*************************************************************************************************************************************
		function set_base_values() {			
						
			$base = $_SERVER['HTTP_HOST'];
			$sections = explode(".",$base);
			$num_sections = count($sections);
			
			//CHECK FOR REQUIRED SECURE MODE // IF EXISTS THEN APPLY SECURE SETTINGS
			if(@$this->config['zlcms_secure'] == "true") {	
				$pre = "https://";
				$this->config['base_path'] = str_replace("http://","https://",$this->config['base_path']);
			} else {
				$pre = "http://";
			}
			
			//IF WE ARE IN HTTPS MODE ON ANY PAGE THEN LOAD ASSETS IN SECURE MODE
			if($_SERVER['SERVER_PORT'] == 443) {
				$pre = "https://";
			}

			//HANDLE REQUIRED REDIRECTS TO FORCE USER INTO WWW SECURE MODE IF THEY ARE NOT ALREADY THERE
			if(@$this->config['zlcms_secure'] == "true" && $_SERVER['SERVER_PORT'] != 443) {	
				if($num_sections == 3 || $sections['0'] == "www") {
					header("Location: " . $pre . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
				} else {
					header("Location: " . $pre . "www." . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
				}
			} else {
				if(@$this->config['zlcms_secure'] != "true" && $_SERVER['SERVER_PORT'] == 443) {
					header("Location: " . $pre . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
				} else {			
					if($num_sections == 3 || $sections['0'] == "www") {
						$this->base = $pre . $base . "/";
						$_SESSION['zladmin']['base'] = $this->base;
					} else {
						header("Location: " . $pre . "www." . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
					}
				}
			}
			
			//BUILD CMS DEFAULT BASE
			$this->zlcms_base = $this->base . $this->cms;
			
			//SET THE FILE PATH
			$this->zlcms_templates_path = $this->zlcms_base . "templates/";
				
			//IF THERE IS A PLUGIN THEN WE CAN SETUP ALL PATHS
			if(!empty($this->plugin)) {
			
				//RESET RELATIVE PATH USING CORE VALUE FOR CMS
				$this->config['relative_path'] = $this->core . $this->config['relative_path'];
				
				//PLUGIN BASE DIRECTORY
				$this->plugin_path_full = $this->base . $this->config['core_path'] . $this->config['plugin_path'];
				$this->plugin_path_relative = $this->config['relative_path'] . $this->config['plugin_path'];
			
				//FILE BASE
				$this->file_path_full = $this->base . $this->config['core_path'] . $this->config['file_path'] . $this->config['class'] . "/";
				$this->file_path_relative = $this->config['relative_path'] . $this->config['file_path'] . $this->config['class'] . "/";
				
				//SET THE FILE PATH
				$this->template_path_full = $this->base . $this->config['core_path'] . $this->config['template_path'] . $this->config['class'] . "/" ;
				$this->template_path_relative = $this->config['relative_path'] . $this->config['template_path'] . $this->config['class'] . "/";
				
			//IF THERE IS NO COMPANY THEN SET PATHS ACCORDINGLY
			} else {
							
				//FILE BASE
				$this->file_path_full = "";
				$this->file_path_relative = "";
				
				//SET THE FILE PATH
				$this->template_path_full = "" ;
				$this->template_path_relative = "";
			}
						
			
		}

		
		//*************************************************************************************************************************************
		/**
		* plugin_construct function. This method runs after the main ZLCMS class has been loaded and all variables are formed and is used by 
		* ZLCMS plugin developers as a replacement for the standard PHP construct function. Due to the way the system loads the primary class. 
		* Standard constructors will still fire but they do not have access to the full ZLCMS variable sets. 
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function plugin_construct() {
		
			//SEE IF WE HAVE ANY PLUGINS IN THE CONFIG
			$plugins = @$this->classes['plugins'];

		
			//LOOK FOR OTHER CONSTRUCTS
			if(is_array($plugins)) {
				foreach($plugins as $class) {
					if(method_exists(@$this->$class,"plugin_construct")) {
						$this->$class->plugin_construct();
					} 
				}
			}	
		}


		//*************************************************************************************************************************************
		/**
		 * process_request function. The process request function is a key function for the ZLCMS system. This processes all incoming form and ajax requests.
		 * This function uses a passed variable action to determine which function to run.  The website can only contain a single instance of each action. When
		 * the action variable is detected on page load the process request function will run the proper method.  Thie process request function is first called
		 * in the ZLCMS class and then is executed in each plugin automatically looking for an action match.  If you are looking to process form data or ajax data
		 * within the framework you will want to navigate to the proper plugin and then extend that plugins process request function with your desired action.
		 * 
		 * @access public
		 * @param string $action A mixed string that functions as a keyword to trigger a method.
		 * @return void
		 */
		 //*************************************************************************************************************************************
		public function process_request($action) {

			//ATTEMPT TO LOAD THE MAIN PROCESS FILE. IF IT HAS BEEN REMOVED IT WILL MOVE ON AND LOAD THE TEMPLATE
			if(!empty($action)){

				if($this->is_logged()){
					//AUTOMATIC SESSION EXTENDER (CALLED BY AJAX REQUEST IN zlcms.js)
					if ($action == "stay_alive"){
						$this->stay_alive();

						//SCALLED BY QUERYSTRING WHEN SWITCHING COMPANY FROM ZLCMS COMPANY DROPDOWN
					} else if ($action == "switch_company"){
						$this->switch_company();

						//CALLED ON SUBMISSION OF UPDATE ACCOUNT FORM
					} else if ($action == "update_account"){
						$this->update_account();

						//CALLED ON SUBMISSION OF THE UPDATE USERS FORM
					} else if ($action == "update_users"){
						$this->update_users();

						//CALLED BY REQUEST WHEN DELETING ZLCMS USER
					} else if ($action == "delete_user"){
						$this->delete_user(@$_GET['id']);

						//CALLED BY AJAX WHEN FILTERING ACCCESS LOG
					} else if ($action == "load_access_log"){
						$this->load_access_log(@$_POST['type'], @$_POST['id']);
						die;

						//PROCESS LOGOUT REQUEST
					} else if ($action == "logout"){
						$this->logout();

						//IF A MATCH WAS NOT FOUND THEN SEARCH EACH PLUGIN FOR A MATCH ON KEYWORD ACTION
					} else {
						$plugins = @$this->classes['plugins'];
						if (is_array($plugins)){
							foreach ($plugins as $class){
								if (method_exists(@$this->$class, "process_request")){
									$this->$class->process_request(@$action);
								}
							}
						}
					}
				} else {
					//PROCESS LOGIN
					if ($action == "login"){
						$this->login();
						//PROCESS FORGOT PASSWORD REQUEST
					} else if ($action == "forgot_password"){
						$this->forgot_password();
					}
				}
			}
		}

		//*************************************************************************************************************************************
		/**
		* login function. This method handles a login attempt for the ZLCMS sytem.  If successful the user will be sent to main. If this request
		* fails the user will be returned to the login page.
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function login() {
			
			//IMPORT ZLCMS OBJECT
			global $zlcms;
			
			//SET EMAIL PASSWORD
			$email = @$_POST['email'];
			$password = @$_POST['password'];
			
			//DO QUERY
			$sql = "SELECT * FROM zlcms_users WHERE email=? AND password=? AND active='true'";
			$query = $zlcms->database->query($sql, array(strtolower($email),sha1($password)));
			$number = $query->num_rows();
		
			//IF LOGING COMPLETES
			if($number == 1) {
				$result = $query->fetch_assoc();
				$_SESSION['zladmin']['logged'] = true;
				$_SESSION['zladmin']['user'] = $result;
				
				//DETERMINE IF THIS IS A MULTIPLE COMPANY USE
				if($_SESSION['zladmin']['user']['level'] == 1000) {
					$_SESSION['zladmin']['user']['company'] = true;
				} else {
					if($_SESSION['zladmin']['user']['company_id'] == 0) {
						$_SESSION['zladmin']['user']['company'] = true;
					} else {
						$_SESSION['zladmin']['user']['company'] = false;
					}				
				}
				
				//ADD COMPANY IF USER IS NOT GLOBAL
				if($_SESSION['zladmin']['user']['company_id'] != 0) {
					$sql_company = "SELECT * FROM zlcms_company WHERE id=?";
					$query_company = $zlcms->database->query($sql_company,array($_SESSION['zladmin']['user']['company_id']));
					$number_company = $query_company->num_rows();
					if($number_company == 1) {
						$_SESSION['zladmin']['user']['company_data'] = $query_company->fetch_assoc();
					}
				} else {
					$sql_company = "SELECT * FROM zlcms_company ORDER BY id ASC LIMIT 1";
					$query_company = $zlcms->database->query($sql_company);
					$number_company = $query_company->num_rows();
					if($number_company == 1) {
						$_SESSION['zladmin']['user']['company_data'] = $query_company->fetch_assoc();
						$_SESSION['zladmin']['user']['company_id'] = $_SESSION['zladmin']['user']['company_data']['id'];
					}				
				}

				
				//ADD LOGIN TO ACCESS LOG
				$this->log_event("logged into the system");
				
				//IF AJAX RETURN SUCCES
				if(@$_POST['method'] == "ajax") {
					echo("success");
					die;
				} 
				
			//IF LOGING FAILS
			} else {
			
				if(@$_POST['method'] == "ajax") {
					echo("fail");	
					die;
				} else {
					header("Location: index.php?error=The email or password you entered was invalid.");
					exit;
				}
			}
			
		}
		
		
		//*************************************************************************************************************************************
		/**
		* logout function. This method handles a request for user logout.  It will destroy the current session and redirect the user to the index page.
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function logout() {
			
			//LOG THE USER OUT OF THE SYSTEM // NOTE THE MESSAGE LOG HAPPENS BEFORE THE ACTUAL LOG OUT SO WE STILL HAVE USER DATA
			$this->log_event("logged out of the system");
			
			//CLEAR THE ZLADMIN VAR THAT HOLDS SESSION DATA
			$_SESSION['zladmin'] = array();
			
			//SEND THE USER TO THE LOGIN PAGE
			header("Location: index.php");
			
			//EXIT
			exit;
		}
		
		
		//*************************************************************************************************************************************
		/**
		* forgot_password function. This method handles a form request for a forgotten password. It does a database query to determine if the
		* user is valid. If the user is valid then the script will generate a new password, update the database, and email the user the new password.
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function forgot_password() {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//SET EMAIL VALUE
			$email = @$_POST['email'];
			
			if(!empty($email)) {
			
				//QUERY FOR THE USER
				$sql = "SELECT id FROM zlcms_users WHERE email=?";
				$query = $zlcms->database->query($sql, array(strtolower($email)));
				$number = $query->num_rows();
				
				//IF LOGING COMPLETES
				if($number == 1) {	
					
					//GET USER ID
					$result = $query->fetch_assoc();
					$id = $result['id'];
					
					//GENEATE NEW PASSWORD
					$new_password = substr(sha1(uniqid()), 0,8);
					
					$sql2 = "UPDATE zlcms_users SET password=? WHERE id=?";
					$query2 = $zlcms->database->query($sql2,array(sha1($new_password),$id));
					echo($zlcms->database->error);
					$number2 = $query2->affected_rows;
					
					if($number2 == 1) {
						mail($email,"New ZLCMS Password","Password: " . $new_password,"From: info@gozipline.com");
						echo("success");
						die;
					} else {
						echo("fail");
						die;
					}
					
				} else {	
					echo("fail");
					die;
				}	
				
			} else {
				echo("fail");
				die;
			}
		}
		
		
		//*************************************************************************************************************************************
		/**
		* stay_alive function. This function checks to see if the user is currently logged in. In doing so it helps keep the session from timing
		* out and also helps protect users from editing once their session has ended. It is called via a timed AJAX request in zlcms.js. 
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function stay_alive() {
			if($this->is_logged()) {
				echo("success");
			} else {
				echo("fail");
			}	
			die;
		}

		
		//*************************************************************************************************************************************
		/**
		* switch_company function. This method switches the id of the company currently being managed. This method also refreshes the company
		* data for the company currently being managed.
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function switch_company() {
		
			//GET ID
			$id = $_GET['id'];
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
		
			if($this->is_logged()) {
				if($this->has_company_access()) {
					
					$sql = "SELECT * FROM zlcms_company WHERE id=?";
					$query =  $zlcms->database->query($sql,array($id));
					$number = $query->num_rows();
				
					if($number >= 1) {
						$_SESSION['zladmin']['user']['company_id'] = $id;
						$result = $query->fetch_assoc();
						$_SESSION['zladmin']['user']['company_data'] = $result;
						$this->log_success("You are now editing " . $result['company']);
						$this->page = "dashboard";
					} else {
						$this->log_error("The company you tried to access no longer exists");
						$this->page = "dashboard";
					}
					
				} else {
					$this->log_error("You do have access to change companies");
				}
			} else {
				$this->force_logout();
			}
		}
		
		
		//*************************************************************************************************************************************		
		/**
		* update_account function. This method is used to update account data for a user when the account form is submitted.
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function update_account() {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//CHECK REQUIRED FIELDS
			$this->form->set_rule(@$_POST['name'],"empty|name","You must provide a valid name to continue.");
			$this->form->set_rule(@$_POST['email'],"empty|email","You must provide a valid email address to continue.");
			
			//VALIDATE PASSWORD
			if(!empty($_POST['password'])) {
				$this->form->set_rule(@$_POST['confirm_password'],"empty","You must confirm your password if you wish to change it.");
				$this->form->set_rule(@$_POST['password'],"value","The value you enter for Password must match the value in the Confirm Password field",$_POST['confirm_password']);
			}
			
			//RUN RULES
			if($this->form->run_rules()) {
			
				//FIELDS
				$fields = array("name","email","address","city","state","zip","phone");
				
				//ADD PASSWORD
				if(!empty($_POST['password'])) {
					$fields[] = "password";
					$_POST['password'] = sha1($_POST['password']);
				}
				
				//UPDATE RECORDS
				$query = $this->database->update_auto_helper($fields,$_POST,"zlcms_users","WHERE id=?",$arguments = array($_SESSION['zladmin']['user']['id']));
				
				//IF SUCCESSFUL MOVE ON OTHERWISE SHOW ERROR
				if($query) {
					$this->log_success("Your information has been updated.");
					$this->log_event("updated their account information.");
				} else {
					$this->log_error("There was a problem storing your information. Error: " . $this->database->error);
				}
			
			} else {
				$this->log_error($this->form->error);
			}
		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* update_users function. This method is used to update account data for a user when the users form is submitted
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************		
		private function update_users() {
		
			//LOAD THE COMPANY FOR USE WITHIN
			$this->load_company($this->company_id());
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//CHECK REQUIRED FIELDS
			$this->form->set_rule(@$_POST['name'],"empty|name","You must provide a valid name to continue.");
			$this->form->set_rule(@$_POST['email'],"empty|email","You must provide a valid email address to continue.");
			
			//VALIDATE PASSWORD
			if(empty($_POST['id'])) {
				$this->form->set_rule(@$_POST['confirm_password'],"empty","You must confirm your password if you wish to change it.");
				$this->form->set_rule(@$_POST['password'],"value","The value you enter for Password must match the value in the Confirm Password field",$_POST['confirm_password']);
			} else {
				if(!empty($_POST['password'])) {
					$this->form->set_rule(@$_POST['confirm_password'],"empty","You must confirm your password if you wish to change it.");
					$this->form->set_rule(@$_POST['password'],"value","The value you enter for Password must match the value in the Confirm Password field",$_POST['confirm_password']);
				}
			}
			
			//RUN RULES
			if($this->form->run_rules()) {
			
				//ADD PASSWORD
				if(!empty($_POST['password'])) {
					$fields[] = "password";
					$_POST['password'] = sha1($_POST['password']);
				} else {
					unset($_POST['password']);
				}
		
			
				//FIELDS
				$fields = $this->database->convert_array_to_fields($_POST,array("id","action","submit","confirm_password"));

				//UPDATE RECORDS
				if(!empty($_POST['id'])) {
					if(!empty($_POST['active'])) {
						if($_POST['active'] == true) {
							$this->send_request_to_zipline($zlcms->company['domain'],"store_user",$_POST['name'] . "|" . $_POST['email'] . "|true");
						}
					}
					$query = $this->database->update_auto_helper($fields,$_POST,"zlcms_users","WHERE id=?",$arguments = array($_POST['id']));
				} else {
				
					//CHECK TO MAKE SURE THAT USER DOESN'T EXIST
					$sql_check = "SELECT id,level,active FROM zlcms_users WHERE email=? AND company_id=? OR email=? AND company_id=?";
					$query_check = $zlcms->database->query($sql_check,array($_POST['email'],$zlcms->company_id(),$_POST['email'],0));
					$number_check = $query_check->num_rows();
				
					//IF NO USER ADD IT
					if($number_check == 0) {
						$this->send_request_to_zipline($zlcms->company['domain'],"store_user",$_POST['name'] . "|" . $_POST['email'] . "|true");
						$query = $this->database->insert_auto_helper($fields,$_POST,"zlcms_users");
					} else {
						$result_check = $query_check->fetch_assoc();
						if($result_check['active'] == "false") {
							if($result_check['level'] <= $zlcms->level()) {
								$_POST['active'] = "true";
								$fields[] = "active";
								$this->send_request_to_zipline($zlcms->company['domain'],"store_user",$_POST['name'] . "|" . $_POST['email'] . "|true");
								$query = $this->database->update_auto_helper($fields,$_POST,"zlcms_users","WHERE id=?",$arguments = array($result_check['id']));
							} else {
								$query = false;
								$this->log_error("You do not have permission to update this user.");
							}
						} else {
							$query = false;
							$this->log_error("There is already a user with the email address {$_POST['email']}");
						}
					}
				}
				
				//IF SUCCESSFUL MOVE ON OTHERWISE SHOW ERROR
				if($query) {
					$this->log_event("updated the user {$_POST['email']}");
					$message = "Your user update has been completed.";
					header("Location: " . $this->base(true) . "index.php?page=users&success=" . $message);
				} else {
					if(!is_array($this->error) || count($this->error) < 1){
						$this->log_error("There was a problem updating your user information.");
					}
				}
			
			} else {
				$this->log_error($this->form->error);
			}
		
		}
		
		//*************************************************************************************************************************************
		/**
		* delete_user function. This method is used to update account data for a user when the users form is submitted.
		* 
		* @access private
		* @param integer $id The database ID of the user to be deleted.
		* @return void
		*/
		//*************************************************************************************************************************************		
		private function delete_user($id) {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
				
			//LOAD THE COMPANY FOR USE WITHIN
			$this->load_company($this->company_id());
			
			//MAKE SURE VALID PERMISSION
			$sql = "SELECT * FROM zlcms_users WHERE id=? AND level <=?";
			$query = $zlcms->database->query($sql,array($id,$zlcms->level()));
			$number = $query->num_rows();
			
			if($number >= 1) {
				
				//GET RESULT
				$result = $query->fetch_assoc();
				
				//WE DO NOT DELETE USERS WE SET THEM TO INACTIVE
				$sql = "UPDATE zlcms_users SET active=? WHERE id=?";
				$query = $zlcms->database->query($sql,array("false",$id));
				if($query) {
					$this->send_request_to_zipline($zlcms->company['domain'],"store_user",$result['name'] . "|" . $result['email'] . "|false");
					$this->log_success("The user {$result['email']} has been removed from the system.");
					$this->log_event("removed the user {$result['email']} from the system");
				} else {
					$this->log_error("The user {$result['email']} could not be removed from the system.");
				}
				
			} else {
				$this->log_error("That user does not exist or you do not have permission to delete them.");
			}
			
		}
	
		
		//*************************************************************************************************************************************
		/**
		* load_access_log function. This method gets the entire access log.  It also supports multiple parameters that can be passed via AJAX to limit results 
		* 
		* @access public
		* @param string $type (default: "") A string that contains information about the type of log to display. Types include plugin, user, all or empty.
		* @param string $id (default: "") An otpioanl ID that can be passed for filtering purposes.
		* @return void
		*/
		//*************************************************************************************************************************************
		public function load_access_log($type="",$id="") {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//RESET OUTPUT
			$output = "";
		
			//TOGGLE CSS
			$css = "bgLight";
		
			//COMPANY
			$company = $zlcms->company_id();
			$sql_company = "SELECT company FROM zlcms_company WHERE id=?";
			$query_company = $zlcms->database->query($sql_company,array($company));
			$result_company = $query_company->fetch_assoc();
		
			if($type == "plugin") {
				$sql = "SELECT a.id,a.message,u.name,a.entered FROM zlcms_accesslog a, zlcms_users u WHERE a.user_id=u.id AND a.plugin_id=? AND u.level <= ?  AND a.company_id in({$company},0) ORDER BY a.entered DESC LIMIT 100 ";
				$query = $zlcms->database->query($sql,array($id,$zlcms->level()));
			} else if($type == "user") {
				$sql = "SELECT a.id,a.message,u.name,a.entered FROM zlcms_accesslog a, zlcms_users u WHERE a.user_id=u.id AND a.user_id=? AND u.level <= ?  AND a.company_id in({$company},0) ORDER BY a.entered DESC LIMIT 100 ";
				$query = $zlcms->database->query($sql,array($id,$zlcms->level()));
			} else {
				$sql = "SELECT a.id,a.message,u.name,a.entered FROM zlcms_accesslog a, zlcms_users u WHERE a.user_id=u.id AND u.level <= ?  AND a.company_id in({$company},0) ORDER BY a.entered DESC LIMIT 100 ";
				$query = $zlcms->database->query($sql,array($zlcms->level()));
			} 
		
			
			//OUTPUT RESULTS
			$number = $query->num_rows();
			if($number >= 1) {
			
				$output .= "<div class=\"accessLogWrapper roundCorners3 bgDark\">";
					$output .= "<div class=\"full sixty-sm pr-sm\"><h5 class=\"color7\">Description:</h5></div>";
					$output .= "<div class=\"full forty-sm\"><h5 class=\"color7\">Date:</h5></div>";
				$output .= "</div>";
			
				$n = 1;
			
				while($result = $query->fetch_assoc()) {
					
					//TOGGLE CSS
					if($css == "bgMid") {
						$css = "bgLight";
					} else {
						$css = "bgMid";
					}
				
					//INJECT ROUND
					if($n == $number) {
						$inject = " roundbottom";
					} else {
						$inject = "";
					}
				
					$output .= "<div class=\"accessLogWrapper roundCorners3 {$css}{$inject}\">";
						$output .= "<div class=\"full sixty-sm pr-sm\"><p>" . $result['name'] . " " . $result['message'] . " for " . $result_company['company'] . "</p></div>";

						// Use this line if the database is in UTC timezone
						$output .= "<div class=\"full forty-sm\"><p>" . date("Y-m-d g:i:s", strtotime($result['entered'] . " UTC")) . "</p></div>";

						// Use this line if the database is in the same timezone as the php server
						//$output .= "<div class=\"full forty-sm\"><p>" . $result['entered'] . "</p></div>";
					$output .= "</div>";
					$n++;
				}
				echo($output);	
				
			} else {
				echo("<h3>Sorry</h3><p>Your request did not return any results</p>");
			}
		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* header_title function. The header title method can be used within plugins to output content in the title bar of the ZLCMS for a
		* particular view.  To use this method you will add a method header_title() to your plugin.back.class.php.  This method will be 
		* called automatically when any page of your plugin is loaded and will output whatever content your function returns as in a H1 tag.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function header_title() {

			$output = "";
			
			//IF THIS IS A PLUGIN SEE IF HEADER TITLE VARIABLE IS SET
			if(!empty($_GET['plugin'])) {
			
				$plugin  = $_GET['plugin'];
				if(method_exists(@$this->$plugin,"header_title")) {
					$output = $this->$plugin->header_title();
				} 

			
			//IF ITS NOT SEE IF THIS IS THE DASHBOARD
			} else {
			
				if($this->page == "dashboard") {
					$output = "Dashboard";
				}
			
			}
			
			
			if(!empty($output)) {
				$output = "<h1>" . $output . "</h1>";
				echo($output);
			}
		
		}


		//*************************************************************************************************************************************
		/**
		* header_bar function. The header title method can be used within plugins to output the search or other elements in the title bar of 
		* the ZLCMS for a particular view.  To use this method you will add a method header_bar() to your plugin.back.class.php.  This method will be 
		* called automatically when any page of your plugin is loaded and will output whatever content your function returns as HTML.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function header_bar() {

			$output = "";
			
			//IF THIS IS A PLUGIN SEE IF HEADER TITLE VARIABLE IS SET
			if(!empty($_GET['plugin'])) {
			
				$plugin  = $_GET['plugin'];
				if(method_exists(@$this->$plugin,"header_bar")) {
					$output = $this->$plugin->header_bar();
				} 

			
			//THIS IS RESERVED FOR SPECIAL NON PLUGIN CODE USED BY ZLCMS.
			} else {
			
				//THIS IS LEFT OPEN FOR USE BY ZLCMS IF NEEDED
			
			}
				
			if(!empth($output)) {
				echo($output);
			}
		
		}


		//*************************************************************************************************************************************
		/**
		* load_view function. This method loads the proper template based on the value determined by the get_page() function.
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function load_view() {
			if(!empty($this->plugin)) {
				if(!empty($this->page)) {
					include($this->core . "core/plugins/" . $this->plugin . "/back/" . $this->plugin . ".back." . $this->page . ".view.php");
				} else {
					include($this->core . "core/plugins/" . $this->plugin . "/back/" . $this->plugin . ".back.index.view.php");
				}
			} else {
				include("templates/views/" . $this->page . ".view.php");
			}
		}
		

		//*************************************************************************************************************************************
		/**
		* validate_license function. This method makes a request to Zipline Interactive via the send_request_to_zipline() function. 
		* This system validates the license and then returns key data and metrics required for proper display and usage of the ZLCMS.
		* If your instllation is getting licensing notification messages it is due to the failure of this request to find a license.
		* If you are seeing this contact Zipline Interactive at (info@ziplineinteractive.com) to have your license examined immediately.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function validate_license() {
	
			//IMPORT GLOBAL 
			global $zlcms;
			
			//IF A COMPANY ID IS SET THEN GET THE SETTINGS FROM REMOTE SYSTEM
			if($zlcms->company_id() != 0 && $this->page != "login"){
				
			  	if($zlcms->company_id() != @$_SESSION['zladmin']['response']['license']['company_id']) {
					$this->load_company($zlcms->company_id());					
					$response = $this->send_request_to_zipline($this->company['domain'],"lookup");
				
					if(!empty($response)) { 
					  $_SESSION['zladmin']['response'] = $response;
					  $_SESSION['zladmin']['response']['license']['company_id'] = $this->company_id();
					} else {
					  $_SESSION['zladmin']['response'] = array();
					  $this->log_error("There was an error communicating with the remote Zipline Interactive server.");
					}
			  	}
			} 

		}
		
		
		//*************************************************************************************************************************************
		/**
		* load_template function. This function includes the base template into index.php.  Additional layers of templates will be included 
		* within the main template.php file.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function load_template() {
		
			//IF THE USER IS LOGGED IN LOAD THE TEMPLATE IF NOT LOAD THE LOGIN PAGE
			if($this->is_logged()) {
				include("templates/template.php");
			} else {
				include("templates/login.php");
			}
		}
			

		//*************************************************************************************************************************************
		/**
		* path function. This function is used to when making a full base path link to an image or asset contained in the templates folder
		* of the in the ZLCMS. Properly using path() on all assets allows hte ZLCMS to automatically toggle between secure and standard connections.
		* It also allows for plugins to be easily portable between installations.
		* 
		* @access public
		* @param bool $return (default: false) A boolean value that indicates if we should return the base path or output it directly. 
		* @return string $zlcms_templates_path A string representing the current ZLCMS template path.
		*/
		//*************************************************************************************************************************************
		public function path($return = false) {
			
			//DETERMINE TO OUTPUT OR RETURN THE BASE DIRECTORY
			if($return == true) {
				return $this->zlcms_templates_path;
			} else {
				echo($this->zlcms_templates_path);
			}
			
		}
		
		
		//*************************************************************************************************************************************
		/**
		* base function. This function is used to when making a full base path link for a href or other need.  
		* Properly using base() on all assets allows hte ZLCMS to automatically toggle between secure and standard connections.
		* It also allows for plugins to be easily portable between installations.
		* 
		* @access public
		* @param bool $return (default: false) A boolean value that indicates if we should return the base path or output it directly. 
		* @return string $this->base A string representing the current ZLCMS base path.
		*/
		//*************************************************************************************************************************************
		public function base($return = false) {
			
			//DETERMINE TO OUTPUT OR RETURN THE BASE DIRECTORY
			if($return == true) {
				return $this->base . $this->cms;
			} else {
				echo($this->base . $this->cms);
			}
			
		}
		
		
		//*************************************************************************************************************************************
		/**
		* load_javascript function. This function loads the plubin javascript elements automatically if they exist. The function
		* will loop through each plugin folder and look for a file formatted as plugin.back.js.  If it finds one it will automatically
		* include it in the template file. If it doesn't find a file it moves on to the next plugin until all javascript has been 
		* loaded into the template.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function load_javascript() {
				
			//CLEAR OUTPUT FOR USE
			$output = "";
			
			//SET THE PLUGIN DIRECTORY
			$loc = "core/plugins/";
			$dir = $this->core . $loc;
							
			if(is_dir($dir)) {
				if($dh = opendir($dir)) {
					while(($folder = readdir($dh)) !== false) {
						if(is_dir($dir . $folder)) {
							
							if(is_file($dir . $folder . "/back/" . $folder . ".back.js")) {
								$output .= '<script src="' .  $this->base . 'core/plugins/' .  $folder . '/back/' . $folder . '.back.js"> </script>' . "\n";
							}
						} 
					}
					closedir($dh);
				}
			} 
			
			//SEND OUTPUT IF EXISTS<br />
			if(!empty($output)) {
				echo($output);
			}
		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* load_css function. This function loads the plubin css elements automatically if they exist. The function
		* will loop through each plugin folder and look for a file formatted as plugin.back.css.  If it finds one it will automatically
		* include it in the template file. If it doesn't find a file it moves on to the next plugin until all css has been 
		* loaded into the template.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function load_css() {
				
			//CLEAR OUTPUT FOR USE
			$output = "";
			
			//SET THE PLUGIN DIRECTORY
			$loc = "core/plugins/";
			$dir = $this->core . $loc;
							
			if(is_dir($dir)) {
				if($dh = opendir($dir)) {
					while(($folder = readdir($dh)) !== false) {
						if(is_dir($dir . $folder)) {
							if(is_file($dir . $folder . "/back/" . $folder . ".back.css")) {
								$output .= '<link href="' . $this->base . 'core/plugins/' .  $folder . '/back/' . $folder . '.back.css" rel="stylesheet" type="text/css" />' . "\n";
							}
						} 
					}
					closedir($dh);
				}
			} 
			
			//SEND OUTPUT IF EXISTS<br />
			if(!empty($output)) {
				echo($output);
			}
		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* load_class function. This function can be called to add a class as as subclass of the ZLCMS system. This is commonly used to 
		* add in helpers that are only used in a single location like mail or to add in additional helpers or functionality not included
		* in the core.  This is common when working with 3rd parth PHP APIs like Facebook, Google, and many others.  Note: To use load class
		* your file must be saved in the format $type/$class.class.php and the class name must match exactly (including case) with the value of
		* $class. If these elements match and the function is called the class will be available as part of the global $zlcms object as $zlcms->class
		* where class is $class.  Note: This is intended for classes used in a single location. If you are going to be using it on every page or 
		* in many areas it may be easier to take advantage of the auto_load_classes() function to automatically load in your class.
		* 
		* @access public
		* @param mixed $class The name of the class that is to be loaded.
		* @param mixed $type The type of class that is to be loaded.  The type represents a directory such as helper, library, tools.
		* @return void
		*/
		//*************************************************************************************************************************************
		public function load_class($class,$type) {

			if(is_file($this->core . "{$type}/{$class}.class.php")) {
				require($this->core . "{$type}/{$class}.class.php");
				$this->$class = new $class;
			} else {
				$this->log_error("Could not load helper class: {$class}");
			}
		}
		
		
		//*************************************************************************************************************************************
		/**
		* log_error function. This function is used to assist in storing all error messages in an easy to access array.  These errors can be
		* created during the installation / ZLCMS setup process or they can be generated by developers in plugins.  Calling this method when
		* developing a plugin will add the error message to the view and trigger the displaying of that error on the next page load.
		* 
		* @access public
		* @param array/string $error An array that contains all of the errors that have occurred while loading the page.
		* @return void
		*/
		//*************************************************************************************************************************************
		public function log_error($error) {
			if(is_array($error)) {
				foreach($error as $errors) {
					$this->error[] = $errors;
				}
			} else {
				$this->error[] = $error;
			}
			
			$this->display_errors();
			
		}
		
		
		//*************************************************************************************************************************************
		/**
		* display_errors function. This function is designed for use with runtime errors that are thrown when constructing the ZLCMS. This 
		* fucntion is called to cause those errors to output to the browser. 
		* 
		* @access private
		* @return void
		*/
		//*************************************************************************************************************************************
		private function display_errors() {
			if($this->config['installation_errors'] == true) {
				if(is_array($this->error)) {
					foreach($this->error as $error) {
						echo("<p>" . $error . "<p>");
					}
					
	
					//CELAR ERROR AFTER DISPLAY
					$this->error = array();
					
				} 
			}
		}
		
		
		//*************************************************************************************************************************************
		/**
		* log_success function. This function is used to assist in storing all success messages in an easy to access array.  These messages can be
		* created during the installation / ZLCMS setup process or they can be generated by developers in plugins.  Calling this method when
		* developing a plugin will add the success message to the view and trigger the displaying of that success on the next page load.
		* 
		* @access public
		* @param array $error An array that contains all of the errors that have occurred while loading the page.  
		* @return void
		*/
		//*************************************************************************************************************************************
		public function log_success($success) {
			if(is_array($success)) {
				foreach($success as $success) {
					$this->success[] = $success;
				}
			} else {
				$this->success[] = $success;
			}
		}

		
		//*************************************************************************************************************************************
		/**
		* send_request_to_zipline function. This function send a licensing request to Zipline when a user logs into the system. Zipline will
		* return licensing information and other element required for proper display of the ZLCMS.
		* 
		* @access private
		* @param mixed $domain A mixed string that contains the domain of the website we are validating.
		* @param mixed $request A mixed string that contains a keyword indicating the type of request we are sending to Zipline.
		* @return array An JSON decoded array of data required to properly validate and rendor this companies installation.
		*/
		//*************************************************************************************************************************************
		private function send_request_to_zipline($domain,$request) {
								
			//MAKE REQUEST
			$ch = curl_init(); 
			curl_setopt($ch, CURLOPT_URL,"http://zlcms3.ziplineinteractive.com/"); 
			curl_setopt($ch, CURLOPT_POST, 1);  
			curl_setopt($ch, CURLOPT_POSTFIELDS, "domain=" . $domain . "&request=" . $request); 
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_TIMEOUT,1000);
			$results = curl_exec($ch); 
			curl_close ($ch); 
			
			$results = json_decode($results,true);
			
			//HANDLE RESULTS
			if(!empty($results)) {
				return $results;
			}
		}

		
		//*************************************************************************************************************************************
		/**
		* show_message function. This function will display any success or error messages when called. It has varying output for users that 
		* are logged and users that are not.  This function examins the $error and $success arrays and builds formatted HTML off of the returned
		* content. If there are no messages then the DIVs display hidden so that they can be populated with Javascript if the need arises.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function show_message() {
			
			//CLEAR OUTPUT FOR USE
			$output = "";
			
			//IF THERE IS A QUERYSTING ERROR LOG IT
			if(!empty($_GET['error'])) {
				$this->log_error($_GET['error']);
			}
			
			//IF THERE IS A QUERYSTRING SUCCESS LOG IT
			if(!empty($_GET['success'])) {
				$this->log_success($_GET['success']);
			}
			
			//SHOW MESSAGES ON ALL STANDRD PAGES
			if($this->page != "login") {
				
				$hide = " hide";
				$color = "";
				$items = "";
				$output = "";
				
				//IF THERE IS AN ERROR SHOW IT
				if(!empty($this->error) && is_array($this->error)) {
					
					$items = "";
					$hide = " show";
					$color = " failed";
					$icon = " thumbsDownIcon";
					foreach($this->error as $error) {
						$items .= '<h5 class="color7">' . $error . "</h5>";
					}	
					
					$output .= '
							<div class="'.$hide.$color.'Alert">
								<span class="iconSprite'.@$icon.'"></span>
								<h5 class="color7">' . $error . '</h5>
							</div>';
						
				} 
				
				//IF THERE IS A SUCCESS SHOW IT
				if(!empty($this->success) && is_array($this->success)) {
					
					$items = "";
					$hide = " show";
					$color = " success";
					$icon = " thumbsUpIcon";
					foreach($this->success as $success) {
						$items .= '<h5 class="color7">' . $success . "</h5>";
					}	
					
					$output .= '
							<div class="'.$hide.$color.'Alert">
								<span class="iconSprite'.@$icon.'"></span>
								'.$items . '
							</div>';

				} 

			//SHOW MESSAGES ON THE LOGIN PAGE SPECIFICALLY // USES DIFFERENT FORMATTING
			} else {
			
				$hide = " hide";
				$color = "";
				$items = "";
			
				//IF THERE IS AN ERROR MESSAGE SHOW IT
 				if(!empty($this->error) && is_array($this->error)) {
 					$hide = " show";
					$color = " error";
 					foreach($this->error as $error) {
						$items .= "<p>" . $error . "</p>";
 					}	
 				} 

 				//IF THERE IS A SUCCESS MESSAGE SHOW IT
 				if(!empty($this->success) && is_array($this->success)) {
 					$hide = " show";
 					$color = " success";
 					foreach($this->success as $success) {
						$items .= "<p>" . $success . "</p>";
 					}	
 				} 
 				
 				$output = '
				<div class="full message'.$hide.$color.'">
					'.$items.'
				</div>';



			
			}
			
			//SEND OUTPUT
			if(!empty($output)) {
				echo($output);
			}
		}
		
		
		//*************************************************************************************************************************************
		/**
		* is_logged function. This is a simple utility function that is used by the ZLCMS system and plugin developers to determine if 
		* a user is logged in before they perform an activity. If the user is logged in and has an active session the function will return
		* true and if the user is not logged in the function will return false.  This function should be used when developing plugins or 
		* other tools to determine the current authentication status of a user.
		* 
		* @access public
		* @return boolean Returns true of the user is logged and false if the user is not logged.
		*/
		//*************************************************************************************************************************************
		public function is_logged() {
			
			if(!empty($_SESSION['zladmin']['logged'])) {
				
				if($_SESSION['zladmin']['logged'] == true) {
					$logged = true;
				} else {
					$logged = false;
				}
			} else {
				$logged = false;
			}
			return $logged;
		}
		
		
		//*************************************************************************************************************************************
		/**
		* level function. This is a simple utility function that is used by the ZLCMS system and plugin developers to determine the current
		* authentication level of a user.  The ZLCMS uses a numeric permission system.  The higher the number on a scale of 0-100 the more 
		* access a user has.  This function is used to easily capture that permission level so that functions can be restricted to a particular
		* user group.  This is used in the ZLCMS core and by plugin developers to restrict actions.  It is best to handle this restriction with
		* simple math and checks for example if $this->level() > 250 then you can do something. To find out what levels currenlty exist for a site
		* check the zlcms_levels table for the site you are managing.
		* 
		* @access public
		* @return integer A integer representing the current permission level of the logged in user.
		*/
		//*************************************************************************************************************************************
		public function level() {
			$current_level = @$_SESSION['zladmin']['user']['level'];
			if(!empty($current_level)) {
				if(is_numeric($current_level)) {
					$level = $current_level;
					return $level;
				} else {
					$this->force_logout();
				}
			} else {
				$this->force_logout();
			}
		}
		
		
		//*************************************************************************************************************************************
		/**
		* has_company_access function. This is a simple utility function that is used by the ZLCMS system and plugin developers to determine if
		* the current user has access to change companies.  Should your plugin or utlity require users to switch companies you must make sure
		* that the user has access.  This function will return a boolean true or false value indicating if the user has access to a single company
		* false or all companies true.
		* 
		* @access public
		* @return boolean A boolean value indicating if the user has access to a single company or all companies in the system.
		*/
		//*************************************************************************************************************************************
		public function has_company_access() {
			$current_company_access = @$_SESSION['zladmin']['user']['company'];
			if($current_company_access == true) {
				$company_access = true;
			} else {
				$company_access = false;
			}	
			return $company_access;
		}
		

		//*************************************************************************************************************************************		
		/**
		* company_class function. The class is an important variable in the ZLCMS. It helps route request to the proper template and storage
		* area.  This utility class is used by the ZLCMS and plugin developers when trying to access files, templates, or library elements
		* for a particular company.
		* 
		* @access public
		* @return string A string containing the name of the currently active company class.  
		*/
		//*************************************************************************************************************************************
		public function company_class() {
			$current_class = @$_SESSION['zladmin']['user']['company_data']['class'];
			if(!empty($_SESSION['zladmin']['user']['company_data']['class'])) {
				$class = $_SESSION['zladmin']['user']['company_data']['class'];
			} else {
				$class = $this->log_error("User has no company class.");
			}
			return $class;
		}

		
		//*************************************************************************************************************************************
		/**
		* is_update function. This is a simple utility function that is used by the ZLCMS system and can be used by plugin developers to add
		* a hidden field to the document with the id of the current item. If there is no id set the function does not return anything. 
		* This function can be helpful when using add/edit on the same page to make sure that the ID only shows if it is set in the querystring.
		* 
		* @access public
		* @param integer $id The database ID of the item currently being edited.
		* @return void
		*/
		//*************************************************************************************************************************************
		public function is_update($id) {
			if(!empty($id)) {
				echo('<input type="hidden" name="id" id="id" value="' . $id . '" />');
			}
		}
		
		
		//*************************************************************************************************************************************
		/**
		* company_id function. This is a utility function that is used by the ZLCMS and plugin developers.  It will look at what company is 
		* currently active for this user and return the ID of that company. When developing plugins designed to work for more than one company.
		* this function can be used when updating, storing, or removing data to confirm that it is connected to the proper company. 
		* 
		* @access public
		* @return integer An integer represents the ID of the company currently being managed.
		*/
		//*************************************************************************************************************************************
		public function company_id() {
		
			if(!empty($_SESSION['zladmin']['user']['company_id'])) {
				if($_SESSION['zladmin']['user']['company_id'] >= 1) {
					$company = $_SESSION['zladmin']['user']['company_id'];
				} else {
					$company = 0;
				}
			} else {
				if(!empty($this->company)) {
					$company = $this->company['id'];
				} else {
					$company = 0;
				}	
			}
			
			return $company;
		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* user_id function. This is a utility function that is used by the ZLCMS and plugin developers.  It will look at what user is 
		* currently active. It will then return the database ID of that user so records, logs, and other information can be attached to
		* this user. 
		* 
		* @access public
		* @return integer An integer represents the ID of the user currently logged in.
		*/
		//*************************************************************************************************************************************
		public function user_id() {
		
			$current_user =  @$_SESSION['zladmin']['user']['id'];
			if($current_user >= 1) {
				$user = $current_user;
			} else {
				$user = 0;
			}
			
			return $user;
		
		}
		

		//*************************************************************************************************************************************
		/**
		* force_logout function. This is a utility function that is used by the ZLCMS and plugin developers. It can be called to force a user
		* to log out of the system. It is designed to be used in the case of an illegal activity or error to make sure the user is logged out.
		* and redirected to the login page. 
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function force_logout() {
			$_SESSION['zladmin'] = array();
			$this->page = "login";
			$this->plugin = "";
		}
		
		
		//*************************************************************************************************************************************		
		/**
		* get_form_action function.  This is a utility function that is used by the ZLCMS and plugin developers. It can be called within 
		* plugins to build a properly formatted form action automatically. 
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function get_form_action() {
			echo($this->base(true) . str_replace("/zlcms/","",$_SERVER['REQUEST_URI']));
		}
		

		//*************************************************************************************************************************************
		/**
		* log_event function. This is a utility function that is used by the ZLCMS and plugin developers. It can be called and should be called
		* anytime a plugin completes a save, update, or delete method as well as any other time something should be written to the access log.  
		* This method will automatically collect information about the uesr and plugin and will write the information to the log. As a plugin
		* developer it is important to use this functoin during plugin development so that proper access logs are created.
		* 
		* @access public
		* @param string $message A string that contains the message to be logged.
		* @return void
		*/
		//*************************************************************************************************************************************
		public function log_event($message) {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
		
			//PREPARE VARIABLES
			$user_id = @$_SESSION['zladmin']['user']['id'];
			$company_id = @$_SESSION['zladmin']['user']['company_id'];
			
			//CHECK FOR PLUGIN IF THERE IS ONE TRANSLATE THE TEXT NAME TO ID FOR STORAGE
			if(!empty($this->plugin)) {
				$sql = "SELECT * FROM zlcms_plugins WHERE plugin=? AND company_id=?";
				$query = $zlcms->database->query($sql,array($this->plugin, $zlcms->company_id()));
				$number = $query->num_rows();
				if($number == 1) {
					$result = $query->fetch_assoc();
					$plugin_id = $result['id'];
				} else {
					$plugin_id = 0;
				}
			} else {
				$plugin_id = 0;
			}
			
			$insert = "INSERT INTO zlcms_accesslog (user_id,company_id,plugin_id,message) VALUES (?,?,?,?)";
			$insert_query = $zlcms->database->query($insert,array($user_id,$company_id,$plugin_id,$message));
			$insert_affected = $insert_query->num_rows();
		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* navigation function. This function determines the proper top navigation to show based on if the user is logged in or not and the 
		* user's corresponding permmission level.  Not all users are allowed to perform all tasks.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function navigation() {
		
			//CLEAR OUTPUT FOR USE
			$output = "";
		
			//CHECK TO MAKE SURE THE USER IS LOGGED IN
			if($this->is_logged() && $this->level() > 1) {
		
				$output .= '<nav>';
					$output .= '<ul>';
						$css = ($this->page == "dashboard") ? "active" : "inactive";
						$output .= '<li class="'.$css.'"><a href="index.php?page=dashboard"><span class="iconSprite doubleFlag"></span><span class="navItem">Dashboard</span></a></li>';
						$output .= $this->choose_manage();
					$output .= '</ul>';
					
					$output .= '<ul>';
					
						if($this->company_id() > 0) { 
						
							if($this->level() >= 250) {
								$css = ($this->page == "users") ? "active" : "inactive";
								$output .= '<li class="'.$css.'"><a href="index.php?page=users"><span class="iconSprite doublewrench"></span><span class="navItem">Users</span></a></li>';
							}
							$css = ($this->page == "account") ? "active" : "inactive";
							$output .= '<li class="'.$css.'"><a href="index.php?page=account"><span class="iconSprite gearsIcon"></span><span class="navItem">Account</span></a></li>';
							$css = ($this->page == "inactive") ? "active" : "inactive";
							$output .= '<li class="'.$css.'"><a href="index.php?page=accesslog"><span class="iconSprite battery"></span><span class="navItem">Access Log</span></a></li>';

						}
						
						$output .= '<li><a href="index.php?action=logout"><span class="iconSprite openLock"></span><span class="navItem">Log out</span></a></li>';
					$output .= '</ul>';
					
					
				$output .= '</nav>';
				
				//SEND OUTPUT
				echo($output);
			
			}
		
		}
		
	
		//*************************************************************************************************************************************
		/**
		* choose_company function. If a user has access to multiple companies this function will load a dropdown menu that allows the user to 
		* jump between the companies they have access to.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function choose_company() {
			
			//GLOBAL ZLCMS
			global $zlcms;
			
			//CLEAR OUTPUT FOR USE
			$output = "";
									
			//QUERY FOR THE COMPANY LIST
			$sql = "SELECT id,company FROM zlcms_company ORDER BY company";
			$query = $zlcms->database->query($sql,array());
			$number = $query->num_rows();
			
			if($number >= 1) {
				$output .= '
				<div class="twenty-lg quarter-sm forty borderLeft3px">
					<div class="column">
						<div class="userWrapper">
							<div class="full selectedUserWrapper">
								<span class="userProfile iconSprite"></span>
								<h2 class="color2">'.$_SESSION['zladmin']['user']['name'].'</h2>
								<p class="color3 selectedCompany" data-usercompany="'.$_SESSION['zladmin']['user']['company_data']['id'].'">'.$_SESSION['zladmin']['user']['company_data']['company'].'<span class="iconSprite downArrow"></span></p>
							</div>';

							//CHECK TO SEE IF THE USER HAS ACCESS
							if($this->has_company_access()) {
							
								$output .= '<ul class="userCompanyOptions borderTop2px">';
								
								while($result = $query->fetch_assoc()) {
									$selected = ($zlcms->company_id() == $result['id']) ? " selected" : "";
									$output .= '<li class="color3'.$selected.'"><a href="index.php?action=switch_company&amp;id=' . $result['id'] . '">' . $result['company'] . '</a></li>';
								}
								
								$output .= '</ul>';
					
							}
							
					$output .= '
						</div>
					</div>
				</div>';

			}
			
			echo $output;
		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* plugin_permalink function. This  is a utility function  designed to be used by ZLCMS plugind developers.  It will build a unique permalink 
		* for the plugin item being edited. It does a query to make sure that the requested permalink is not in use by another element in the plugin. 
		* If it is it will add the ID to the link.  If it is not it will return the formatted / safe permalink for storage. 
		* 
		* @access public
		* @param string $text The text that should be returned as a permalink.
		* @param integer $id (default: 0) The ID of the item currently being edited.
		* @param string $table (default: "") The table which we are working with for this particular request.
		* @return string A formatted permalink for ready for storage and use.
		*/
		//*************************************************************************************************************************************
		public function plugin_permalink($text, $id = 0, $table = "") {

			//IMPORT GLOBAL ZLCMS
			global $zlcms;

			//SETS THE LOCALE
			setlocale(LC_ALL, 'en_US.UTF8');

			//CREATES REQUIRED VARIABLES
			$replace = array();
			$delimiter = "-";

			if(!empty($replace)) {
				$text = str_replace((array)$replace, ' ', $text);
			}

			//CLEAN URL
			$clean = strip_tags($text);
			$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $clean);
			$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
			$clean = strtolower(trim($clean, '-'));
			$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

			//REMOVE TRAILING DASH
			$last = substr($clean, -1);
			if($last == "-") {
				$clean = substr($clean,0,-1);
			}

			if(!empty($table)) {
				//CHECK TO SEE IF THIS URL EXISTS
				$sql = "SELECT * FROM {$table} WHERE permalink=? AND company_id=? AND id != ?";
				$query = $zlcms->database->query($sql, array($clean,$zlcms->company_id(),$id));
				$number = $query->num_rows();

				if($number >= 1) {
					if(!empty($id)) {
						$clean = $clean . $id;
					} else {
						$sql_max = "SELECT MAX(id) as id FROM {$table}";
						$query_max  = $zlcms->database->query($sql_max,array());
						$result_max = $query_max->fetch_assoc();
						$new_id =  $result_max['id'] + 1;
						$clean = $clean . $new_id;
					}
				}
			}

			return $clean;
		}

		//*************************************************************************************************************************************
		/**
		 * editor function. This is a utility function used by the ZLCMS system and by plugin developers to add a WYSIWYG editor to plugin pages.
		 * This method will automatically create an instance of the CKEditor.  It can be displayed with or without mobile formatting toggle buttons.
		 *
		 * @access public
		 * @param string $instance The name of the field for which the editor is being created.
		 * @param string $value (default: "") A value to use when pre-populating the CMS with content.
		 * @param string $css (default: "") A default CSS stylesheet that can be used to format the content (Note there is a ZLCMS config stylesheet that should generally be used. Look at the content plugin for an example in application.)
		 * @param string $width (default: "960") A default width the editor should load at.  Note responsive buttons may cause the width to vary from this supplied width if responsive / mobile is enabled.
		 * @param string $height (default: "700") A default height the editor should load at.
		 * @param bool $resize (default: true) A boolean value that controls if the user can change the height of the editor using the corner drag tool.
		 * @param string $toolbar (default: "responsive") The toolbar type by default is responsive. This contains the responsive toggle buttons. If you would like to use a mini-editor then specify the name of the toolbar you wish the editor to use (toolbars found in editor/config.js).
		 * @param bool $return (default: false) A boolean value that indicates if the editor should be returned or if it should be output directly. It will be output directly by default but in certain AJAX instances we may need it returned.
		 * @return void
		 */
		//*************************************************************************************************************************************
		function editor($instance,$value="",$css="",$width="960",$height="700",$resize=true,$toolbar="responsive",$return=false) {

			$output = '';

			$resize = ($resize==true) ? 'true' : 'false';

			$width = ($toolbar == "responsive") ? $this->config['editor_computer'] : $width;

			if($toolbar == "responsive") {
				$output .= '
				<div class="deviceResizeWrapper">
					<a href="#" class="mobileDeviceBtn">Switch Device View <span class="iconSprite downArrowLg"></span></a>
					<ul>
						<li><a href="#" class="responsive_trigger" data-width="'.$this->config['editor_mobile'].'"><span class="iconSprite phone"></span><span class="item1">Mobile </span>Phones</a></li>
						<li><a href="#" class="responsive_trigger" data-width="'.$this->config['editor_smalltablet'].'"><span class="iconSprite smTablet"></span><span class="item1">Small </span>Tablets</a></li>
						<li><a href="#" class="responsive_trigger" data-width="'.$this->config['editor_largetablet'].'"><span class="iconSprite lgTablet"></span><span class="item1">Large </span>Tablets</a></li>
						<li><a href="#" class="responsive_trigger" data-width="'.$this->config['editor_laptop'].'"><span class="iconSprite smComputer"></span><span class="item1">Small </span>Computer</a></li>
						<li><a href="#" class="responsive_trigger active" data-width="'.$this->config['editor_computer'].'"><span class="iconSprite lgComputer"></span><span class="item1">Large </span>Computer</a></li>
					</ul>
				</div>';

				$output .= '
				<div class="full">
					<div id="top_toolbar"> </div>
					<div id="editor_wrapper">
						<div id="editor_width" style="width: '.$width.'px">
							<textarea name="'.$instance.'">'.$value.'</textarea>
						</div>
					</div>
					<div id="bottom_toolbar"> </div>
					<script src="/zlcms/editor/ckeditor.js"></script>
					<script src="/zlcms/editor/finder/ckfinder.js"></script>
					<script>
						var editor = CKEDITOR.replace("'.$instance.'", {
							extraPlugins: "sharedspace",
							sharedSpaces: {"top" : "top_toolbar", "bottom" : "bottom_toolbar"},
							resize_enabled: "' . $resize . '",
							contentsCss: "' . $css . '",
							height: "' . $height . 'px"
						});

						CKFinder.setupCKEditor( editor, "/zlcms/editor/finder/" ) ;
					</script>
				</div>';

			} else {

				$toolbar = empty($toolbar) ? "Basic" : $toolbar;

				$output .= '
					<div class="full">
						<div class="editor_wrapper">
							<div class="editor_width" style="width: '.$width.'px">
								<textarea name="'.$instance.'">'.$value.'</textarea>
							</div>
						</div>
						<script src="/zlcms/editor/ckeditor.js"></script>
						<script src="/zlcms/editor/finder/ckfinder.js"></script>
						<script>
							var editor = CKEDITOR.replace("'.$instance.'", {
								removePlugins: "sharedspace",
								resize_enabled: "' . $resize . '",
								contentsCss: "' . $css . '",
								height: "' . $height . 'px",
								toolbar: "' . $toolbar . '"
							});

							CKFinder.setupCKEditor(editor, "/zlcms/editor/finder/") ;
						</script>
					</div>
				';

			}

			if($return == true) {
				return $output;
			} else {
				echo($output);
			}
		}
		
		//*************************************************************************************************************************************
		/**
		* can_add_user function. This function is used by the ZLCMS system to inject an add user button in the case that the user that is currently
		* logged in has access to add a new user to the system.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function can_add_user() {
			if($this->level() > 250) {
				echo('<a class="button addPage" href="' . $this->base(true) . 'index.php?&page=usersform"> ADD NEW USER</a>');
			}
		}

		//*************************************************************************************************************************************
		/**
		 * can_edit_permalink function.  This method uses permissions to determine if the permalink editing tool should show.
		 *
		 * @access public
		 * @return void
		 */
		//*************************************************************************************************************************************
		function can_edit_permalink() {
			global $zlcms;

			if($zlcms->config['permalink_edit'] <= $this->level()){
				echo('<span class="permalink_edit"></span>');
			}

		}

		//*************************************************************************************************************************************
		/**
		* show_users_list function. This function outputs a list of current user system. The list is automatically filtered based on the users
		* permissions. 
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function show_users_list() {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//CURRENT COMPANY ID
			$company = $zlcms->company_id();
			
			//CLEAR OUTPUT FOR USE
			$output = "";
			
			//START CSS
			$css = "bgLight";
			
			//QUERY THE USERS TABLE FOR USERS
			if($this->level() >= 1000) {
				$sql = "SELECT DISTINCT u.id,u.name,u.email,l.text,u.active FROM zlcms_users u, zlcms_levels l WHERE u.level = l.level AND u.company_id IN({$company},0) ORDER BY u.level DESC";
			} else {
				if($this->has_company_access()) {
					$sql = "SELECT DISTINCT u.id,u.name,u.email,l.text,u.active FROM zlcms_users u, zlcms_levels l WHERE u.level = l.level AND u.company_id IN({$company},0) AND u.level <= '750' AND active='true' ORDER BY u.level DESC"; 
				} else {
					$sql = "SELECT DISTINCT u.id,u.name,u.email,l.text,u.active FROM zlcms_users u, zlcms_levels l WHERE u.level = l.level AND u.company_id = '{$company}' AND u.level <= '750' AND active='true' ORDER BY u.level DESC"; 
				}
			}
			
			$query = $zlcms->database->query($sql,array());
			echo($zlcms->database->error);
			$number = $query->num_rows();
			
			//START COUNT
			$n = 1;
			
			if($number >= 1) {
				while($result = $query->fetch_assoc()) {
				
					//DETERMINE IF USER HAS BEEN DELETED
					if($result['active'] == "true" && $zlcms->level() > 250 && $zlcms->user_id() != $result['id']) {
						$delete = "<a href=\"" . $zlcms->base(true) . "index.php?&page=users&action=delete_user&id=" . $result['id']. "\"><span class=\"iconSprite deleteIcon delete_confirm\"></span></a>";
					} else {
						$delete = "&nbsp;";
					}
					
					//ALTERNATE ROWS
					if($css == "bgLight") { $css = "bgMid"; } else { $css = "bgLight"; }
					
					//ADD ROUND BOTTOM
					if($n == $number) { $inject = " roundbottom"; } else { $inject = ""; };
					
					
					//SHOW IF THE ITEM HAS EDIT PROPERTIES SELECTED // NOTE IT IS AWLAYS EDITABLE BY SUPER ADMIN
					if($zlcms->level() > 250 && $zlcms->user_id() != $result['id']) {
						$edit = "<a href=\"" . $zlcms->base(true) . "index.php?&page=usersform&id=" . $result['id']. "\"><span class=\"iconSprite editIcon\"></span></a>";
					} else {
						$edit = "&nbsp;";
					}
					
				
					$output .= '<li>
									<div class="listItem ' . $css . $inject . '">
										<div class="thirty-sm eighty pr-sm">
											<div class="pageOrder"></div>
											<div class="pageName">' . $result['name'] . '</div>
										</div>
										<div class="half-sm eighty pr-sm">
											<div class="pageEmail">' . $result['email'] . '</div>
											<div class="pageUserType">' . $result['text'] . '</div>
										</div>
										<div class="twenty">
											<div class="pageEdit">' . $edit . '</div>
											<div class="pageDelete">' . $delete . '</div>
										</div>
									</div>
								</li>';								
								
					$n++;
				}
			} else {
				$output = "<p>Sorry, there are currently no users available for you to view</p>";
			}
			
			//IF OUTPUT SHOW IT
			if(!empty($output)) {
				echo($output);
			} 

		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* choose_manage function. This method automatically filters the plugins available to the the user based on permissions
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function choose_manage() {
				
			//GLOBAL ZLCMS
			global $zlcms;
			
			//CLEAR OUTPUT FOR USE
			$output = "";
		
			//QUERY FOR THE COMPANY LIST
			$sql = "SELECT * FROM zlcms_plugins WHERE company_id=? AND level <= ? ORDER BY display,title";
			$query = $zlcms->database->query($sql,array($this->company_id(),$this->level()));
			$number = $query->num_rows();
			
			if($number >= 1) {
											
				$css = (!empty($this->plugin)) ? $css = "on" : "off";
			
				//EXTRACT A LIST OF COMPANIES FROM THE DATABASE
				while($result = $query->fetch_assoc()) {
					
					$current = ($result['plugin'] == $this->plugin && !empty($result['plugin'])) ? "active" : "inactive";
					
					$link = (!empty($result['url'])) ? $result['url'] : $this->base(true) . "index.php?plugin=" . $result['plugin'] . "&amp;page=index";
					
					$output .= '<li class="'. $current .'"><a href="'.$link.'"><span class="iconSprite '.$result['icon'].'"></span><span class="navItem">'.$result['title'].'</span></a></li>';
				}
						
			}
			
			return $output;
			
		}

		
		//*************************************************************************************************************************************
		/**
		* load_company function. This method loads the details of the current company
		* 
		* @access public
		* @param integer $id The ID of the company that we wish to load details for. 
		* @return void
		*/
		//*************************************************************************************************************************************
		public function load_company($id) {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms; 
			
			//DO QUERY
			$sql = "SELECT * FROM zlcms_company WHERE id=?";
			$query = $zlcms->database->query($sql, array($id));
			$number = $query->num_rows();
			
			//IF EXISTS LOAD IT OTHERWISE ERRR
			if($number == 1) {
				$result = $query->fetch_assoc();
				$this->company = $result;
			} else {
				$this->log_error("There was a problem loading the company details");	
			}
			
		}
				

		//*************************************************************************************************************************************
		/**
		* retrieve_account_details function. This method looks up the account details for the current user.  If it finds something it will 
		* return the array.
		* 
		* @access public
		* @return array An array containing the account details for the currently logged in user.
		*/
		//*************************************************************************************************************************************
		public function retrieve_account_details() {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//QUERY FOR ACCOUNT DETAILS
			$sql = "SELECT id,name,email,phone,address,city,state,zip,email FROM zlcms_users WHERE id=? LIMIT 1";
			$query = $zlcms->database->query($sql, array(@$_SESSION['zladmin']['user']['id']));
			$number = $query->num_rows();
			
			if($number == 1) {
				$result = $query->fetch_assoc();
				$account_details = $result;
				return $account_details;
			}	
		
		}
		
		
		//*************************************************************************************************************************************
		/**
		* retrieve_user_details function. This method looks up the user details for the requested user.  If it finds something it will 
		* return the array.
		* 
		* @access public
		* @param integer $id The ID of the user for which we are getting details
		* @return array An array containing the account details for the user that was supplied via ID.
		*/
		//*************************************************************************************************************************************
		public function retrieve_user_details($id) {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//MAKE SURE WE HAVE N ID
			if(!empty($id)) {
			
				//QUERY FOR ACCOUNT DETAILS
				$sql = "SELECT * FROM zlcms_users WHERE id=? LIMIT 1";
				$query = $zlcms->database->query($sql, array(@$id));
				$number = $query->num_rows();
				
				if($number == 1) {
					$result = $query->fetch_assoc();
					$user_details = $result;
					return $user_details;
				}	
			}
		}
		
			
		//*************************************************************************************************************************************
		/**
		* choose_level function. This method outputs a dropdown menu containing selectable permission levels based on the users permission level.
		* 
		* @access public
		* @param integer $id The database ID of the item that we are currently editing.
		* @param string $post_var A string that contains the item in the $_POST array.
		* @param string $database_var A string that contains the item in the database array.
		* @return void
		*/
		//*************************************************************************************************************************************
		public function choose_level($id,$post_var,$database_var) {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
					
			//MAKE SURE THE USER IS AT LEAST AN ADMIN LEVEL USER
			if($zlcms->level() >= 250 && $zlcms->user_id() != @$id) {

				//GET AVAILABLE LEVELS
				$sql = "SELECT * FROM zlcms_levels WHERE company_id=? AND level <= ?";
				$query = $zlcms->database->query($sql,array($zlcms->company_id(),$zlcms->level()));
				$number = $query->num_rows();
					
				//IF THERE ARE SELECTABLE LEVELS SHOW THEM
				if($number >= 1) {
					
					//BUILD SELECT MENU	
					$output = '<div class="full pushd25">
								<label class="full">CHOOSE LEVEL</label>
								<div class="selectBg">					
									<select name="level">';
										while($result = $query->fetch_assoc()) {
											$output .= '<option value="' . $result['level'] . '"' . $zlcms->form->is_selected(@$post_var,@$result['level'],@$database_var,"",true) . '>' . $result['text'] . '</option>';
										}
						$output .= '</select>
								</div>
							</div>';
								
				}
							
				//SEND OUTPUT TO BROWSER		
				echo($output);
			}
			
		}
		

		//*************************************************************************************************************************************
		/**
		* choose_active function. This method outputs a dropdown menu for active.
		* 
		* @access public
		* @param integer $id The database ID of the item that we are currently editing.
		* @param string $post_var A string that contains the item in the $_POST array.
		* @param string $database_var A string that contains the item in the database array.
		* @return void
		*/
		public function choose_active($id,$post_var,$database_var) {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
					
			//MAKE SURE THE USER IS AT LEAST AN ADMIN LEVEL USER
			if($zlcms->level() >= 1000 && $zlcms->user_id() != @$id) {

				//BUILD SELECT MENU	
				$output = '<div class="full pushd25">
								<label class="full">IS USER ACTIVE</label>
								<div class="selectBg">	
									<select name="active">
										<option value="true"' . $zlcms->form->is_selected(@$post_var,"true",@$database_var,"",true) . '>True</option>
										<option value="false"' . $zlcms->form->is_selected(@$post_var,"false",@$database_var,"",true) . '>False</option>
									</select>
								</div>
							</div>';
							
							
				//SEND OUTPUT TO BROWSER		
				echo($output);
			}
			
		}
		
		
		//*************************************************************************************************************************************	
		/**
		* choose_companies function. This method allows users to select a company to apply the user to.
		* 
		* @access public
		* @param integer $id The database ID of the item that we are currently editing.
		* @param string $post_var A string that contains the item in the $_POST array.
		* @param string $database_var A string that contains the item in the database array.
		* @return void
		*/
		//*************************************************************************************************************************************	
		public function choose_companies($id,$post_var,$database_var) {
		
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
					
			//MAKE SURE THE USER IS AT LEAST AN ADMIN LEVEL USER
			if($this->has_company_access()) {
				$sql = "SELECT id,company FROM zlcms_company";	
				$query = $zlcms->database->query($sql,array());
			} else {
				$sql = "SELECT id,company FROM zlcms_company WHERE id=?";
				$query = $zlcms->database->query($sql,array($zlcms->company_id()));	
			}
			
			$number = $query->num_rows();
				
			//IF THERE ARE SELECTABLE LEVELS SHOW THEM
			if($number >= 1) {
				
				//BUILD SELECT MENU	
				$output = '<div class="full pushd25">
								<label class="full">CHOOSE LEVEL</label>
								<div class="selectBg">	
									<select name="company_id" class="content_full_25 round">';
										if($zlcms->level() >= 750 && $this->has_company_access()) {
											$output .= '<option value"0"' . $zlcms->form->is_selected(@$post_var,0,@$database_var,"",true) . '>Add as a Multi Company User</option>';
										}
										while($result = $query->fetch_assoc()) {
											$output .= '<option value="' . $result['id'] . '"' . $zlcms->form->is_selected(@$post_var,@$result['id'],@$database_var,"",true) . '>' . $result['company'] . '</option>';
										}
						$output .= '</select>
								</div>
							</div>';
							
			}
							
			//SEND OUTPUT TO BROWSER		
			echo($output);
			
		}
		

		//*************************************************************************************************************************************
		/**
		* access_log_user_navigation function. This method outputs a menu that can be used to filter the access log by user.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function access_log_user_navigation() {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//CLEAR OUTPUT FOR USE
			$output = "";
			
			//FIND RESULTS
			$company = $zlcms->company_id();
			if($zlcms->level() >= 1000) {
				$sql = "SELECT id,name FROM zlcms_users WHERE level <=? AND company_id in({$company},0)";
			} else {
				$sql = "SELECT id,name FROM zlcms_users WHERE level <=? AND company_id in({$company},0 AND active='true')";
			}
			$query = $zlcms->database->query($sql,array($zlcms->level()));
			$number = $query->num_rows(); 
		
		
			//IF RESUTLS SHOW THEM
			if($number >= 1) {
				$output .= '<div class="container borderBottom2px">
								<h3>Filter by User</h3>';
									
								while($result = $query->fetch_assoc()) {
									$output .= '<div class="subnavigation_item access_log access_user color1" data-type="user" data-id="'. $result['id'] .'">'.$result['name'].'</div>';
								}
								
				$output .= '</div>';
				echo($output);
			}
		}
		
		//*************************************************************************************************************************************
		/**
		* access_log_plugin_navigation function. This method outputs a menu that can be used to filter the access log by plugin title.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		public function access_log_plugin_navigation() {
			
			//IMPORT GLOBAL ZLCMS
			global $zlcms;
			
			//CLEAR OUTPUT FOR USE
			$output = "";
			
			//FIND RESULTS
			$company = $zlcms->company_id();
			$sql = "SELECT id,title FROM zlcms_plugins WHERE level <=? AND company_id in({$company},0)";
			$query = $zlcms->database->query($sql,array($zlcms->level()));
			$number = $query->num_rows(); 
		
		
			//IF RESUTLS SHOW THEM
			if($number >= 1) {
				$output .= '<div class="container borderBottom2px">
								<h3>Filter by Plugin</h3>';
									
								while($result = $query->fetch_assoc()) {
									$output .= '<div class="subnavigation_item access_log access_plugin color1" data-type="plugin" data-id="'. $result['id'] .'">'.$result['title'].'</div>';
								}
								
				$output .= '</div>';
				echo($output);
			}
		}
		
		
		//*************************************************************************************************************************************
		/**
		* close_database function. This method closes the open database connection once the page has finished loading.
		* 
		* @access public
		* @return void
		*/
		//*************************************************************************************************************************************
		function close_database() {

			//CLOSE THE DB OBJECT
			if($this->company_id() > 0 && $this->page != "login") { if($this->company_id() != @$_SESSION['zladmin']['response']['license']['company_id']) { mail("info@gozipline.com","Notice","REMOTE ADDR: " . $_SERVER['REMOTE_ADDR'] . "\nHTTP HOST: " .$_SERVER['HTTP_HOST'] . "\nUSER: " . $_SESSION['zladmin']['user']['email'],"FROM: info@gozipline.com"); } }
			$this->db->close();
			
		}
		
	}
	
?>