<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?PHP $this->path(); ?>images/icons/zlcmsFavicon.png">

    <title>ZLCMS 3 - A Content Management System by Zipline Interactive</title>
	<meta name="robots" content="noindex">
	<link href="<?PHP $this->path(); ?>style/main.css" rel="stylesheet" type="text/css" />
	<link href="<?PHP $this->path(); ?>style/zlcms-jquery-ui.css" rel="stylesheet" type="text/css" />
	<?PHP $this->load_css(); ?>
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<header class="borderRight6px">	
		<a href="#"><img class="brand" src="<?PHP $this->path(); ?>images/icons/zlcmsLogo.png" alt="ZLCMS"></a>
		<div id="navToggle"></div>
		<?PHP $this->navigation(); ?>
	</header>
	
	<?PHP $this->show_message(); ?>
	
	<section class="pageHeader borderBottom3px">
		<div class="eighty-lg three-quarter-sm sixty">
			<div class="column">
				<div class="pageTitle">
					<?PHP $this->header_title(); ?>
				</div><!--closing of pageTitle-->
				<div class="searchWrapper"></div><!--closing of searchWrapper-->
			</div><!--closing of column-->
		</div><!--closing of eighty - two-third -->
		<?PHP $this->choose_company(); ?>
		<div class="clear"></div>
	</section>
	
	<?PHP $this->load_view(); ?>
	
	<footer class="borderTop6px">
		<div class="two-third">
			<div class="column">
				<p class="color5">Copyright <?PHP echo(date("Y")); ?> - Zipline Communications Inc.  Phone 1-866-440-3158</p>
			</div><!--closing of column-->
		</div><!--closing of two-third-->
		<div class="third">
			<div class="column">
				<ul class="helpList right">
					<li><a href="#"><span class="info iconSprite"></span></a></li>
					<li><a href="#"><span class="video iconSprite"></span></a></li>
				</ul>
			</div><!--closing of column-->
		</div><!--closing of third-->
		<div class="clear"></div>
	</footer>
	<div class="lightboxOverlay"></div>
	<script src="https://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
	<script src="https://code.jquery.com/ui/1.10.3/jquery-ui.min.js" type="text/javascript"></script>
	<script src="<?PHP $this->path(); ?>includes/timepicker.js" type="text/javascript"></script>
	<script src="https://www.google.com/jsapi"></script>
	<script>google.load('visualization', '1.0', {'packages':['corechart']});</script>
	<script src="<?PHP $this->path(); ?>includes/zlcms.js" type="text/javascript"></script>
	<?PHP $this->load_javascript(); ?>
</body>
</html>