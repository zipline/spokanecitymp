<?PHP $user = $this->retrieve_user_details(@$_GET['id']); ?>

<section class="mainContentWrapper">
	<div class="full column borderBottom3px">
		<div class="mainPageTitle">
			<div class="full">
				<h1 class="push40">MANAGE USER</h1>
			</div><!--closing of full-->
		</div><!--closing of mainPageTitle-->
	</div><!--closing of full-->
	
	<div class="full p25 bg3">

		<form action="<?PHP $this->get_form_action(); ?>" method="post">
		
			<div class="full">
				<div class="half-sm full pr-sm">
					
					<label class="full">NAME *</label>
					<input class="full" type="text" name="name" value="<?PHP $this->form->toggle_value(@$user['name'],@$_POST['name']); ?>" >
			
				</div><!--closing of half-->
				<div class="half-sm full">

					<label class="full">EMAIL *</label>
					<input class="full" type="text" name="email" value="<?PHP $this->form->toggle_value(@$user['email'],@$_POST['email']); ?>">
				
				</div><!--closing of half-->
			</div><!--closing of full-->
			<div class="full">
				<div class="half-sm full pr-sm">
					
					<label class="full">PASSWORD *</label>
					<input class="full" type="password" name="password" value="" >
			
				</div><!--closing of half-->
				<div class="half-sm full">

					<label class="full">CONFIRM PASSWORD *</label>
					<input class="full" type="password" name="confirm_password" value="" >
				
				</div><!--closing of half-->
			</div><!--closing of full-->
			<div class="full">
				<div class="third-sm full pr-sm">
					<?PHP $this->choose_level(@$_GET['id'],@$_POST['level'],@$user['level']); ?>
				</div><!--closing of half-->
				<div class="third-sm full pr-sm">
					<?PHP $this->choose_active(@$_GET['id'],@$_POST['active'],@$user['active']); ?>	
				</div><!--closing of half-->
				<div class="third-sm full">
					<?PHP $this->choose_companies(@$_GET['id'],@$_POST['company'],@$user['company']); ?>						
				</div><!--closing of half-->
			</div><!--closing of full-->
		
			<div class="full">
				<div class="quarter-sm full pr-sm">
					<label class="full">ADDRESS</label>
					<input class="full" type="text" name="address" value="<?PHP $this->form->toggle_value(@$user['address'],@$_POST['address']); ?>">
				</div><!--closing of half-->
				
				<div class="quarter-sm full pr-sm">
					<label class="full">CITY</label>
					<input class="full" type="text" name="city" value="<?PHP $this->form->toggle_value(@$user['city'],@$_POST['city']); ?>">
				</div><!--closing of half-->				
				
				<div class="quarter-sm full pr-sm">
					<label class="full">STATE</label>
					<input class="full" type="text" name="state" value="<?PHP $this->form->toggle_value(@$user['state'],@$_POST['state']); ?>">
				</div><!--closing of half-->
				
				<div class="quarter-sm full">
					<label class="full">ZIP CODE</label>
					<input class="full" type="text" name="zip" value="<?PHP $this->form->toggle_value(@$user['zip'],@$_POST['zip']); ?>">
				</div><!--closing of half-->				
				
			</div><!--closing of full-->

			<div class="full">
				<div class="half-sm full pr-sm">
					<label class="full">PHONE</label>
					<input class="full" type="text" name="phone" value="<?PHP $this->form->toggle_value(@$user['phone'],@$_POST['phone']); ?>">
				</div><!--closing of half-->
								
			</div><!--closing of full-->

			<div class="full">
				<?PHP $this->is_update(@$_GET['id']); ?>
				<input type="hidden" name="action" value="update_users" />
				<input type="submit" name="submit" value="SAVE" class="button" />
			</div>
		</form>

	</div><!--closing of full-->
</section>



<section class="lightboxWrapper" id="infoLightbox">
	<div class="full borderBottom3px">
		<h2>Instructions</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full">
		<p>This form allows you to add/edit system users. When creating users please note administrative users have access to all functions including the ability to add additional users.  Users are a standard type allowing the administrator to restrict action to functions, adding users, and editing select pages from this user type.</p>  
		<p>Note: Your site may contain additional user level options.  Please check your documentation for more information.</p>
	</div><!--closing of full-->
</section>
