<?PHP $account = $this->retrieve_account_details(); ?>


<section class="mainContentWrapper">
	<div class="full column borderBottom3px">
		<div class="mainPageTitle">
			<div class="full">
				<h1>MANAGE YOUR ACCOUNT</h1>
			</div><!--closing of full-->
		</div><!--closing of mainPageTitle-->
	</div><!--closing of full-->
	
	<div class="full p25 bg3">


		<form action="<?PHP $this->get_form_action(); ?>" method="post">
		
			<div class="full">
				<div class="half-sm full pr-sm">
					
					<label class="full">NAME *</label>
					<input class="full" type="text" name="name" value="<?PHP $this->form->toggle_value(@$account['name'],@$_POST['name']); ?>">
				</div><!--closing of half-->
				<div class="half-sm full">

					<label class="full">EMAIL *</label>
					<input class="full" type="text" name="email" value="<?PHP $this->form->toggle_value(@$account['email'],@$_POST['email']); ?>" >
				</div><!--closing of half-->
			</div><!--closing of full-->

			<div class="full">
				<div class="half-sm full pr-sm">
					
					<label class="full">PASSWORD</label>
					<input class="full" type="password" name="password" value="">
				</div><!--closing of half-->
				<div class="half-sm full">

					<label class="full">CONFIRM PASSWORD</label>
					<input class="full" type="password" name="confirm_password" value="">
				</div><!--closing of half-->
			</div><!--closing of full-->

			<div class="full">
				<div class="half-sm full pr-sm">
					
					<label class="full">ADDRESS</label>
					<input class="full" type="text" name="address" value="<?PHP $this->form->toggle_value(@$account['address'],@$_POST['address']); ?>">
				</div><!--closing of half-->
				<div class="half-sm full">

					<label class="full">CITY</label>
					<input class="full" type="text" name="city" value="<?PHP $this->form->toggle_value(@$account['city'],@$_POST['city']); ?>">
				</div><!--closing of half-->
			</div><!--closing of full-->

			<div class="full">
				<div class="third-sm full pr-sm">
					
					<label class="full">STATE</label>
					<input class="full" type="text" name="state" value="<?PHP $this->form->toggle_value(@$account['state'],@$_POST['state']); ?>">
				</div><!--closing of half-->
				<div class="third-sm full pr-sm">

					<label class="full">ZIP CODE</label>
					<input class="full" type="text" name="zip" value="<?PHP $this->form->toggle_value(@$account['zip'],@$_POST['zip']); ?>">
				</div><!--closing of half-->
				<div class="third-sm full">

					<label class="full">PHONE</label>
					<input class="full" type="text" name="phone" value="<?PHP $this->form->toggle_value(@$account['phone'],@$_POST['phone']); ?>">
				</div><!--closing of half-->
			</div><!--closing of full-->

			<div class="full">
				<input type="hidden" name="action" value="update_account" /><input type="submit" name="submit" value="SAVE ACCOUNT DETAILS" class="button" />
			</div><!--closing of full-->

		</form>

	</div><!--closing of full-->
</section>


<section class="lightboxWrapper" id="infoLightbox">
	<div class="full borderBottom3px">
		<h2>Instructions</h2>
		<div class="closeLightbox">x</div>
	</div>
	<div class="full">
		<p>The account form allows you to manage your personal information, email address, and password.  This data is used through-out the system to identify and notify you.  Please make sure all of this information is accurate.</p>
		<p>(Note: That password you use for the ZLCMS system is encrypted using a one way hash encryption system and is not visible to anyone.  Please feel free to use a password that is secure and easy to remember)</p> 
	</div><!--closing of full-->
</section>
