		<div id="login" class="round">
			<div id="login_loading">
				<div id="login_loading_image"><img src="templates/images/login_loading.gif" alt="Login Loading" title="Login Loading" /></div>
				<div id="login_loading_text">Logging In..</div>
			</div>
			<div id="login_error" class="round">
				<div id="login_image"><img src="templates/images/login_error.gif" title="Login Error" alt="Login Error" /></div>
				<div id="login_text">Error</div>
			</div>
			<div class="login_row"><input type="text" name="login_email" id="login_email" class="login_field round" /></div>
			<div class="login_row"><input type="password" name="login_password" id="login_password" class="password_field round" /></div>
			<div class="login_row">
				<div id="login_button"><input type="submit" name="submit" value="LOGIN" class="submit_button round" id="login_submit" /></div>
				<div id="login_password_link"><p>Forgot Password?</p></div>
			</div>
			<div id="password" class="round">
				<div class="login_row"><input type="text" name="password_email" id="password_email" class="forgot_password_field round" /></div>
				<div class="login_row"><input type="submit" name="submit" value="SEND PASSWORD" class="submit_button round" id="password_submit" /></div>
			</div>
		</div>
