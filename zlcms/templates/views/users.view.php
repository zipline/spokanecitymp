<section class="mainContentWrapper">
	<div class="full column borderBottom3px">
		<div class="mainPageTitle">
			<div class="two-third-med half pr">
				<h1 class="push40">USERS</h1>
			</div><!--closing of full-->
			<div class="third-med half">
					
				<?PHP $this->can_add_user(); ?>						
				
			</div><!--closing of forty - full-->
		</div><!--closing of mainPageTitle-->
	</div><!--closing of full-->
	
	<div class="full p25 bg3">

		<ul class="pageList">
			<li>
				<div class="listItem bgDark">
					<div class="thirty pr">
						<div class="pageOrder"></div>
						<div class="pageName">User</div>
					</div>
					<div class="half pr">
						<div class="pageEmail">Email</div>
						<div class="pageUserType">Type</div>
					</div>
					<div class="twenty">
						<div class="pageEdit"><a href="#"><span class="iconSprite editIcon"></span></a></div>					
						<div class="pageDelete"><a href="#"><span class="iconSprite deleteIcon delete_confirm"></span></a></div>
					</div>
				</div>			

				<ul id="users_container">
					<?PHP $this->show_users_list(); ?>
		
				</ul>



			</li>

		</ul>
	

	</div><!--closing of full-->
</section>