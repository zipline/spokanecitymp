<section class="mainContentWrapper">
	<div class="full column borderBottom3px">
		<div class="mainPageTitle">
			<div class="full">
				<h1>ACCESS LOG</h1>
			</div><!--closing of full-->
		</div><!--closing of mainPageTitle-->
	</div><!--closing of full-->
	
	<div class="eighty-lg two-third-sm full">	
		<div class="full p25 bg1">
			<div id="access_log_wrapper">
				<?PHP $this->load_access_log("all"); ?>
			</div>
						
		</div><!--closing of full-->	
	</div><!--closing of eighty-lg-->

	<div class="twenty-lg third-sm full borderLeft3px bg2">
		<div class="rightColumn">			
			<?PHP $this->access_log_user_navigation(); ?>
			<?PHP $this->access_log_plugin_navigation(); ?>
			<div class="container">
				<div class="subnavigation_item access_log subnavigation_on color6" data-type="all" data-id="">Show All</div>
			</div>				
		</div><!--closing of rightColumn-->
	</div><!--closing of twenty-lg-->

</section>