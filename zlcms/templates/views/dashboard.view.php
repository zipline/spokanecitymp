<?PHP global $zlcms; ?>



<?PHP 
/*
	//THE FOLLOWING DISPLAYS A THREE COLUMN CONTENT SECTION
		- LEFT COLUMN:  MAIN NAVIGATION
		- CENTER COLUMN: DASHBOARD MAIN CONTENT
		- RIGHT COLUMN: "AT A GLANCE" CONTENT
*/
?>


<script> var charting = new Array(); </script>
<?PHP if(is_array(@$_SESSION['zladmin']['response']['statistics']['visits'])) { ?>
	<?PHP foreach(array_reverse($_SESSION['zladmin']['response']['statistics']['visits']) as $item) { ?>
	 	<script>charting.push(['<?PHP echo(date('M j',strtotime($item['date']))); ?>',<?PHP echo($item['visits']); ?>]);</script>
	<?PHP } ?>
<?PHP } ?>
<section class="mainContentWrapper">

	<div class="eighty-lg three-quarter-sm full">			
		<div class="analyticsWrapper borderBottom2px">
			<div class="chartHeader">
				<h2>Total Visits last 30 days
			</div><!--closing of chartHeader-->
			<div id="chart_div"></div>
		</div><!--closing of analytics-->		

		<?PHP if(is_array(@$_SESSION['zladmin']['response']['statistics']['goals'])) { ?>
		<div class="full">
			<div class="column">
				<div class="goalsWrapper">
					<h4>Goals:</h4>			
					<div class="goalsItem bgDark roundCorners3 hidden-med">
						<div class="quarter-med pr-med full">
							<p class="color7">Name</p>
						</div>
						<div class="quarter-med pr-med full">								
							<p class="color7">Starts</p>
						</div>
						<div class="quarter-med pr-med full">									
							<p class="color7">Complete</p>
						</div>
						<div class="quarter-med full">
							<p class="color7">Value</p>
						</div><!--closing of forty-->

					</div><!--closing of goalsItem-->

					<?PHP $bg = 'bgMid'; ?>
					<?PHP foreach($_SESSION['zladmin']['response']['statistics']['goals'] as $item) { ?>	
						<?PHP $bg = ($bg=='bgMid') ? 'bgLight' : 'bgMid'; ?>		

						<div class="goalsItem <?PHP echo($bg); ?> roundCorners3">
							<div class="quarter-med pr-med full">
								<h6 class="visible-med color2">Name: </h6>
								<p class="color5"><?PHP echo($item['name']); ?></p>
							</div>
							<div class="quarter-med pr-med full">
								<h6 class="visible-med color2">Starts: </h6>
								<p class="color5"><?PHP echo($item['stats'][1]['starts']); ?></p>
							</div>
							<div class="quarter-med pr-med full">
								<h6 class="visible-med color2">Complete: </h6>
								<p class="color5"><?PHP echo($item['stats'][1]['completions']); ?></p>
							</div>
							<div class="quarter-med full">
								<h6 class="visible-med color2">Value: </h6>								
								<p class="color5">$<?PHP echo(money_format('%(#10n', $item['stats'][1]['value'])); ?></p>
							</div><!--closing of forty-->

						</div><!--closing of goalsItem-->
					<?PHP } ?>
			</div>
			</div>
		</div>		
		<?PHP } ?>

		<?PHP if(is_array(@$_SESSION['zladmin']['response']['statistics']['goalsbysource'])) { ?>
			<div class="full">
				<div class="column">
					<div class="goalsWrapper">
						<h4>Goal Sources:</h4>							
						<div class="goalsItem bgDark roundCorners3 hidden-med">
							<div class="twenty pr">
								<p class="color7">Goal Source</p>
							</div>
							<div class="twenty pr">								
								<p class="color7">Medium</p>
							</div>
							<div class="twenty pr">									
								<p class="color7">Goal Starts</p>
							</div>
							<div class="twenty pr">
								<p class="color7">Goals Completed</p>
							</div>
							<div class="twenty">								
								<p class="color7">Total Value</p>
							</div>
	
						</div><!--closing of goalsItem-->
											
						<?PHP $bg = 'bgMid'; ?>
						<?PHP foreach($_SESSION['zladmin']['response']['statistics']['goalsbysource'] as $item) { ?>	
							<?PHP $bg = ($bg=='bgMid') ? 'bgLight' : 'bgMid'; ?>		
							<div class="goalsItem <?PHP echo($bg); ?> roundCorners3">
								<div class="twenty-med pr-med full">
									<h6 class="visible-med color2">Goal Source: </h6>
									<p class="color5"><?PHP echo($item['source']); ?></p>
								</div>
								<div class="twenty-med pr-med full">
									<h6 class="visible-med color2">Medium: </h6>
									<p class="color5"><?PHP echo($item['medium']); ?></p>
								</div>
								<div class="twenty-med pr-med full">
									<h6 class="visible-med color2">Goal Starts: </h6>
									<p class="color5"><?PHP echo($item['goalstartsall']); ?></p>
								</div>							
								<div class="twenty-med pr-med full">
									<h6 class="visible-med color2">Goals Completed: </h6>
									<p class="color5"><?PHP echo($item['goalcompleationsall']); ?></p>
								</div>
								<div class="twenty-med full">
									<h6 class="visible-med color2">Total Value: </h6>
									<p class="color5">$<?PHP echo(money_format('%(#10n', $item['goalvalueall'])); ?></p>
								</div>
	
							</div><!--closing of goalsItem-->
						<?PHP } ?>
					</div>
				</div>
			</div>		
		<?PHP  } ?>
		
		<div class="half-med full">
			<div class="column">

				<div class="analyticsData">
					<h4>Top Pages</h4>
					<ol>
						<?PHP if(is_array(@$_SESSION['zladmin']['response']['statistics']['toppages'])) { ?>
							<?PHP $bg = 'bg0'; ?>
							<?PHP foreach($_SESSION['zladmin']['response']['statistics']['toppages'] as $item) { ?>	
								<?PHP $bg = ($bg=='bg0') ? 'bg2' : 'bg0'; ?>			
								<li class="<?PHP echo($bg); ?> roundCorners3"><a href="<?PHP echo(rtrim($this->config['base_path'],"/") . $item['url']); ?>" target="_blank"><?PHP echo(substr($item['title'],0,50)); ?></a></li>
							<?PHP } ?>
						<?PHP } else { ?>
								<li class="bg2">There are currently no top pages to show</li>
						<?PHP  } ?>
					</ol>
				</div><!--closing of analyticsData-->
			</div><!--closing of column-->
		</div><!--closing of half-->

		<div class="half-med full">
			<div class="column">
				<div class="analyticsData">
					<h4>Top Landing Pages</h4>
<?PHP //NOTE:  Please set the character limit on the li to 40 ?>				
					<ol>
						<?PHP if(is_array(@$_SESSION['zladmin']['response']['statistics']['toplandingpages'])) { ?>
							<?PHP $bg = 'bg0'; ?>
							<?PHP foreach($_SESSION['zladmin']['response']['statistics']['toplandingpages'] as $item) { ?>	
								<?PHP $bg = ($bg=='bg0') ? 'bg2' : 'bg0'; ?>			
								<li class="<?PHP echo($bg); ?> roundCorners3"><a href="<?PHP echo(rtrim($this->config['base_path'],"/") . $item['url']); ?>" target="_blank"><?PHP echo(substr($item['title'],0,50)); ?></a></li>
							<?PHP } ?>
						<?PHP } else { ?>
								<li class="bg2">There are currently no top landing pages to show</li>
						<?PHP  } ?>
					</ol>
				</div><!--closing of analyticsData-->
			</div><!--closing of column-->
		</div><!--closing of half-->

		<div class="full">
			<div class="column">
				<div class="dashboardNews">
					<h4>What&#39;s New at Zipline</h4>
					<ul>
						
	
						<?PHP if(is_array(@$_SESSION['zladmin']['response']['news'])) { ?>
							<?PHP $bg = 'bg0'; ?>
							<?PHP foreach($_SESSION['zladmin']['response']['news'] as $item) { ?>
								<?PHP $bg = ($bg=='bg0') ? 'bg2' : 'bg0'; ?>		
								<li class="<?PHP echo($bg); ?> roundCorners3">
									<a href="<?PHP echo($item['link']); ?>" target="_blank">
										<div class="full">
											<img class="border1" src="<?PHP echo($item['image']); ?>" alt="<?PHP echo($item['title']); ?>" width="80">
											<span class="title color6"><?PHP echo($item['title']); ?></span>
											<span class="description color5"><?PHP echo(substr($item['description'],0,150)); ?>...</span>
										</div>
									</a>
								</li>
							<?PHP } ?>
						<?PHP } else { ?>
								<li class="bg2">There is currently no news to show</li>
						<?PHP  } ?>

					</ul>
				</div><!--closing of analyticsData-->
			</div><!--closing of column-->
		</div><!--closing of half-->

	</div><!--closing of two-third-->



	<?PHP 
	/*
		//SECTION CONTAINS:  
			-- RIGHT COLUMN
	*/
	?>
	<div class="twenty-lg quarter-sm full borderLeft3px bg2">
		<div class="rightColumn">
			<div class="container borderBottom2px">
				<h3>Last 30 days:</h3>
				<ul>
					<li class="bg1 roundCorners3 color2"><span class="iconSprite people"></span><?PHP echo(!empty($_SESSION['zladmin']['response']['statistics']['snapshot'][1]['visits']) ? $_SESSION['zladmin']['response']['statistics']['snapshot'][1]['visits'] : 0); ?> Unique Visitors</li>
					<li class="color2"><span class="iconSprite profilePaper"></span><?PHP echo(!empty($_SESSION['zladmin']['response']['statistics']['snapshot'][1]['pageviews']) ? $_SESSION['zladmin']['response']['statistics']['snapshot'][1]['pageviews'] : 0); ?> Page Views</li>
					<li class="bg1 roundCorners3 color2"><span class="iconSprite foldedPaper"></span><?PHP $pages = (!empty($_SESSION['zladmin']['response']['statistics']['snapshot'][1]['visits']) && !empty($_SESSION['zladmin']['response']['statistics']['snapshot'][1]['pageviews'])) ? $_SESSION['zladmin']['response']['statistics']['snapshot'][1]['pageviews']/$_SESSION['zladmin']['response']['statistics']['snapshot'][1]['visits'] : 0; echo(round($pages,2)); ?> Pages / Visit</li>
					<li class="color2"><span class="iconSprite clock"></span><?PHP $time = (!empty($_SESSION['zladmin']['response']['statistics']['snapshot'][1]['session_duration'])) ? $_SESSION['zladmin']['response']['statistics']['snapshot'][1]['session_duration'] : 0; echo(gmdate('H:i:s',$time)); ?> Avg. Visit Duration</li>
					<li class="bg1 roundCorners3 color2"><span class="iconSprite bounceRate"></span><?PHP $bounce = (!empty($_SESSION['zladmin']['response']['statistics']['snapshot'][1]['bounce_rate'])) ? $_SESSION['zladmin']['response']['statistics']['snapshot'][1]['bounce_rate'] : 0; echo(round($bounce)); ?>% Bounce Rate</li>
					<li class="color2"><span class="iconSprite secsToLoad"></span><?PHP $time = (!empty($_SESSION['zladmin']['response']['statistics']['snapshot'][1]['page_load_time'])) ? $_SESSION['zladmin']['response']['statistics']['snapshot'][1]['page_load_time'] : 0; echo(round($time)); ?> Seconds to Load</li>
				</ul>
			</div><!--closing of container-->
			

		
			<div class="container borderBottom2px">
				<h3>Devices:</h3>
				
				<?PHP //PIE CHART HERE ?>
				
				<ul>

					<?PHP if(is_array(@$_SESSION['zladmin']['response']['statistics']['devices'])) { ?>
						<?PHP $bg = 'bg0'; ?>
						<?PHP $total = 0; ?>
						<?PHP foreach($_SESSION['zladmin']['response']['statistics']['devices'] as $item) { $total += $item['visits']; } ?>
						<?PHP foreach($_SESSION['zladmin']['response']['statistics']['devices'] as $item) { ?>	
							<?PHP $bg = ($bg=='bg0') ? 'bg1' : 'bg0'; ?>	
							<?PHP $percentage = round(($item['visits'] / $total) * 100,0); ?>	
							<li class="<?PHP echo($bg); ?> roundCorners3"><span class="iconSprite mini<?PHP echo(ucwords($item['device'])); ?>"></span><?PHP echo($percentage); ?>% Users on <?PHP echo(ucwords($item['device'])); ?></li>
						<?PHP } ?>

					<?PHP } else { ?>
							<li class="bg2">There is currently no device information to show</li>
					<?PHP  } ?>	
				</ul>
			</div><!--closing of container-->
		
			<div class="container">
				<h3>Top Sources</h3>
				<ul>
					<?PHP if(is_array(@$_SESSION['zladmin']['response']['statistics']['topreferringsites'])) { ?>
						<?PHP $bg = 'bg0'; ?>
						<?PHP foreach($_SESSION['zladmin']['response']['statistics']['topreferringsites'] as $item) { ?>	
							<?PHP $bg = ($bg=='bg0') ? 'bg1' : 'bg0'; ?>			
							<li class="<?PHP echo($bg); ?> roundCorners3"><a class="color2" href="http://<?PHP echo($item['source']); ?>" target="_blank"><?PHP echo(substr($item['source'],0,20)); ?>... <?PHP echo($item['pageviews']); ?></a></li>
						<?PHP } ?>
					<?PHP } else { ?>
							<li class="bg2">There are currently no keywords to show</li>
					<?PHP  } ?>					
				</ul>
			</div><!--closing of container-->

			<div class="container">
				<h3>Top Keywords</h3>
				<ul>
					<?PHP if(is_array(@$_SESSION['zladmin']['response']['statistics']['keywords'])) { ?>
						<?PHP $bg = 'bg0'; ?>
						<?PHP foreach($_SESSION['zladmin']['response']['statistics']['keywords'] as $item) { ?>	
							<?PHP $bg = ($bg=='bg0') ? 'bg1' : 'bg0'; ?>			
							<li class="<?PHP echo($bg); ?> roundCorners3"><?PHP echo(substr($item['source'],0,20)); ?>... <?PHP echo($item['pageviews']); ?></li>
						<?PHP } ?>
					<?PHP } else { ?>
							<li class="bg2">There are currently no keywords to show</li>
					<?PHP  } ?>					
				</ul>
			</div><!--closing of container-->
		
		</div><!--closing of rightColumn-->
	</div><!--closing of twenty-->

</section>
