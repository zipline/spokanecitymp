//**********************************************************************************************************************
/**
* DOCUMENT: /zlcms/templates/includes/login.js
* DEVELOPED BY: Ryan Stemkoski
* COMPANY: Zipline Interactive
* EMAIL: ryan@ziplineinteractive.com
* PHONE: 509-321-2849
* DATE: 6/5/2014
* DESCRIPTION: This document contains programming required for the CMS login.  Requires the jQuery library.
*/
//***********************************************************************************************************************
$(function() {
    
   	var submit = false;
   	var login = true;
    
  	//*******************************************************************************************************************
	//SUBMIT LOGIN FORM
	//*******************************************************************************************************************
  	$('#loginForm').submit(function(e) {
  		e.preventDefault();
  		
  		if(submit == false) {
  			
  			submit = true;
  			$('.message').hide();
  			$('.message').removeClass("error").removeClass("success");
  			
  			//HANDLE LOGIN
  			if(login == true) {
	  			
	  			var email = $('#email').val();
	  			var password = $('#password').val();
	  			$('#login').hide();
	  			$('#loggingIn').show();
	  			
	  			$.post("index.php", { action: "login", email: email, password: password, method: "ajax" },
					function(data){
						if(data == "success") {
							submit = false;
							window.location = "index.php?page=dashboard";
						} else {
		  					submit = false;
		  					$('#login').show();
		  					$('#loggingIn').hide();
		  					$('.message').addClass("error");
		  					$('.message').html("<p>Invalid email or password</p>").slideDown("normal");
						}
					}
				);	
				
			//HANLDE FORGOT PASSWORD		
  			} else {
  			
  				var email = $('#email').val();
				$.post("index.php", { action: "forgot_password", email: email },
					function(data){
					
						console.log(data);
						
						if(data == "success") {
							$('#login_loading').hide();
		  					$('.message').addClass("success");
		  					$('.message').html("<p>An email has been set to the supplied address with a new password.</p>").slideDown("normal");							
							submit = false;
							
							//RESET LOGIN FOR USE WITH NEW PASSWORD
							login = true;
							$('#password').show();
							$('#login').val("Login");
							$('#forgot_password').text("Forgot Password");
						} else {
							$('#login_loading').hide();
		  					$('.message').addClass("error");
		  					$('.message').html("<p>The email address your supplied is not connected to a ZLCMS account.</p>").slideDown("normal");
		  					submit = false;
						}
					}
				);
	
  			}
  		}

  	});
  	
  	
   	//*******************************************************************************************************************
	//SUBMIT LOGIN FORM
	//*******************************************************************************************************************  		
	$('#forgot_password').click(function(e) {
	
		e.preventDefault();
	
		if(login == true) {
			login = false;  		
			$('#password').hide();
			$('#login').val("Get Password");
			$('#forgot_password').text("Login");
			
		} else {
			login = true;
			$('#password').show();
			$('#login').val("Login");
			$('#forgot_password').text("Forgot Password");
		}
	
	});
	
   	//*******************************************************************************************************************
	//SUBMIT LOGIN FORM
	//*******************************************************************************************************************  		
	function position_login() {
	
		var login_height = $('.loginWrapper').outerHeight();
		var window_height = $(window).height();
		
		console.log(window_height);
		console.log(login_height);
		
		var top_position = 0;
		
		if(login_height < window_height) {
			top_position = (window_height - login_height) / 2;
		}
		
		console.log(top_position);
		
		$('.loginWrapper').css("top",top_position + "px");
		
	
	}
	
   	//*******************************************************************************************************************
	//IF THE WINDOW SIZE CHANGES THEN MOVE THE LOGIN BOX
	//*******************************************************************************************************************  		
	$(window).resize(function() {
		position_login();
	});
	
	
	position_login();
  
});
