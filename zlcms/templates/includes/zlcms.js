//**********************************************************************************************************************
/**
 * DOCUMENT: /zlcms/includes/main.js
 * DEVELOPED BY: Ryan Stemkoski
 * COMPANY: Zipline Interactive
 * EMAIL: ryan@gozipline.com
 * PHONE: 509-321-2849
 * DATE: 4/28/2010
 * DESCRIPTION: This document contains programming required for the CMS login.  Requires the jQuery library.
 */
//***********************************************************************************************************************

//***********************************************************************************************************************
//GLOBAL VARIABLES USED IN CONJUNCTION WITH THE PLUGIN MOVE FUNCTIONALITY (DRAG AND DROP SORTING)
//***********************************************************************************************************************
var plugin, plugin_move_url, plugin_move_action;
var is_sortable = true;

//***********************************************************************************************************************
//CUSTOM ZLCMS ALERT
//***********************************************************************************************************************
function zlcms_alert(title,text) {
	alert(title + ' ' + text);
}

//***********************************************************************************************************************
//SETS RECURSIVE TIMEOUT TO KEEP THE USERS SESSION ALIVE
//***********************************************************************************************************************
function stay_alive() {
	$.post("index.php", { action: "stay_alive" }, function(data) {

		if(data == "success") {
			stayalive = setTimeout(stay_alive,45000);
		} else {
			window.location = "index.php";
		}
	});

}

//***********************************************************************************************************************
//THIS FUNCTION CREATES SORTABLE NAVIGATION ELEMENTS
//***********************************************************************************************************************
function plugin_sort(move_object) {
	//CHECK TO SEE IF THERE IS AN ACTIVE SORTABLE
	if(is_sortable) {

		var thisObj = move_object.length > 0 ? move_object : $(this);

		//GET THE PARENT AND CURRENT ELEMENTS TO THE CLICKED ITEM
		var parent = thisObj.parent('div').parent('div').parent('div').parent('li').parent('ul');
		var current = thisObj.parent('div').parent('div').parent('div').parent('li');

		//MAKE TRANSPARENT
		current.animate({ opacity: 0.60 }, 100 );

		//PREVENT ANOTHER SORTABLE
		is_sortable = false;

		//CREATE SORTABLE
		parent.sortable({
			axis: 'y',
			update: function() {
				var parent_id = parent.data("parent");
				var list = parent.sortable('serialize');

				$.post(plugin_move_url, { action: plugin_move_action, parent: parent_id, items: list });
				//parent.sortable('destroy');
				is_sortable = true;
			},
			stop: function() {
				current.animate({ opacity: 1.0 }, 100 );
				parent.sortable('destroy');
				is_sortable = true;
			}

		}).disableSelection();

		//IF THERE IS AN EXISTING SORTABLE SHOW ERROR
	} else {
		zlcms_alert("Sorting Error","You currently have an instance of sorting initiated.  You may only sort one item at a time. Please complete the previous sort and try again.")
	}
}

/********************************************************************************************************************
 LOAD ACCESS LOG
 ********************************************************************************************************************/
function load_access_log(type_in,id_in) {
	$.post("index.php?page=accesslog", { type: type_in, id: id_in, action: "load_access_log" },
		function(data){
			if(data) {
				$('#access_log_wrapper').html(data);
			}
		}
	);
}


/********************************************************************************************************************
 ATTEMPT TO HIDE INSTRUCTION BUTTONS IF ITEMS ARE MISSING
 ********************************************************************************************************************/
function toggle_instructions() {

	if($('#videoHolder').length) {
		if($('#videoHolder').html().length <= 0) {
			$('.video').parents('li').hide();
		}
	}

	if($('#instructionsHolder').length) {
		if($('#instructionsHolder').html().length <= 0) {
			$('.info').parents('li').hide();
		}
	}

}

//***********************************************************************************************************************
//THIS FUNCTION USES AJAX TO CREATE A PERMALINK (FOR USE IN CUSTOM PLUGINS)
//***********************************************************************************************************************
function plugin_update_permalink(title, type, table) {
	//IF THERE IS NO PERMALINK OR WE ARE UPDATING IT RUN FUNCTION.  IF THERE IS A PERMALINK SET DON'T TRY TO SET IT
	if(title != "") {
		$.post("index.php?plugin=" + plugin + "&page=form", { action: plugin + "_update_permalink", text: title, table: table },
			function(data){
				if(data != "" && data != "-") {
					$('#permalink').val(data);

					if(type=="permalink") {
						$('.permalink_edit').show();
						$('#permalink').css("background-color","#F2F2F2");
						$('#permalink').prop('readonly', true);
					}
				}
			}
		);
	}

}


//***********************************************************************************************************************
//ON DOCUMENT READY FUNCTIONS
//***********************************************************************************************************************
$(function() {

	//*******************************************************************************************************************
	//EXECUTES TIMEOUT TO KEEP THE USERS SESSION ALIVE
	//*******************************************************************************************************************
	var stayalive = setTimeout(stay_alive,45000);

	//*******************************************************************************************************************
	//TURNS ALL CHOOSE DATE CLASS FIELDS INTO A DATEPICKER
	//*******************************************************************************************************************
	$('.choose_date').datepicker({ disabled: false, dateFormat: 'yy-mm-dd' });
	$('.choose_time').timepicker({controlType: 'select', timeFormat: 'h:mm tt'});

    //HEIGHT OF WINDOW
    var windowHeight = $(document).height();
	var windowWidth = $(window).width();

	var navWindowHeight = $(window).height() - 155;

	//*******************************************************************************************************************
	//CUSTOM JAVASCRIPT BASED ON WINDOW SIZE
	//*******************************************************************************************************************
	if (windowWidth < 768) {
		//HIDE MAIN NAV
		$('nav').hide();
		//SHOW NAV TOGGLE
		$('#navToggle').show();
		//DISPLAY NAV ON CLICK
		$('#navToggle').click(function(){
			$('nav').toggle('slide',{direction:'up'},1000);
		});

    } else {
        //HIDE MOBILE NAV TOGGLE BUTTON
        $('#navToggle').hide();
        //SHOW NAV 
        $('nav').show();
        //ADD MIN-HEIGHT TO THE NAV AND RIGHT COLUMN BG
        $('nav, .rightColumn').css('height', navWindowHeight);
    }

	$(window).resize(function(){
		if ($(window).width() != windowWidth && $(window).width() < 768) {
			//REFRESH BROWSER
			location.reload();

			if ($(window).width() < 768) {
				//HIDE MAIN NAV
				$('nav').hide();
				//SHOW NAV TOGGLE
				$('#navToggle').show();
				//DISPLAY NAV ON CLICK
				$('#navToggle').click(function(){
					$('nav').toggle('slide',{direction:'up'},1000);
				});

				//APPLY WINDOW HEIGHT TO THE <HEADER>
				$('nav, .rightColumn').css('height', 'auto');


            }
            else {
                //HIDE MOBILE NAV TOGGLE BUTTON
                $('#navToggle').hide();
                //SHOW NAV
                $('nav').show();
                //ADD MIN-HEIGHT TO THE NAV AND RIGHT COLUMN BG
                $('nav, .rightColumn').css('min-height', navWindowHeight);

                //APPLY WINDOW HEIGHT TO THE <HEADER>
                $('nav, .rightColumn').css('height', navWindowHeight);

			}
		}
	});

	//*******************************************************************************************************************
	//CUSTOM JAVASCRIPT FOR HANDLING COMPANY SELECTION
	//*******************************************************************************************************************
	var selectedUserWrapper 	= $('.selectedUserWrapper');
	var selectedCompany 		= $('.selectedCompany');

	var userCompanyOptionsList 	= $('.userCompanyOptions');

	$(selectedUserWrapper).click(function(){

		//CLICK WRAPPER TO DISPLAY COMPANY OPTION MENU
		$(userCompanyOptionsList).toggle();

		$(userCompanyOptionsList).children('li').click(function(){
			$(selectedCompany).html($(this).html());
			$(userCompanyOptionsList).hide();
		});

	});


	//*******************************************************************************************************************
	//CUSTOM JAVASCRIPT FOR HANDLING SUBNAVIGATION
	//*******************************************************************************************************************
	//MOBILE TOGGLE SUB NAVIGATION
	var subNavWrapper 			= $('.subNavWrapper');
	var subNavWrapperBtn 		= $(subNavWrapper).children('.mobileDeviceBtn');
	var subNavList				= $(subNavWrapper).children('.subNav');

	//DEVICE RESIZE TOGGLE
	var deviceResizeWrapper		= $('.deviceResizeWrapper');
	var deviceResizeWrapperBtn 	= $(deviceResizeWrapper).children('.mobileDeviceBtn');
	var deviceResizeList		= $(deviceResizeWrapper).children('ul');

	//TOGGLE MOBILE SIDE NAVIGATION ON MOBILE IF WINDOW WITH IS LESS THAN 900
	if (windowWidth < 900) {

		//TOGGLE SUBNAV LIST ON CLICK
		$(subNavWrapperBtn).click(function(e){

			//DISPLAY SUBNAV LIST
			$(subNavList).toggle();

			//TAKE THE SELECTED CHILD AND REPLACE THE HTML WITH BUTTON TEXT
			$(subNavList).children('li').click(function(){
				$(subNavWrapperBtn).html($(this).html());
				$(subNavList).hide();
			});

			//ON CLICK, SCROLL TO TOP OF WRAPPER
			$('html, body').animate({
				scrollTop: $(subNavWrapper).offset().top - 80
			}, 800);


			e.preventDefault();

		});

		//TOGGLE DEVICE RESIZE LIST ON CLICK
		$(deviceResizeWrapperBtn).click(function(e){

			//DISPLAY DEVICE LIST
			$(deviceResizeList).toggle();

			//TAKE THE SELECTED CHILD AND REPLACE THE HTML WITH BUTTON TEXT
			$(deviceResizeList).children('li').click(function(){
				$(deviceResizeWrapperBtn).html($(this).html());
				$(deviceResizeList).hide();
			});

			//ON CLICK, SCROLL TO TOP OF WRAPPER
			$('html, body').animate({
				scrollTop: $(deviceResizeWrapper).offset().top - 80
			}, 800);


			e.preventDefault();
		});

		$(document).click(function(e){
			if (!$(e.target).is('.selectedUserWrapper *, .userCompanyOptions *')) {
				$(userCompanyOptionsList).hide();
			}
			if (!$(e.target).is('.subNavWrapper *, .subNav *')) {
				$(subNavList).hide();
			}
			if (!$(e.target).is('.deviceResizeWrapper *, .deviceResizeWrapper ul *')) {
				$(deviceResizeList).hide();
			}
		});



	} else {
		//SHOW SIDE NAV
		$(subNavList, deviceResizeList).show();


	}

	//IF WINDOW SIZE ON RESIZE IS LESS THEN 768
	$(window).resize(function(){
		if ($(window).width() != windowWidth && $(window).width() < 900) {
			//REFRESH BROWSER
			location.reload();

			//TOGGLE SUBNAV LIST ON CLICK
			$(subNavWrapperBtn).click(function(e){

				//DISPLAY SUBNAV LIST
				$(subNavList).toggle();

				//TAKE THE SELECTED CHILD AND REPLACE THE HTML WITH BUTTON TEXT
				$(subNavList).children('li').click(function(){
					$(subNavWrapperBtn).html($(this).html());
					$(subNavList).hide();
				});

				//ON CLICK, SCROLL TO TOP OF WRAPPER
				$('html, body').animate({
					scrollTop: $(subNavWrapper).offset().top - 80
				}, 800);

				e.preventDefault();

			});

			//TOGGLE DEVICE RESIZE LIST ON CLICK
			$(deviceResizeWrapperBtn).click(function(e){

				//DISPLAY DEVICE LIST
				$(deviceResizeList).toggle();

				//TAKE THE SELECTED CHILD AND REPLACE THE HTML WITH BUTTON TEXT
				$(deviceResizeList).children('li').click(function(){
					$(deviceResizeWrapperBtn).html($(this).html());
					$(deviceResizeList).hide();
				});

				//ON CLICK, SCROLL TO TOP OF WRAPPER
				$('html, body').animate({
					scrollTop: $(deviceResizeWrapper).offset().top - 80
				}, 800);

				e.preventDefault();
			});

			$(document).click(function(e){
				if (!$(e.target).is('.selectedUserWrapper *, .userCompanyOptions *')) {
					$(userCompanyOptionsList).hide();
				}
				if (!$(e.target).is('.subNavWrapper *, .subNav *')) {
					$(subNavList).hide();
				}
				if (!$(e.target).is('.deviceResizeWrapper *, .deviceResizeWrapper ul *')) {
					$(deviceResizeList).hide();
				}
			});

		}
	});



	//TOGGLE INFO LIGHTBOX
	$('.info.iconSprite').click(function(e){
		$('#infoLightbox').fadeIn();
		$('.lightboxOverlay').fadeIn();
		e.preventDefault();
	});
	//TOGGLE TUTORIAL LIGHTBOX
	$('.iconSprite.video').click(function(e){
		$('#tutorialLightbox').fadeIn();
		$('.lightboxOverlay').fadeIn();
		e.preventDefault();
	});
	//HIDE INFO | TUTORIALLIGHTBOX
	$('.lightboxOverlay, .closeLightbox').click(function(){
		$('#infoLightbox, #tutorialLightbox').fadeOut();
		$('.lightboxOverlay').fadeOut();
	});


	$(document).click(function(e){
		if (!$(e.target).is('.selectedUserWrapper *, .userCompanyOptions *')) {
			$(userCompanyOptionsList).hide();
		}
	});

	//*************************************************************************************************************************************
	/*
	 * Handles the switching of the editor view for responsive devices
	 */
	//*************************************************************************************************************************************
	$('.responsive_trigger').click(function(e){
		e.preventDefault();
		$('.responsive_trigger').removeClass("active");
		var width = $(this).data('width');
		$('#editor_width').width(width);
		$(this).addClass('active');
	});

	//Mobile - toggle dropdown menus:
	//switch devices to display content manage
	//page subNav options

	//*************************************************************************************************************************************
	/*
	 * Adds a confirmation box to anything with the delete_confirm class (mostly used in plugins)
	 */
	//*************************************************************************************************************************************
	$('.delete_confirm').click(function(e){
		var confirmed = confirm("You are about to permanently delete this item.  Are you sure you wish to continue with this delete?");
		if(!confirmed){
			e.preventDefault();
		}
	});

	//*******************************************************************************************************************
	//HANDLE CLICK EVENT FOR CUSTOM FILE UPLOAD BUTTONS
	//*******************************************************************************************************************
	$('.upload_button').change(function(){
		var filepath = $(this).val().split("\\");
		var filename = filepath.pop();
		var parentWrapper = $(this).parent('div').parent('div');
		parentWrapper.find('label').text(filename);
	});

	//*************************************************************************************************************************************
	/*
	 * This Javascript adds a chart to the main dashboard screen
	 */
	//*************************************************************************************************************************************
	if(typeof(charting) != "undefined") {

		// Set a callback to run when the Google Visualization API is loaded.
		google.setOnLoadCallback(drawChart);

		// Callback that creates and populates a data table,
		// instantiates the pie chart, passes in the data and
		// draws it.
		drawChart();

		$(window).resize(function(){
			drawChart();
		});


	}

	//*******************************************************************************************************************
	// LOAD ACCESS LOG ON CLICK OF BUTTON TYPE
	//*******************************************************************************************************************
	$('.access_log').click(function() {
		$('.subnavigation_item').removeClass('subnavigation_on');
		$(this).addClass('subnavigation_on');
		var type = $(this).data('type');
		var id = $(this).data('id');
		load_access_log(type,id);
	});


	toggle_instructions();

	// MAKE PERMALINK INPUT EDITABLE IF USER CLICKS THE PERMALINK EDIT BUTTON
	$('.permalink_edit').click(function() {
		$('#permalink').prop('readonly', false);
		$('#permalink').focus();
		$('.permalink_edit').hide();
		original_permalink = $('#permalink').val();
		$('#permalink').css("background-color","#FFFFFF");
	});

});


//*******************************************************************************************************************
//DRAWS DASHBOARD CHART USING GOOGLE CHARTING LIBRARY
//*******************************************************************************************************************  	
function drawChart() {

	// Create the data table.
	var data = new google.visualization.DataTable();
	data.addColumn('string', 'Dates');
	data.addColumn('number', 'Visits');
	data.addRows(charting);

	var chart_width = $('#chart_div').width();

	// Set chart options
	var options = {chartArea: {top:35, width: '100%', height: '86%'},
		titlePosition: 'in',
		axisTitlesPosition: 'in',

		hAxis: {
			textPosition: 'out',
			showTextEvery: 4,
			textStyle: {
				auraColor: 'none',
				color: '#666666',
				fontName: 'Lato'
			},
			gridlines: {
				count:7,
				color: "#F2F2F2"
			}
		},
		vAxis: {
			textPosition: 'in',
			textStyle: {
				auraColor: 'none',
				color: '#666666',
				fontName: 'Lato',
				fontSize: 10
			},
			gridlines: {
				count:10,
				color: "#F2F2F2"
			},
			baselineColor: '#E6E6E6'},
		colors: ['#FF5810']

	};

	// Instantiate and draw our chart, passing in some options.
	var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
	chart.draw(data, options);

}