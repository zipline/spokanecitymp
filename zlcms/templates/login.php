<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="<?PHP $this->path(); ?>images/icons/zlcmsFavicon.png">
    <title>ZLCMS 3 - A Content Management System by Zipline Interactive</title>		
	<link href="<?PHP $this->path(); ?>style/login.css" rel="stylesheet">
    <!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
	<meta name="robots" content="noindex">
</head>
<body>
	<div class="loginWrapper">
		<div class="full">
			<img class="loginLogo" src="<?PHP $this->path(); ?>images/icons/loginZLlogo.png" alt="ZLCMS Login">
		</div><!--closing of full-->
		<?PHP $this->show_message(); ?>
		<form id="loginForm" method="post" action="<?PHP echo($_SERVER['REQUEST_URI']); ?>">
			<input type="text" value="" placeholder="Email" name="email" id="email">				
			<input type="password" value="" placeholder="Password" name="password" id="password">
			<div class="half pr">
				<input type="hidden" name="action" value="login">
				<input type="submit" value="Login" name="login" id="login">
				<img src="<?PHP $this->path(); ?>images/icons/ajax-loader.gif" alt="Waiting for login" id="loggingIn">
			</div><!--closing of half-->
			<div class="half">
				<a href="#" id="forgot_password">Forgot Password</a>
			</div><!--closing of half-->
		</form>
		<div class="clear"></div>
	</div><!--closing of loginWrapper-->
	<script src="https://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
	<script src="<?PHP $this->path(); ?>includes/login.js" type="text/javascript"></script>
</body>
</html>