<?PHP

	//START THE ZLCMS SESSION
	session_start();
	
	//TURN ON ALL ERRORS FOR PROPER DEBUGGING
	ini_set('display_errors',1);
	error_reporting(E_ALL);
	ini_set('default_charset', 'UTF-8');

	//INCLUDE THE ZLCMS SYSTEM CLASS
	include("controllers/zlcms.class.php");

	//INITIATE THE ZLCMS CLASS
	$zlcms = new ZLCMS(@$_GET['page'],@$_GET['plugin']);
	
	//ZLCMS INITIATE FUNCTION
	$zlcms->initiate($_SERVER['SERVER_NAME'],@$_GET['url']);
	
	//ZLCMS PLUGIN CONSTRUCT
	$zlcms->plugin_construct();
	
	//PROCESSES ALL POST/GET REQUESTS WITH ACTION SET (USED FOR FORM PROCESSING)
	$zlcms->process_request(@$_REQUEST['action']);
	
	//ATTEMPT TO VALIDATE LICENSE
	$zlcms->validate_license();

	//LOAD UP THE TEMPLATE
	$zlcms->load_template();
	
	//CLOSE THE DATABASE CONNECTION
	$zlcms->close_database();

?>