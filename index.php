<?PHP

	//SHOW ALL ERRORS
	ini_set('display_errors',1);
	error_reporting(E_ALL);

	//START THE ZLCMS SESSION
	session_start();
	
	//INCLUDE THE ZLCMS SYSTEM CLASS
	include("core/controllers/zlcms_core.class.php"); 
	
	//INITIATE THE ZLCMS CLASS
	$zlcms = new zlcms_core();

	//SELECTS A WEBSITE AND LOADS THE PAGES FROM THE DB
	$zlcms->initiate($_SERVER['SERVER_NAME'],@$_GET['url']);

	//SERVES AS A CONSTRUCT FOR ALL PLUGINS BUT LOADS AFTER THEY ARE INITIATED
	$zlcms->plugin_construct();
	
	//PROCESSES ALL POST/GET REQUESTS WITH ACTION SET (USED FOR FORM PROCESSING)
	$zlcms->process_request(@$_REQUEST['action']);

	//LOAD UP THE TEMPLATE
	$zlcms->load_template();

	//CLOSE THE DATABASE CONNECTION
	$zlcms->close_database();
	
?>